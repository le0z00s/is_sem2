<?php
/**
 * @package Views
 */
?>
<div id="part-<?php echo $index; ?>-content" class="page">
	<div class="page" id="page-<?php echo $index; ?>">
	  <div class="part" id="part-<?php echo $index; ?>">
	    <input id="part_<?php echo ($index-1); ?>_name" name="part[<?php echo ($index-1); ?>][name]" type="hidden" value="<?php echo $page_part->name; ?>" />
	    <?php if (isset($page_part->id)): ?>
	    <input id="part_<?php echo ($index-1); ?>_id" name="part[<?php echo ($index-1); ?>][id]" type="hidden" value="<?php echo $page_part->id; ?>" />
	    <?php endif; ?>
	    <div>
	    	<textarea class="textarea" id="part_<?php echo ($index-1); ?>_content" name="part[<?php echo ($index-1); ?>][content]" style="width: 100%" rows="20" cols="40"><?php echo htmlentities($page_part->content, ENT_COMPAT, 'UTF-8'); ?></textarea>
	    </div>
	  </div>
	</div>
</div>

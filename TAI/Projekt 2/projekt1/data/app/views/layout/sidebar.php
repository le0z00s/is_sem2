<?php
/**
 * @package Views
 */
?>
<?php if (Dispatcher::getAction() == 'index'): ?>

<p class="button"><a href="<?php echo get_url('layout/add'); ?>"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/layout.png" align="middle" alt="layout icon" /> <?php echo __('New Layout'); ?></a></p>

<?php endif; ?>

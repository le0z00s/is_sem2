<?php
/**
 * @package Views
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title><?php echo __('Register').' - '.Setting::get('admin_title'); ?></title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="favourites icon" href="<?php echo PATH_PUBLIC; ?>data/admin/themes/<?php echo Setting::get('theme'); ?>/images/favicon.ico" />
        <link href="<?php echo PATH_PUBLIC; ?>data/admin/themes/<?php echo Setting::get('theme'); ?>/login.css" id="css_theme" media="screen" rel="Stylesheet" type="text/css" />
        <script type="text/javascript" charset="utf-8" src="<?php echo PATH_PUBLIC; ?>data/admin/javascripts/jquery-1.8.3.min.js"></script>
        <script type="text/javascript">
            // <![CDATA[
            $(document).ready(function() {
                (function showMessages(e) {
                    e.fadeIn('slow')
                    .animate({opacity: 1.0}, 1500)
                    .fadeOut('slow', function() {
                        if ($(this).next().attr('class') == 'message') {
                            showMessages($(this).next());
                        }
                        $(this).remove();
                    })
                })( $(".message:first") );

                $("input:visible:enabled:first").focus();
            });
            // ]]>
        </script>
    </head>
    <body>
        <div id="dialog">
            <h1><?php echo __('Register').' - '.Setting::get('admin_title'); ?></h1>
            <?php if (Flash::get('error') !== null): ?>
                <div id="error" class="message" style="display: none;"><?php echo Flash::get('error'); ?></div>
            <?php endif; ?>
            <?php if (Flash::get('success') !== null): ?>
                    <div id="success" class="message" style="display: none"><?php echo Flash::get('success'); ?></div>
            <?php endif; ?>
            <?php if (Flash::get('info') !== null): ?>
                        <div id="info" class="message" style="display: none"><?php echo Flash::get('info'); ?></div>
            <?php endif; ?>
                        <form action="<?php echo get_url('login/register'); ?>" method="post">
                            <div id="register-name-div">
                            	<label for="user_name"><?php echo __('First Name'); ?></label>
                            	<input class="medium" id="user_name" name="user[name]" size="40" type="text" value="<?php echo $user->name; ?>" />
                            </div>
                            <div id="register-surname-div">
                            	<label for="user_surname"><?php echo __('Last Name'); ?></label>
                            	<input class="medium" id="user_surname" name="user[surname]" size="40" type="text" value="<?php echo $user->surname; ?>" />
                            </div>
                            <div id="register-address-div">
                            	<label for="user_address"><?php echo __('Address'); ?></label>
                            	<input class="medium" id="user_address" name="user[address]" size="40" type="text" value="<?php echo $user->address; ?>" />
                            </div>
                            
                            <div id="register-country-div">
                            	<label for="user_country"><?php echo __('Country'); ?></label>
                            	<input class="medium" id="user_country" name="user[country]" size="40" type="text" value="<?php echo $user->country; ?>" />
                            </div>
                            
                            <div id="register-phone-div">
                            	<label for="user_tel"><?php echo __('Telephone'); ?></label>
                            	<input class="medium" id="user_tel" maxlength="10" name="user[tel]" size="40" type="tel" value="<?php echo $user->tel; ?>" />
                            </div>
                            
                            <div id="register-email-div">
                            	<label class="optional" for="user_email"><?php echo __('E-mail'); ?></label>
                            	<input class="medium" id="user_email" name="user[email]" size="40" type="text" value="<?php echo $user->email; ?>" />
                            </div>
                            
                            <div id="register-username-div">
                            	<label for="user_username"><?php echo __('Username'); ?></label>
                            	<input class="medium" id="user_username" maxlength="40" name="user[username]" size="40" type="text" value="<?php echo $user->username; ?>"/>
                            </div>
                            
                            <div id="register-password-div">
                            	<label for="user_password"><?php echo __('Password'); ?></label>
                            	<input class="medium" id="user_password" maxlength="40" name="user[password]" size="40" type="password" value="" />
                            </div>
                            
                            <div id="register-confirm-div">
                            	<label for="user_confirm"><?php echo __('Confirm Password'); ?></label>
                            	<input class="medium" id="user_confirm" maxlength="40" name="user[confirm]" size="40" type="password" value="" />
                            </div>
                            
                            <div id="register-language-div">
                            	<label for="user_language"><?php echo __('Language'); ?></label>
                            	<select class="select" id="user_language" name="user[language]">
									<?php foreach (Setting::getLanguages() as $code => $label): ?>
           							<option value="<?php echo $code; ?>"<?php if ($code == $user->language) echo ' selected="selected"'; ?>><?php echo $label; ?></option>
									<?php endforeach; ?>
     							</select>
                            </div>
                            
                            <div class="clean"></div>
                            <div id="register_submit">
                                <input class="submit" type="submit" accesskey="s" value="<?php echo __('Register'); ?>" />
                            </div>
                        </form>
                    </div>
                    <p><?php echo __('website:').' <a href="'.URL_PUBLIC.'">'.Setting::get('admin_title').'</a>'; ?></p>
        <script type="text/javascript" charset="utf-8">
            // <![CDATA[
            var loginUsername = document.getElementById('login-username');
            if (loginUsername.value == '') {
                loginUsername.focus();
            } else {
                document.getElementById('login-password').focus();
            }
            // ]]>
        </script>
    </body>
</html>

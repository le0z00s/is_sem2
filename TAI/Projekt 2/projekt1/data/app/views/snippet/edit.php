<?php

/**
 * @package Views
 * @version $Id$
 */
?>
<h1><?php echo __(ucfirst($action).' snippet'); ?></h1>

<form action="<?php echo $action=='edit' ? get_url('snippet/edit/'.$snippet->id): get_url('snippet/add'); ; ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
    <div class="form-area">
        <h3><?php echo __('Name'); ?></h3>
        <div id="meta-pages" class="pages">
            <p class="title">
                <input class="textbox" id="snippet_name" maxlength="100" name="snippet[name]" size="255" type="text" value="<?php echo $snippet->name; ?>" />
            </p>
        </div>

        <h3><?php echo __('Body'); ?></h3>
        <div id="pages" class="pages">
            <div class="page" style="">
                <textarea class="textarea" cols="40" id="snippet_content" name="snippet[content]" rows="20" style="width: 100%"><?php echo htmlentities($snippet->content, ENT_COMPAT, 'UTF-8'); ?></textarea>
                <?php if (isset($snippet->updated_on)): ?>
                <p style="clear: left">
                    <small><?php echo __('Last updated by'); ?> <?php echo $snippet->updated_by_name; ?> <?php echo __('on'); ?> <?php echo date('D, j M Y', strtotime($snippet->updated_on)); ?></small>
                </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <p class="buttons">
        <?php if (($action=='edit' && AuthUser::hasPermission('snippet_edit')) || ($action=='add' && AuthUser::hasPermission('snippet_add'))): ?>
            <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
            <input class="button" name="continue" type="submit" accesskey="e" value="<?php echo __('Save and Continue Editing'); ?>" />
            <?php echo __('or'); ?> 
        <?php else: ?>
            <?php echo ($action=='add') ? __('You do not have permission to add snippets!') : __('You do not have permission to edit snippets!'); ?> 
        <?php endif;?>
        <a href="<?php echo get_url('snippet'); ?>"><?php echo __('Cancel'); ?></a>
    </p>
</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
  document.getElementById('snippet_name').focus();
// ]]>
</script>

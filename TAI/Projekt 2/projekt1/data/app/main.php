<?php
/**
 * @package CMS
 */
if (!defined('HELPER_PATH'))
    define('HELPER_PATH', CORE_ROOT.DS.'helpers');
if (!defined('URL_SUFFIX'))
    define('URL_SUFFIX', '');

ini_set('date.timezone', DEFAULT_TIMEZONE);
if (function_exists('date_default_timezone_set'))
    date_default_timezone_set(DEFAULT_TIMEZONE);
else
    putenv('TZ='.DEFAULT_TIMEZONE);

function explode_path($path) {
    return preg_split('/\//', $path, -1, PREG_SPLIT_NO_EMPTY);
}

function explode_uri($uri) {
    return explode_path($uri);
}

function find_page_by_slug($slug, &$parent, $all = false) {
    return Page::findBySlug($slug, $parent, $all);
}


function url_match($url) {
    $url = trim($url, '/');

    if (CURRENT_PATH == $url)
        return true;

    return false;
}


function url_start_with($url) {
    $url = trim($url, '/');

    if (CURRENT_PATH == $url)
        return true;

    if (strpos(CURRENT_PATH, $url) === 0)
        return true;

    return false;
}


function main() {
    $path = $_SERVER['QUERY_STRING'];

    $path = urldecode($path);

    if (!USE_MOD_REWRITE && strpos($path, '?') !== false) {
        $_GET = array();
        list($path, $get_var) = explode('?', $path);
        $exploded_get = explode('&', $get_var);

        if (count($exploded_get)) {
            foreach ($exploded_get as $get) {
                list($key, $value) = explode('=', $get);
                $_GET[$key] = $value;
            }
        }
    }
    else if (!USE_MOD_REWRITE && (strpos($path, '&') !== false || strpos($path, '=') !== false)) {
        $path = '/';
    }

    if (USE_MOD_REWRITE && array_key_exists('PAGE_PATH', $_GET)) {
        $path = $_GET['PAGE_PATH'];
        unset($_GET['PAGE_PATH']);
    }
    else if (USE_MOD_REWRITE)
        $path = '/';

    if (array_key_exists('WOLFAJAX', $_GET)) {
        $path = '/'.ADMIN_DIR.$_GET['WOLFAJAX'];
        unset($_GET['WOLFAJAX']);
    }
    if (URL_SUFFIX !== '' and URL_SUFFIX !== '/')
        $path = preg_replace('#^(.*)('.URL_SUFFIX.')$#i', "$1", $path);

    define('CURRENT_PATH', trim($path, '/'));

    define('CURRENT_URI', CURRENT_PATH);

    if ($path != null && $path[0] != '/')
        $path = '/'.$path;

    if (Dispatcher::hasRoute($path)) {
        Observer::notify('dispatch_route_found', $path);
        Dispatcher::dispatch($path);
        exit;
    }

    foreach (Observer::getObserverList('page_requested') as $callback) {
        $path = call_user_func_array($callback, array(&$path));
    }

    $page = Page::findByPath($path, true);

    if (is_object($page)) {
        if (Page::STATUS_PREVIEW == $page->status_id) {
            AuthUser::load();
            if (!AuthUser::isLoggedIn() || !AuthUser::hasPermission('page_view'))
                pageNotFound($path);
        }

        if ($page->getLoginNeeded() == Page::LOGIN_REQUIRED) {
            AuthUser::load();
            if (!AuthUser::isLoggedIn()) {
                Flash::set('redirect', $page->url());
                redirect(URL_PUBLIC.(USE_MOD_REWRITE ? '' : '?/').ADMIN_DIR.'/login');
            }
        }

        Observer::notify('page_found', $page);
        $page->_executeLayout();
    }
    else {
        pageNotFound($path);
    }
}
ob_start();
main();
ob_end_flush();

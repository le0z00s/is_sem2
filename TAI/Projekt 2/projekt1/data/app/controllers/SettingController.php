<?php

/**
 * @package Controllers
 */

class SettingController extends Controller {

    private static final function _checkPermission() {
        AuthUser::load();
        if (!AuthUser::isLoggedIn()) {
            redirect(get_url('login'));
        }
        else {
            if (!AuthUser::hasPermission('admin_edit')) {
                Flash::set('error', __('You do not have permission to access the requested page!'));

                if (Setting::get('default_tab') === 'setting') {
                    redirect(get_url('page'));
                }
                else {
                    redirect(get_url());
                }
            }
        }
    }


    public final function __construct() {
        SettingController::_checkPermission();
        $this->setLayout('backend');
    }

    public final function index() {
        if (get_request_method() == 'POST') {
            $this->_save();
        }

        $this->display('setting/index',
                array('csrf_token' => SecureToken::generateToken(BASE_URL.'setting'))
        );
    }

    private final function _save() {
        $data = $_POST['setting'];

        if (isset($_POST['csrf_token'])) {
            $csrf_token = $_POST['csrf_token'];
            if (!SecureToken::validateToken($csrf_token, BASE_URL.'setting')) {
                Flash::set('error', __('Invalid CSRF token found!'));
                Observer::notify('csrf_token_invalid', AuthUser::getUserName());
                redirect(get_url('setting'));
            }
        }
        else {
            Flash::set('error', __('No CSRF token found!'));
            Observer::notify('csrf_token_not_found', AuthUser::getUserName());
            redirect(get_url('setting'));
        }

        if (!isset($data['allow_html_title'])) {
            $data['allow_html_title'] = 'off';
        }

        use_helper('Kses');
        $allowed = array(
            'img' => array(
                'src' => array()
            ),
            'abbr' => array(
                'title' => array()
            ),
            'acronym' => array(
                'title' => array()
            ),
            'b' => array(),
            'blockquote' => array(
                'cite' => array()
            ),
            'br' => array(),
            'code' => array(),
            'em' => array(),
            'i' => array(),
            'p' => array(),
            'strike' => array(),
            'strong' => array()
        );
        $data['admin_title'] = kses(trim($data['admin_title']), $allowed);

        Setting::saveFromData($data);
        Flash::set('success', __('Settings have been saved!'));
        redirect(get_url('setting'));
    }

    public final function activate_plugin($plugin) {
        Plugin::activate($plugin);
        Observer::notify('plugin_after_enable', $plugin);
    }

    public final function deactivate_plugin($plugin) {
        Plugin::deactivate($plugin);
        Observer::notify('plugin_after_disable', $plugin);
    }

    public final function uninstall_plugin($plugin) {
        Plugin::uninstall($plugin);
        Observer::notify('plugin_after_uninstall', $plugin);
    }

    public static $ietf = array(
        'en' => 'English',
        'en_AU' => 'English/Australia',
        'en_BZ' => 'English/Belize',
        'en_CA' => 'English/Canada',
        'en_IE' => 'English/Ireland',
        'en_JM' => 'English/Jamaica',
        'en_NZ' => 'English/New Zealand',
        'en_PH' => 'English/Philippines',
        'en_ZA' => 'English/South Africa',
        'en_TT' => 'English/Trinidad and Tobago',
        'en_UK' => 'English/United Kingdom',
        'en_US' => 'English/United States',
        'en_ZW' => 'English/Zimbabwe',
        'pl' => 'Polish');

}

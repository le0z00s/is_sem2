<?php
/**
 * @package Controllers
 */

class PluginController extends Controller {

    public function __call($function, $args) {
        if (!defined('CMS_BACKEND')) {
            $args = implode(', ', $args);
            return Page::$function($args);
        }
        else {
            return false;
        }
    }

    public function __get($variable) {
        if (!defined('CMS_VERSION')) {
            if (isset(Page::$$variable)) {
                return Page::$$variable;
            }
            else {
                return false;
            }
        }
    }

    public $url;

    public $plugin;

    function __construct() {
        if (defined('CMS_BACKEND')) {
            AuthUser::load();
            if ( ! AuthUser::isLoggedIn()) {
                redirect(get_url('login'));
            }
        }
    }

    public function display($view, $vars=array(), $exit=true) {
        if (defined('CMS_BACKEND')) {
            return parent::display($view, $vars, $exit);
        }
        else {
            $this->content = $this->render($view, $vars);
            $this->executeFrontendLayout();
            if ($exit) {
                exit;
            }
        }
    }

    private function executeFrontendLayout() {
        $sql = 'SELECT content_type, content FROM '.TABLE_PREFIX.'layout WHERE name = '."'$this->frontend_layout'";

        Record::logQuery($sql);

        $stmt = Record::getConnection()->prepare($sql);
        $stmt->execute();

        $layout = $stmt->fetchObject();

        if ($layout) {
            if ($layout->content_type == '') {
                $layout->content_type = 'text/html';
            }

            header('Content-Type: '.$layout->content_type.'; charset=UTF-8');

            $this->url = CURRENT_PATH;

            eval('?>'.$layout->content);
        }
    }

    public function setLayout($layout) {
        if (defined('CMS_BACKEND')) {
            parent::setLayout($layout);
        }
        else {
            $this->frontend_layout = $layout;
        }
    }

    public function render($view, $vars=array()) {
        if (defined('CMS_BACKEND')) {
            if ($this->layout) {
                $this->layout_vars['content_for_layout'] = new View('../../plugins/'.$view, $vars);
                return new View('../layouts/'.$this->layout, $this->layout_vars);
            }
            else {
                return new View('../../plugins/'.$view, $vars);
            }
        }
        else {
            return parent::render($view, $vars);
        }
    }

    public function execute($action, $params) {
        if (isset(Plugin::$controllers[$action])) {
            $plugin = Plugin::$controllers[$action];
            if (file_exists($plugin->file)) {
                include_once $plugin->file;

                $plugin_controller = new $plugin->class_name;

                $action = count($params) ? array_shift($params): 'index';

                call_user_func_array(
                    array($plugin_controller, $action),
                    $params
                );
            }
            else {
                throw new Exception("Plugin controller file '{$plugin->file}' was not found!");
            }
        }
        else {
            throw new Exception("Action '{$action}' is not valid!");
        }
    }

}

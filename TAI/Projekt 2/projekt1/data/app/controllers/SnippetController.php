<?php
/**
 * @package Controllers
 */

class SnippetController extends Controller {

    public function __construct() {
        AuthUser::load();
        if ( ! AuthUser::isLoggedIn()) {
            redirect(get_url('login'));
        }
        else {
            if ( ! AuthUser::hasPermission('snippet_view') ) {
                Flash::set('error', __('You do not have permission to access the requested page!'));

                if (Setting::get('default_tab') === 'snippet') {
                    redirect(get_url('page'));
                }
                else {
                    redirect(get_url());
                }
            }
        }

        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('snippet/sidebar'));
    }

    public function index() {
        $this->display('snippet/index', array(
                'snippets' => Record::findAllFrom('Snippet', '1=1 ORDER BY position')
        ));
    }

    public function add() {
        if (get_request_method() == 'POST') {
            $this->_add();
        }

        $snippet = Flash::get('post_data');

        if (empty($snippet)) {
            $snippet = new Snippet;
        }

        $this->display('snippet/edit', array(
                'action'  => 'add',
                'csrf_token' => SecureToken::generateToken(BASE_URL.'snippet/add'),
                'snippet' => $snippet
        ));
    }

    private function _add() {
        $data = $_POST['snippet'];
        
        if (!AuthUser::hasPermission('snippet_add')) {
            Flash::set('error', __('You do not have permission to add snippets!'));
            redirect(get_url('snippet'));
        }
        
        if (isset($_POST['csrf_token'])) {
            $csrf_token = $_POST['csrf_token'];
            if (!SecureToken::validateToken($csrf_token, BASE_URL.'snippet/add')) {
                Flash::set('post_data', (object) $data);
                Flash::set('error', __('Invalid CSRF token found!'));
                Observer::notify('csrf_token_invalid', AuthUser::getUserName());
                redirect(get_url('snippet/add'));
            }
        }
        else {
            Flash::set('post_data', (object) $data);
            Flash::set('error', __('No CSRF token found!'));
            Observer::notify('csrf_token_not_found', AuthUser::getUserName());
            redirect(get_url('snippet/add'));
        }

        $snippet = new Snippet($data);

        if ( ! $snippet->save()) {
            Flash::set('post_data', (object) $data);
            Flash::set('error', __('Snippet has not been added. Name must be unique!'));
            redirect(get_url('snippet', 'add'));
        }
        else {
            Flash::set('success', __('Snippet has been added!'));
            Observer::notify('snippet_after_add', $snippet);
        }

        if (isset($_POST['commit'])) {
            redirect(get_url('snippet'));
        }
        else {
            redirect(get_url('snippet/edit/'.$snippet->id));
        }
    }

    public function edit($id) {
        $snippet = Flash::get('post_data');

        if (empty($snippet)) {
            $snippet = Snippet::findById($id);
            if ( !$snippet ) {
                Flash::set('error', __('Snippet not found!'));
                redirect(get_url('snippet'));
            }
        }

        if (get_request_method() == 'POST') {
            $this->_edit($id);
        }

        $this->display('snippet/edit', array(
                'action'  => 'edit',
                'csrf_token' => SecureToken::generateToken(BASE_URL.'snippet/edit'),
                'snippet' => $snippet
        ));
    }

    private function _edit($id) {
        $data = $_POST['snippet'];
        $data['id'] = $id;
        
        if (!AuthUser::hasPermission('snippet_edit')) {
            Flash::set('error', __('You do not have permission to edit snippets!'));
            redirect(get_url('snippet'));
        }
        
        if (isset($_POST['csrf_token'])) {
            $csrf_token = $_POST['csrf_token'];
            if (!SecureToken::validateToken($csrf_token, BASE_URL.'snippet/edit')) {
                Flash::set('post_data', (object) $data);
                Flash::set('error', __('Invalid CSRF token found!'));
                Observer::notify('csrf_token_invalid', AuthUser::getUserName());
                redirect(get_url('snippet/edit/'.$id));
            }
        }
        else {
            Flash::set('post_data', (object) $data);
            Flash::set('error', __('No CSRF token found!'));
            Observer::notify('csrf_token_not_found', AuthUser::getUserName());
            redirect(get_url('snippet/edit/'.$id));
        }

        $snippet = new Snippet($data);

        if ( ! $snippet->save()) {
            Flash::set('post_data', (object) $data);
            Flash::set('error', __('Snippet :name has not been saved. Name must be unique!', array(':name'=>$snippet->name)));
            redirect(get_url('snippet/edit/'.$id));
        }
        else {
            Flash::set('success', __('Snippet :name has been saved!', array(':name'=>$snippet->name)));
            Observer::notify('snippet_after_edit', $snippet);
        }

        if (isset($_POST['commit'])) {
            redirect(get_url('snippet'));
        }
        else {
            redirect(get_url('snippet/edit/'.$id));
        }
    }

    public function delete($id) {
        
        if (!AuthUser::hasPermission('snippet_delete')) {
            Flash::set('error', __('You do not have permission to delete snippets!'));
            redirect(get_url('snippet'));
        }
        
        if ($snippet = Record::findByIdFrom('Snippet', $id)) {
            if ($snippet->delete()) {
                Flash::set('success', __('Snippet :name has been deleted!', array(':name'=>$snippet->name)));
                Observer::notify('snippet_after_delete', $snippet);
            }
            else {
                Flash::set('error', __('Snippet :name has not been deleted!', array(':name'=>$snippet->name)));
            }
        }
        else {
            Flash::set('error', __('Snippet not found!'));
        }

        redirect(get_url('snippet'));
    }

    public function reorder() {
        parse_str($_POST['data']);

        foreach ($snippets as $position => $snippet_id) {
            $snippet = Record::findByIdFrom('Snippet', $snippet_id);
            $snippet->position = (int) $position + 1;
            $snippet->save();
        }
    }

}

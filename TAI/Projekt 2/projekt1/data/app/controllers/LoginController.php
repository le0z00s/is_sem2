<?php 
/**
 * @package Controllers
 */

class LoginController extends Controller {

    function __construct() {
        if (defined('USE_HTTPS') && USE_HTTPS && (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on")) {
            $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            header("Location: $url");
            exit;
        }

        AuthUser::load();
    }
    function index() {
        $redirect = '';
        
        if (Flash::get('redirect') != null)
            $redirect = Flash::get('redirect');
        elseif (Flash::get('HTTP_REFERER') != null)
            $redirect = trim(Flash::get('HTTP_REFERER'));
        
        if (AuthUser::isLoggedIn()) {
            if ($redirect != '')
                redirect($redirect);
            else
                redirect(get_url());
        }

        Observer::notify('login_required', $redirect);

        $this->display('login/login', array(
            'username' => Flash::get('username'),
            'redirect' => $redirect
        ));
    }

    function login() {
        $redirect = '';
        
        if (Flash::get('redirect') != null)
            $redirect = Flash::get('redirect');
        elseif (Flash::get('HTTP_REFERER') != null)
            $redirect = trim(Flash::get('HTTP_REFERER'));
        
        // Allow plugins to handle login
        Observer::notify('login_requested', $redirect);

        // already log in ?
        if (AuthUser::isLoggedIn())
            if ($redirect != '')
                redirect($redirect);
            else
                redirect(get_url());

        if (get_request_method() == 'POST') {
            $data = isset($_POST['login']) ? $_POST['login']: array('username' => '', 'password' => '');
            Flash::set('username', $data['username']);

            if (AuthUser::login($data['username'], $data['password'], isset($data['remember']))) {
                Observer::notify('admin_login_success', $data['username']);

                // redirect to defaut controller and action
                if ($data['redirect'] != null && $data['redirect'] != 'null')
                    redirect($data['redirect']);
                else
                    redirect(get_url());
            }
            else {
                Flash::set('error', __('Login failed. Check your username and password.<br/>If you tried to login more than :attempts times, you will have to wait at least :delay seconds before trying again.', array(':attempts' => DELAY_FIRST_AFTER, ':delay' => DELAY_ONCE_EVERY)));
                Observer::notify('admin_login_failed', $data['username']);
            }
        }

        if ($data['redirect'] != null && $data['redirect'] != 'null')
            redirect($data['redirect']);
        else
            redirect(get_url('login'));

    }

    function logout() {
        if (isset($_GET['csrf_token'])) {
            $csrf_token = $_GET['csrf_token'];
            if (!SecureToken::validateToken($csrf_token, BASE_URL.'login/logout')) {
                Flash::set('error', __('Invalid CSRF token found!'));
                redirect(get_url());
            }
        }
        else {
            Flash::set('error', __('No CSRF token found!'));
            redirect(get_url());
        }
        
        Observer::notify('logout_requested');

        $username = AuthUser::getUserName();
        AuthUser::logout();
        
        setcookie("expanded_rows", "", time()-3600);
        setcookie("meta_tab", "", time()-3600);
        setcookie("page_tab", "", time()-3600);
        
        Observer::notify('admin_after_logout', $username);
        redirect(get_url());
    }

    function forgot() {
        if (get_request_method() == 'POST')
            return $this->_sendPasswordTo($_POST['forgot']['email']);

        $this->display('login/forgot', array('email' => Flash::get('email')));
    }

    private function _sendPasswordTo($email) {
        $user = User::findBy('email', $email);
        if ($user) {
            use_helper('Email');

            $new_pass = '12'.dechex(rand(100000000, 4294967295)).'K';
            $user->password = AuthUser::generateHashedPassword($new_pass.$user->salt);
            $user->save();

            $email = new Email();
            $email->from(Setting::get('admin_email'), Setting::get('admin_title'));
            $email->to($user->email);
            $email->subject(__('Your new password from ').Setting::get('admin_title'));
            $email->message(__('Username').': '.$user->username."\n".__('Password').': '.$new_pass);
            $email->send();

            Flash::set('success', __('An email has been sent with your new password!'));
            redirect(get_url('login'));
        }
        else {
            Flash::set('email', $email);
            Flash::set('error', __('No user found!'));
            redirect(get_url('login/forgot'));
        }
    }
    
    public function register() {    
    	if (get_request_method() == 'POST') {
    		return $this->_register();
    	}
    
    	$user = Flash::get('post_data');
    
    	if (empty($user)) {
    		$user = new User;
    		$user->language = Setting::get('language');
    	}
    
    	$this->display('login/register', array(
    			'user' => $user,
    			'roles' => Record::findAllFrom('Role')
    	));
    }
    
    
    private function _register() {
    	use_helper('Validate');
    	$data = $_POST['user'];
    
    	$errors = false;
    
    	if (strlen($data['password']) >= 5 && $data['password'] == $data['confirm']) {
    		unset($data['confirm']);
    	}
    	else {
    		Flash::set('error', __('Password and Confirm are not the same or too small!'));
    		redirect(get_url('login/register'));
    	}
    
    	if (strlen($data['username']) < 3) {
    		Flash::set('error', __('Username must contain a minimum of 3 characters!'));
    		redirect(get_url('login/register'));
    	}
    
    	if ($data['username'] == $data['password']) {
    		Flash::set('error', __('The password must not be the same as the username!'));
    		redirect(get_url('login/register'));
    	}
    
    	$fields = array('username');
    	foreach ($fields as $field) {
    		if (!empty($data[$field]) && !Validate::alphanum_space($data[$field])) {
    			$errors[] = __('Illegal value for :fieldname field!', array(':fieldname' => $field));
    
    			$data[$field] = '';
    		}
    	}
    
    	if (!empty($data['name']) && !Validate::alphanum_space($data['name'], true)) {
    		$errors[] = __('Illegal value for :fieldname field!', array(':fieldname' => 'name'));
    	}
    
    	if (!empty($data['email']) && !Validate::email($data['email'])) {
    		$errors[] = __('Illegal value for :fieldname field!', array(':fieldname' => 'email'));
    
    		$data['email'] = '';
    	}
    
    	if (!empty($data['language']) && !Validate::alpha_dash($data['language'])) {
    		$errors[] = __('Illegal value for :fieldname field!', array(':fieldname' => 'language'));
    
    		$data['language'] = 'en';
    	}
    
    	if ( Record::existsIn('User', 'username=:username', array( ':username' => $data['username'] )) ) {
    		$errors[] = __('Username <b>:username</b> is already in use, please choose other!', array( ':username' => $data['username'] ));
    	}
    
    	Flash::set('post_data', (object) $data);
    
    	if ($errors !== false) {
    		Flash::set('error', implode('<br/>', $errors));
    		redirect(get_url('login/register'));
    	}
    
    	$user = new Register($data);
    
    	$user->salt = AuthUser::generateSalt();
    	$user->password = AuthUser::generateHashedPassword($user->password, $user->salt);
    
    	if ($user->save()) {
    		Flash::set('success', __('User has been added!'));
    		Observer::notify('user_after_add', $user->name, $user->id);
    	}
    	else {
    		Flash::set('error', __('User has not been added!'));
    	}
    
    	redirect(get_url('login'));
    }
}

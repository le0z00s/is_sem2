<?php
/**
 * @package Models
 */

final class Cron extends Record {
    const TABLE_NAME = 'cron';

    protected $id = '1';
    protected $lastrun;

    public function __construct($data=null) {
        if ($data !== null)
            $this->lastrun = $data->lastrun;
    }

    public function getLastRunTime() {
        return $this->lastrun;
    }

    public function beforeUpdate() {
        $this->lastrun = time();
        return true;
    }

    public function generateWebBug() {
            $cronbug = '<!-- About the image below: the website owner chose to start a (cron) job on the server by displaying the following image. -->'."\n";
            $cronbug .= '<img id="cron-webbug" style="display: none;" src="'.URL_PUBLIC.'/data/app/cron.php" width="1" height="1" border="0" />'."\n";
            return $cronbug;
    }

}

?>

<?php 
/**
 * @package Models
 */
class PageTag extends Record {
    const TABLE_NAME = 'page_tag';

    public static function deleteByPageId($id) {
        return self::deleteWhere('PageTag', 'page_id = :page_id', array(':page_id' => (int) $id)) === false ? false : true;
    }
}

<?php
/**
 * @package Models
 */
class PagePart extends Record {
    const TABLE_NAME = 'page_part';

    public $name = 'body';
    public $page_id = 0;
    public $content = '';
    public $content_html = '';

    public function beforeSave() {
            $this->content_html = $this->content;

        return true;
    }

    public static function findByPageId($id) {
        return self::find(array(
            'where'  => 'page_id = :page_id',
            'order'  => 'id',
            'values' => array(':page_id' => (int) $id)
        ));
    }

    public static function deleteByPageId($id) {
        return self::deleteWhere('PagePart', 'page_id = :page_id', array(':page_id' => (int) $id)) === false ? false : true;
    }

}

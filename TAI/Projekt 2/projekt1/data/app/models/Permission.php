<?php 
/**
 * @package Models
 */

class Permission extends Record {
    const TABLE_NAME = 'permission';

    public $id;
    public $name;

    static private $permissions = false;

    public function id() { return $this->id; }
    public function name() { return $this->name; }

    public function  __toString() {
        return $this->name;
    }

    public static function getPermissions() {
        if (!self::$permissions) {
            $perms = self::find();

            foreach ($perms as $perm) {
                self::$permissions[$perm->id()] = $perm;
            }
        }

        return array_values(self::$permissions);
    }

    public function beforeSave() {
        self::$permissions = false;
        return true;
    }

    public static function findById($id) {
        if (!self::$permissions) {
            self::getPermissions();
        }

        if (!array_key_exists((int) $id, self::$permissions))
            return false;

        return self::$permissions[$id];
    }

    public static function findByName($name) {
        return self::findOne(array(
            'where'  => 'name = :name',
            'values' => array(':name' => $name)
        ));
    }
}

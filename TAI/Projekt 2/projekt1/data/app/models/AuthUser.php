<?php
/**
 * @package Models
 */
if (!defined('COOKIE_HTTP_ONLY')) define('COOKIE_HTTP_ONLY', false);
if (!defined('COOKIE_LIFE')) define ('COOKIE_LIFE', 1800);  // 30 minutes
if (!defined('ALLOW_LOGIN_WITH_EMAIL')) define ('ALLOW_LOGIN_WITH_EMAIL', false);
if (!defined('DELAY_ON_INVALID_LOGIN')) define ('DELAY_ON_INVALID_LOGIN', true);
if (!defined('DELAY_ONCE_EVERY')) define ('DELAY_ONCE_EVERY', 30); // 30 seconds
if (!defined('DELAY_FIRST_AFTER')) define ('DELAY_FIRST_AFTER', 3);

class AuthUser {
    const SESSION_KEY                   = 'wolf_auth_user';
    const COOKIE_KEY                    = 'wolf_auth_user';

    static protected $is_logged_in  = false;
    static protected $user_id       = false;
    static protected $is_admin      = false;
    static protected $record        = false;
    static protected $roles         = array();

    static public final function load() {
        if (isset($_SESSION[self::SESSION_KEY]) && isset($_SESSION[self::SESSION_KEY]['username']))
            $user = User::findBy('username', $_SESSION[self::SESSION_KEY]['username']);
        else if (isset($_COOKIE[self::COOKIE_KEY]))
                $user = self::challengeCookie($_COOKIE[self::COOKIE_KEY]);
            else
                return false;

        if ( ! $user)
            return self::logout();

        self::setInfos($user);

        return true;
    }

    static private final function setInfos(Record $user) {
        $_SESSION[self::SESSION_KEY] = array('username' => $user->username);

        self::$record = $user;
        self::$is_logged_in = true;
        self::$roles = $user->roles();
        self::$is_admin = self::hasPermission('administrator');
    }

    static public final function isLoggedIn() {
        return self::$is_logged_in;
    }

    static public final function getRecord() {
        return self::$record ? self::$record: false;
    }

    static public final function getId() {
        return self::$record ? self::$record->id: false;
    }

    static public final function getUserName() {
        return self::$record ? self::$record->username: false;
    }

    static public final function getPermissions() {
        $perms = array();

        foreach(self::$roles as $role) {
            $perms = array_merge($perms, $role->permissions());
        }

        return $perms;
    }

    static public final function hasPermission($permissions) {
        if ($permissions === false)
            return true;
        
        if (self::getId() == 1)
            return true;

        foreach (explode(',', $permissions) as $permission) {
            foreach (self::$roles as $role) {
                if ($role->hasPermission($permission))
                    return true;
            }
        }

        return false;
    }

    static public final function hasRole($roles) {
        if (self::getId() == 1)
            return true;

        foreach (explode(',', $roles) as $role) {
            if (in_array($role, self::$roles))
                return true;
        }

        return false;
    }

    static public final function forceLogin($username, $set_cookie=false) {
        return self::login($username, null, $set_cookie, false);
    }

    static public final function login($username, $password, $set_cookie=false, $validate_password=true) {
        self::logout();

        $user = User::findBy('username', $username);

        if (DELAY_ON_INVALID_LOGIN && $user->failure_count > DELAY_FIRST_AFTER) {
            $last = explode(' ', $user->last_failure);
            $date = explode('-', $last[0]);
            $hours = explode(':', $last[1]);

            $last = mktime($hours[0], $hours[1], $hours[2], $date[1], $date[2], $date[0]);
            $now = time() - (DELAY_ONCE_EVERY * $user->failure_count);

            if ($last > $now) {
                return false;
            }
        }

        if ( ! $user instanceof User && ALLOW_LOGIN_WITH_EMAIL)
            $user = User::findBy('email', $username);

        if ($user instanceof User && (false === $validate_password || self::validatePassword($user, $password))) {
            $user->last_login = date('Y-m-d H:i:s');
            $user->failure_count = 0;
            $user->save();

            if ($set_cookie) {
                $time = $_SERVER['REQUEST_TIME'] + COOKIE_LIFE;
                setcookie(self::COOKIE_KEY, self::bakeUserCookie($time, $user), $time, '/', null, (isset($_ENV['SERVER_PROTOCOL']) && ((strpos($_ENV['SERVER_PROTOCOL'],'https') || strpos($_ENV['SERVER_PROTOCOL'],'HTTPS')))), COOKIE_HTTP_ONLY);
            }

            session_regenerate_id(true);

            self::setInfos($user);
            return true;
        }
        else {
            if ($user instanceof User) {
                $user->last_failure = date('Y-m-d H:i:s');
                $user->failure_count = ++$user->failure_count;
                $user->save();
            }

            return false;
        }
    }

    static public final function logout() {
        unset($_SESSION[self::SESSION_KEY]);

        self::eatCookie();
        self::$record = false;
        self::$user_id = false;
        self::$is_admin = false;
        self::$roles = array();
    }

    static private final function challengeCookie($cookie) {
        $params = self::explodeCookie($cookie);
        if (isset($params['exp'], $params['id'], $params['digest'])) {
            if ( ! $user = User::findById($params['id']))
                return false;

            if (self::bakeUserCookie($params['exp'], $user) == $cookie && $params['exp'] > $_SERVER['REQUEST_TIME'])
                return $user;

        }
        return false;
    }

    static private final function explodeCookie($cookie) {
        $pieces = explode('&', $cookie);

        if (count($pieces) < 2)
            return array();

        foreach ($pieces as $piece) {
            list($key, $value) = explode('=', $piece);
            $params[$key] = $value;
        }
        return $params;
    }

    static private final function eatCookie() {
        setcookie(self::COOKIE_KEY, false, $_SERVER['REQUEST_TIME']-COOKIE_LIFE, '/', null, (isset($_ENV['SERVER_PROTOCOL']) && (strpos($_ENV['SERVER_PROTOCOL'],'https') || strpos($_ENV['SERVER_PROTOCOL'],'HTTPS'))));
    }

    static private final function bakeUserCookie($time, User $user) {
        use_helper('Hash');
        $hash = new Crypt_Hash('sha256');

        return 'exp='.$time.'&id='.$user->id.'&digest='.bin2hex($hash->hash($user->username.$user->salt));
    }

    static public final function generateSalt($max = 32) {
        use_helper('Hash');
        $hash = new Crypt_Hash('sha256');

        $base = rand(0, 1000000) . microtime(true) . rand(0, 1000000) . rand(0, microtime(true));
        $salt = bin2hex($hash->hash($base));

        if($max < 32){
            $salt = substr($salt, 0, $max);
        }

        if($max > 32){
            $salt = substr($salt, 0, $max);
        }

        return $salt;
    }

    static public final function generateHashedPassword($password, $salt) {
        use_helper('Hash');

        $hash = new Crypt_Hash('sha512');

        return bin2hex($hash->hash($password.$salt));
    }

    static public final function validatePassword(User $user, $pwd) {
        if (empty($user->salt)) {
            return $user->password == sha1($pwd);
        }
        else {
            return $user->password == self::generateHashedPassword($pwd, $user->salt);
        }
    }

}

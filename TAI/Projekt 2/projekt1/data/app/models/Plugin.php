<?php
/**
 * @package Models
 */
class Plugin {
    static $plugins = array();
    static $plugins_infos = array();
    static $updatefile_cache = array();

    static $controllers = array();
    static $javascripts = array();
    static $stylesheets = array();

    static function init() {
        $dir = PLUGINS_ROOT.DS;

        if ($handle = opendir($dir)) {
            while (false !== ($plugin_id = readdir($handle))) {
                $file = $dir.$plugin_id.DS.'i18n'.DS.I18n::getLocale().'-message.php';
                $default_file = PLUGINS_ROOT.DS.$plugin_id.DS.'i18n'.DS.DEFAULT_LOCALE.'-message.php';
                
                if (file_exists($file)) {
                    $array = include $file;
                    I18n::add($array);
                }

                if (file_exists($default_file)) {
                    $array = include $default_file;
                    I18n::addDefault($array);
                }
            }
        }
        
        self::$plugins = unserialize(Setting::get('plugins'));
        foreach (self::$plugins as $plugin_id => $tmp) {
            $file = PLUGINS_ROOT.DS.$plugin_id.DS.'index.php';
            if (file_exists($file))
                include $file;
        }
    }

    static function setInfos($infos) {
        if (!isset($infos['type']) && defined('CMS_BACKEND')) {
            self::$plugins_infos[$infos['id']] = (object) $infos;
            return;
        }
        else if (!isset($infos['type'])) {
            return;
        }

        if (defined('CMS_BACKEND') && ($infos['type'] == 'backend' || $infos['type'] == 'both')) {
            self::$plugins_infos[$infos['id']] = (object) $infos;
            return;
        }

        if (!defined('CMS_BACKEND') && ($infos['type'] == 'frontend' || $infos['type'] == 'both')) {
            self::$plugins_infos[$infos['id']] = (object) $infos;
            return;
        }
    }

    static function activate($plugin_id) {
        self::$plugins[$plugin_id] = 1;
        self::save();

        $file = PLUGINS_ROOT.'/'.$plugin_id.'/enable.php';
        if (file_exists($file))
            include $file;

        if (isset(self::$controllers[$plugin_id])) {
            $class_name = Inflector::camelize($plugin_id).'Controller';
            AutoLoader::addFile($class_name, self::$controllers[$plugin_id]->file);
        }
    }

    static function deactivate($plugin_id) {
        if (isset(self::$plugins[$plugin_id])) {
            unset(self::$plugins[$plugin_id]);
            self::save();

            $file = PLUGINS_ROOT.'/'.$plugin_id.'/disable.php';
            if (file_exists($file))
                include $file;
        }
    }

    static function uninstall($plugin_id) {
        if (isset(self::$plugins[$plugin_id])) {
            unset(self::$plugins[$plugin_id]);
            self::save();
        }

        $file = PLUGINS_ROOT.'/'.$plugin_id.'/uninstall.php';
        if (file_exists($file)) {
            include $file;
        }
    }

    static function save() {
        Setting::saveFromData(array('plugins' => serialize(self::$plugins)));
    }

    static function findAll() {
        $dir = PLUGINS_ROOT.'/';

        if ($handle = opendir($dir)) {
            while (false !== ($plugin_id = readdir($handle))) {
                if ( ! isset(self::$plugins[$plugin_id]) && is_dir($dir.$plugin_id) && strpos($plugin_id, '.') !== 0) {
                    $file = PLUGINS_ROOT.'/'.$plugin_id.'/index.php';
                    if (file_exists($file))
                        include $file;
                }
            }
            closedir($handle);
        }

        ksort(self::$plugins_infos);
        return self::$plugins_infos;
    }

    public static function hasPrerequisites($plugin, &$errors=array()) {
        if (isset($plugin->require_wolf_version) && version_compare($plugin->require_wolf_version, CMS_VERSION, '>')) {
            $errors[] = __('The plugin requires a minimum of Wolf CMS version :v.', array(':v' => $plugin->require_wolf_version));
        }
        
        if (isset($plugin->require_php_extensions)) {
            $exts = explode(',', $plugin->require_php_extensions);
            if(!empty($exts)) {
                foreach ($exts as $ext) {
                    if (trim($ext) !== '' && !extension_loaded($ext)) {
                        $errors[] = __('One or more required PHP extension is missing: :exts', array(':exts', $plugin->require_php_extentions));
                    }
                }
            }
        }
        
        if (count($errors) > 0)
            return false;
        else return true;
    }

    static function addController($plugin_id, $label, $permissions=false, $show_tab=true) {
        if (!isset(self::$plugins_infos[$plugin_id])) return;

        $class_name = Inflector::camelize($plugin_id).'Controller';
        $file = PLUGINS_ROOT.'/'.$plugin_id.'/'.$class_name.'.php';

        if (!file_exists($file)) {
            if (defined('DEBUG') && DEBUG)
                throw new Exception('Plugin controller file not found: '.$file);
            return false;
        }

        self::$controllers[$plugin_id] = (object) array(
            'label' => ucfirst($label),
            'class_name' => $class_name,
            'file'	=> $file,
            'permissions' => $permissions,
            'show_tab' => $show_tab
        );

        AutoLoader::addFile($class_name, self::$controllers[$plugin_id]->file);

        return true;
    }

    static function addJavascript($plugin_id, $file) {
        if (file_exists(PLUGINS_ROOT.'/' . $plugin_id . '/' . $file)) {
            self::$javascripts[] = $plugin_id.'/'.$file;
        }
    }

    static function addStylesheet($plugin_id, $file) {
        if (file_exists(PLUGINS_ROOT.'/' . $plugin_id . '/' . $file)) {
            self::$stylesheets[] = $plugin_id.'/'.$file;
        }
    }


    static function hasSettingsPage($plugin_id) {
        $class_name = Inflector::camelize($plugin_id).'Controller';

        return (array_key_exists($plugin_id, Plugin::$controllers) && method_exists($class_name, 'settings'));
    }


    static function hasDocumentationPage($plugin_id) {
        $class_name = Inflector::camelize($plugin_id).'Controller';

        return (array_key_exists($plugin_id, Plugin::$controllers) && method_exists($class_name, 'documentation'));
    }

    static function isEnabled($plugin_id) {
        if (array_key_exists($plugin_id, Plugin::$plugins) && Plugin::$plugins[$plugin_id] == 1)
            return true;
        else
            return false;
    }

    static function deleteAllSettings($plugin_id) {
        if ($plugin_id === null || $plugin_id === '') return false;

        $tablename = TABLE_PREFIX.'plugin_settings';

        $sql = "DELETE FROM $tablename WHERE plugin_id=:pluginid";

        Record::logQuery($sql);

        $stmt = Record::getConnection()->prepare($sql);

        return $stmt->execute(array(':pluginid' => $plugin_id));
    }

    static function setAllSettings($array, $plugin_id) {

        if (!is_array($array) || !is_string($plugin_id)) return false;
        if (empty($array) || empty($plugin_id)) return false;

        $tablename = TABLE_PREFIX.'plugin_settings';

        $existingSettings = array();

        $sql = "SELECT name FROM $tablename WHERE plugin_id=:pluginid";

        Record::logQuery($sql);

        $stmt = Record::getConnection()->prepare($sql);

        $stmt->execute(array(':pluginid' => $plugin_id));

        while ($settingname = $stmt->fetchColumn())
            $existingSettings[$settingname] = $settingname;

        $ret = false;

        foreach ($array as $name => $value) {
            if (array_key_exists($name, $existingSettings)) {
                $sql = "UPDATE $tablename SET value=:value WHERE name=:name AND plugin_id=:pluginid";
            }
            else {
                $sql = "INSERT INTO $tablename (value, name, plugin_id) VALUES (:value, :name, :pluginid)";
            }

            Record::logQuery($sql);

            $stmt = Record::getConnection()->prepare($sql);

            $ret = $stmt->execute(array(':pluginid' => $plugin_id, ':name' => $name, ':value' => $value));
        }

        return $ret;
    }

    static function setSetting($name, $value, $plugin_id) {

        if (!is_string($name) || !is_string($value) || !is_string($plugin_id)) return false;
        if (empty($name) || empty($value) || empty($plugin_id)) return false;

        $tablename = TABLE_PREFIX.'plugin_settings';

        $existingSettings = array();

        $sql = "SELECT name FROM $tablename WHERE plugin_id=:pluginid";

        Record::logQuery($sql);

        $stmt = Record::getConnection()->prepare($sql);

        $stmt->execute(array(':pluginid' => $plugin_id));

        while ($settingname = $stmt->fetchColumn())
            $existingSettings[$settingname] = $settingname;

        if (in_array($name, $existingSettings)) {
            $sql = "UPDATE $tablename SET value=:value WHERE name=:name AND plugin_id=:pluginid";
        }
        else {
            $sql = "INSERT INTO $tablename (value, name, plugin_id) VALUES (:value, :name, :pluginid)";
        }


        Record::logQuery($sql);

        $stmt = Record::getConnection()->prepare($sql);

        return $stmt->execute(array(':pluginid' => $plugin_id, ':name' => $name, ':value' => $value));
    }

    static function getAllSettings($plugin_id=null) {
        if ($plugin_id == null) return false;

        $tablename = TABLE_PREFIX.'plugin_settings';

        $settings = array();

        $sql = "SELECT name,value FROM $tablename WHERE plugin_id=:pluginid";

        Record::logQuery($sql);

        $stmt = Record::getConnection()->prepare($sql);

        $stmt->execute(array(':pluginid' => $plugin_id));

        while ($obj = $stmt->fetchObject()) {
            $settings[$obj->name] = $obj->value;
        }

        return $settings;
    }

    static function getSetting($name=null, $plugin_id=null) {
        if ($name == null || $plugin_id == null) return false;

        $tablename = TABLE_PREFIX.'plugin_settings';

        $existingSettings = array();

        $sql = "SELECT value FROM $tablename WHERE plugin_id=:pluginid AND name=:name LIMIT 1";

        Record::logQuery($sql);

        $stmt = Record::getConnection()->prepare($sql);

        $stmt->execute(array(':pluginid' => $plugin_id, ':name' => $name));

        return $stmt->fetchColumn();
    }
}

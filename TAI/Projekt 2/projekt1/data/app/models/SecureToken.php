<?php

if (!defined('SECURE_TOKEN_EXPIRY')) define ('SECURE_TOKEN_EXPIRY', 900);  // 15 minutes

/**
 * @package Models
 */
final class SecureToken extends Record {
    const TABLE_NAME = 'secure_token';

    public $id;
    public $username;
    public $url;
    public $time;

    public static final function generateToken($url) {
        use_helper('Hash');
        $hash = new Crypt_Hash('sha256');
        AuthUser::load();
        
        if (AuthUser::isLoggedIn()) {
            $user = AuthUser::getRecord();
            $time = microtime(true);
            $target_url = str_replace('&amp;', '&', $url);
            $pwd = substr(bin2hex($hash->hash($user->password)), 5, 20);

            $oldtoken = SecureToken::getToken($user->username, $target_url);

            if (false === $oldtoken) {
                $oldtoken = new SecureToken();
                
                $oldtoken->username = $user->username;
                $oldtoken->url = bin2hex($hash->hash($target_url));
                $oldtoken->time = $time;

                $oldtoken->save();
            }
            else {
                $oldtoken->username = $user->username;
                $oldtoken->url = bin2hex($hash->hash($target_url));
                $oldtoken->time = $time;

                $oldtoken->save();
            }

            return bin2hex($hash->hash($user->username.$time.$target_url.$pwd.$user->salt));
        }
        
        return false;
    }

    public static final function validateToken($token, $url) {
        use_helper('Hash');
        $hash = new Crypt_Hash('sha256');
        AuthUser::load();
        
        if (AuthUser::isLoggedIn()) {
            $user = AuthUser::getRecord();
            $target_url = str_replace('&amp;', '&', $url);
            $pwd = substr(bin2hex($hash->hash($user->password)), 5, 20);

            $time = SecureToken::getTokenTime($user->username, $target_url);

            if ((microtime(true) - $time) > SECURE_TOKEN_EXPIRY) {
                return false;
            }

            if (!isset($user->salt)) {
                return (bin2hex($hash->hash($user->username.$time.$target_url.$pwd)) === $token);
            }
            else {
                return (bin2hex($hash->hash($user->username.$time.$target_url.$pwd.$user->salt)) === $token);
            }
        }

        return false;
    }

    private static final function getToken($username, $url) {
        use_helper('Hash');
        $hash = new Crypt_Hash('sha256');

        $token = false;
        $token = self::findOne(array(
            'where' => 'username = :username AND url = :url',
            'values' => array(':username' => $username,
                              ':url' => bin2hex($hash->hash($url))
            )
        ));

        if ($token !== null && $token !== false && $token instanceof SecureToken) {
            return $token;
        }

        return false;
    }

    private static final function getTokenTime($username, $url) {
        use_helper('Hash');
        $hash = new Crypt_Hash('sha256');
        $time = 0;

        $token = self::findOne(array(
            'where' => 'username = :username AND url = :url',
            'values' => array(
                ':username' => $username,
                ':url' => bin2hex($hash->hash($url))
            )
        ));

        if ($token) {
            $time = $token->time;
        }

        return $time;
    }
}

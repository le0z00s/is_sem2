<?php
/**
 * @package Models
 */

class UserRole extends Record {
    const TABLE_NAME = 'user_role';

    public $user_id = false;
    public $role_id = false;

    public static function setRolesFor($user_id, $roles) {

        Record::deleteWhere('UserRole', 'user_id = :user_id', array(':user_id' => (int) $user_id));

        foreach ($roles as $role => $role_id) {
            Record::insert('UserRole', array('user_id' => (int) $user_id, 'role_id' => (int) $role_id));
        }

    }
	
    public static function findByUserId($id) {
    	$tablename = self::tableNameFromClassName('UserRole');
    
    	return self::findOne(array(
    			'select' => "$tablename.*",
    			'where' => $tablename . '.user_id = :id',
    			'values' => array(':id' => $id)
    	));
    }
}

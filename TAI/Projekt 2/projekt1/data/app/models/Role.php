<?php
/**
 * @package Models
 */

class Role extends Record {
    const TABLE_NAME = 'role';

    public $id;
    public $name;
    public $permissions = false;

    public function  __toString() {
        return $this->name;
    }

    public function permissions() {
        if (!$this->permissions) {
            $this->permissions = array();

            foreach (RolePermission::findPermissionsFor($this->id) as $perm) {
                $this->permissions[$perm->name] = $perm;
            }
        }

        return $this->permissions;
    }

    public function hasPermission($permissions) {
        if (!$this->permissions)
            $this->permissions();

        foreach (explode(',', $permissions) as $permission) {
            if (array_key_exists($permission, $this->permissions))
                return true;
        }

        return false;
    }

    public static function findByName($name) {
        return self::findOne(array(
            'where'  => 'name = :name',
            'values' => array(':name' => $name)
        ));
    }

    public static function findByUserId($id) {

        $userroles = UserRole::find(array(
            'where'  => 'user_id = :user_id',
            'values' => array(':user_id' => (int) $id)
        ));

        if (count($userroles) <= 0)
            return false;

        $roles = array();

        foreach($userroles as $role) {
            $roles[] = Role::findById($role->role_id);
        }

        return $roles;
    }

    public function getColumns() {
        return array('id', 'name');
    }
}

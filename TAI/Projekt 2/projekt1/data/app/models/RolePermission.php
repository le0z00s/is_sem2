<?php
/**
 * @package Models
 */
class RolePermission extends Record {
    const TABLE_NAME = 'role_permission';

    public $role_id = false;
    public $permission_id = false;

    public static function savePermissionsFor($role_id, $permissions) {
        if (!Record::existsIn('Role', 'id = :role_id', array(':role_id' => $role_id)))
            return false;

        if (!self::deleteWhere('RolePermission', 'role_id = :role_id', array(':role_id' => (int) $role_id)))
            return false;

        foreach ($permissions as $perm) {
            $rp = new RolePermission(array('role_id' => $role_id, 'permission_id' => $perm->id));
            if (!$rp->save())
                return false;
        }

        return true;
    }

    public static function findPermissionsFor($role_id) {
        $roleperms = self::find(array(
            'where'  => 'role_id = :role_id',
            'values' => array(':role_id' => (int) $role_id)
        ));

        $perms = array();

        foreach($roleperms as $role => $perm) {
            $perms[] = Permission::findById($perm->permission_id);
        }

        return $perms;
    }

}

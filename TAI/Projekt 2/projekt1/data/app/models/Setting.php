<?php
/**
 * @package Models
 */

class Setting extends Record {
    const TABLE_NAME = 'setting';

    public $name;
    public $value;

    public static $settings = array();
    public static $is_loaded = false;

    public static function init() {
        if (! self::$is_loaded) {
            $settings = self::find();
            foreach($settings as $setting)
                self::$settings[$setting->name] = $setting->value;

            self::$is_loaded = true;
        }
    }

    public static function get($name) {
        return isset(self::$settings[$name]) ? self::$settings[$name]: false;
    }

    public static function saveFromData($data) {

        foreach ($data as $name => $value) {
            Record::update('Setting', array('value' => $value), 'name = :name', array(':name' => $name));
        }

    }

    public static function getLanguages() {
        $ietf = SettingController::$ietf;

        if ($handle = opendir(APP_PATH.'/i18n')) {
            while (false !== ($file = readdir($handle))) {
                if (strpos($file, '.') !== 0) {
                    $code = substr($file, 0, strpos($file, '-message.php'));
                    $languages[$code] = isset($ietf[$code]) ? $ietf[$code]: __('unknown');
                }
            }
            closedir($handle);
        }
        asort($languages);

        return $languages;
    }

    public static function getThemes() {
        $themes = array();
        $dir = CORE_ROOT.'/admin/themes/';
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if (strpos($file, '.') !== 0 && is_dir($dir.$file)) {
                    $themes[$file] = Inflector::humanize($file);
                }
            }
            closedir($handle);
        }
        asort($themes);

        return $themes;
    }

}

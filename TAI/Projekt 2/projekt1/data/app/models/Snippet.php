<?php
/**
 * @package Models
 */

class Snippet extends Record {
    const TABLE_NAME = 'snippet';

    public $name;
    public $content;
    public $content_html;

    public $created_on;
    public $updated_on;
    public $created_by_id;
    public $updated_by_id;

    public function beforeInsert() {
        $this->created_by_id = AuthUser::getId();
        $this->created_on = date('Y-m-d H:i:s');
        return true;
    }

    public function beforeUpdate() {
        $this->updated_by_id = AuthUser::getId();
        $this->updated_on = date('Y-m-d H:i:s');
        return true;
    }

    public function beforeSave() {
        if (empty($this->name)) {
            return false;
        }
        
        $this->content_html = $this->content;
        return true;
    }

    public static function findAll($args = null) {
        return self::find($args);
    }

    public static function findById($id) {
        $tablename = self::tableNameFromClassName('Snippet');
        $tablename_user = self::tableNameFromClassName('User');
        
        return self::findOne(array(
            'select' => "$tablename.*, creator.username AS created_by_name, updater.username AS updated_by_name",
            'joins' => "LEFT JOIN $tablename_user AS creator ON $tablename.created_by_id = creator.id ".
                       "LEFT JOIN $tablename_user AS updater ON $tablename.updated_by_id = updater.id",
            'where' => $tablename . '.id = ?',
            'values' => array((int) $id)
        ));
    }

    public static function findByName($name) {
        return self::findOne(array(
            'where' => 'name LIKE ?',
            'values' => array($name)
        ));
    }

    public function getColumns() {
        return array(
            'id', 'name', 'content', 'content_html',
            'created_on', 'updated_on', 'created_by_id', 'updated_by_id',
            'position'
        );
    }

}


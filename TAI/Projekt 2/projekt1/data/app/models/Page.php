<?php
/**
 * @package Models
 */

class Page extends Node {
    const TABLE_NAME = 'page';

    const STATUS_DRAFT = 1;
    const STATUS_PREVIEW = 10;
    const STATUS_PUBLISHED = 100;
    const STATUS_HIDDEN = 101;
    const STATUS_ARCHIVED = 200;

    const LOGIN_NOT_REQUIRED = 0;
    const LOGIN_REQUIRED = 1;
    const LOGIN_INHERIT = 2;

    public $id;
    public $title = '';
    public $slug = '';
    public $breadcrumb;
    public $keywords = '';
    public $description = '';
    public $content;
    public $parent_id;
    public $layout_id;
    public $behavior_id;
    public $status_id;
    public $comment_status;
    public $created_on;
    public $published_on;
    public $valid_until;
    public $updated_on;
    public $created_by_id;
    public $updated_by_id;
    public $position;
    public $is_protected;
    public $needs_login;
    public $author;
    public $author_id;
    public $updater;
    public $updater_id;
    // non db fields
    private $parent = false;
    private $path = false;
    private $level = false;
    private $tags = false;

    public function __construct($object=null, $parent=null) {
        if ($parent !== null) {
            $this->parent = $parent;
        }

        if ($object !== null) {
            foreach ($object as $key => $value) {
                $this->$key = $value;
            }
        }
    }


    public function id() {
        return $this->id;
    }


    public function author() {
        return $this->author;
    }


    public function authorId() {
        return $this->author_id;
    }


    public function title() {
        return $this->title;
    }


    public function description() {
        return $this->description;
    }


    public function keywords() {
        return $this->keywords;
    }


    public function parentId() {
        return $this->parent_id;
    }

    public function path() {
        if ($this->path === false) {
            if ($this->parent() !== false) {
                $this->path = trim($this->parent()->path().'/'.$this->slug, '/');
            } else {
                $this->path = trim($this->slug, '/');
            }
        }

        return $this->path;
    }

    public function uri() {
        return $this->path();
    }

    public function getUri() {
        return $this->path();
    }

    public function url($suffix=true) {
        if ($suffix === false) {
            return BASE_URL.$this->path();
        }
        else {
            return BASE_URL.$this->path().($this->path() != '' ? URL_SUFFIX : '');
        }
    }

    public static function urlById($id) {
        if (!is_numeric($id) || !is_int($id) || $id <= 0) {
            return '[urlById: id NAN or id <= 0]';
        }

        $page = self::findById($id);

        if (!$page)
            return '[urlById: no page with that id]';

        return $page->url();
    }


    public function slug() {
        return $this->slug;
    }


    public function breadcrumb() {
        return $this->breadcrumb;
    }


    public function updater() {
        return $this->updater;
    }


    public function updaterId() {
        return $this->updater_id;
    }

    public function breadcrumbs($separator='&gt;') {
        $out = '';
        $url = '';
        $path = '';
        $paths = explode('/', '/'.$this->slug);
        $nb_path = count($paths);

        if ($this->parent() !== false)
            $out .= $this->parent()->_inversedBreadcrumbs($separator);

        return $out.'<span class="breadcrumb-current">'.$this->breadcrumb().'</span>';
    }

    private function _inversedBreadcrumbs($separator) {
        $out = '<a href="'.$this->url().'" title="'.$this->breadcrumb.'">'.$this->breadcrumb.'</a><span class="breadcrumb-separator">'.$separator.'</span>';

        if ($this->parent() !== false)
            return $this->parent()->_inversedBreadcrumbs($separator).$out;

        return $out;
    }

    public function previous() {
        if ($this->parent() !== false) {
            return $this->parent()->children(array(
                'limit' => 1,
                'where' => 'page.position < '.$this->position.' AND page.id < '.$this->id,
                'order' => 'page.position DESC'
            ));
        }

        return false;
    }

    public function next() {
        if ($this->parent() !== false) {
            return $this->parent()->children(array(
                'limit' => 1,
                'where' => 'page.position > '.$this->position.' AND page.id > '.$this->id,
                'order' => 'page.position ASC'
            ));
        }

        return false;
    }

    public function childrenCount($args=null, $value=array(), $include_hidden=false) {

        // Collect attributes...
        $where = isset($args['where']) ? $args['where'] : '';
        $order = isset($args['order']) ? $args['order'] : 'position, id';
        $limit = isset($args['limit']) ? $args['limit'] : 0;
        $offset = isset($args['offset']) ? $args['offset'] : 0;

        // Prepare query parts
        $where_string = trim($where) == '' ? '' : "AND ".$where;
        $limit_string = $limit > 0 ? "LIMIT $limit" : '';
        $offset_string = $offset > 0 ? "OFFSET $offset" : '';

        // Prepare SQL
        $sql = 'SELECT COUNT(*) AS nb_rows FROM '.TABLE_PREFIX.'page '
                .'WHERE parent_id = '.$this->id
                ." AND (COALESCE(valid_until, '') = '' OR '".date('Y-m-d H:i:s')."' < valid_until)"
                .' AND (status_id='.Page::STATUS_PUBLISHED
                .($include_hidden ? ' OR status_id='.Page::STATUS_HIDDEN : '').') '
                ."$where_string ORDER BY $order $limit_string $offset_string";

        $stmt = Record::getConnection()->prepare($sql);

        $stmt->execute($value);

        return (int) $stmt->fetchColumn();
    }

    public function parent($level=null) {

        // check to see if it's already been retrieved, if not get the parent!
        if ($this->parent === false && $this->parent_id != 0) {
            $this->parent = self::findById($this->parentId());
        }

        if ($level === null)
            return $this->parent;

        if ($level > $this->level())
            return false;
        else if ($this->level() == $level)
            return $this;
        else
            return $this->parent->parent($level);
    }


    public function executionTime() {
        return execution_time();
    }

    public function includeSnippet($name) {
        $snippet = Snippet::findByName($name);

        if (false !== $snippet) {
            eval('?'.'>'.$snippet->content_html);
            return true;
        }

        return false;
    }


    private function _loadTags() {

        $this->tags = array();

        $sql = "SELECT tag.id AS id, tag.name AS tag FROM ".TABLE_PREFIX."page_tag AS page_tag, ".TABLE_PREFIX."tag AS tag ".
                "WHERE page_tag.page_id={$this->id} AND page_tag.tag_id = tag.id";

        if (!$stmt = Record::getConnection()->prepare($sql))
            return;

        $stmt->execute();

        // Run!
        while ($object = $stmt->fetchObject())
            $this->tags[$object->id] = $object->tag;
    }

    public function tags() {
        if ($this->tags === false)
            $this->_loadTags();

        return $this->tags;
    }

    public function getTags() {
        $tablename_page_tag = self::tableNameFromClassName('PageTag');
        $tablename_tag = self::tableNameFromClassName('Tag');

        $sql = "SELECT tag.id AS id, tag.name AS tag FROM $tablename_page_tag AS page_tag, $tablename_tag AS tag ".
                "WHERE page_tag.page_id={$this->id} AND page_tag.tag_id = tag.id";

        if (!$stmt = Record::getConnection()->prepare($sql))
            return array();

        $stmt->execute();

        // Run!
        $tags = array();
        while ($object = $stmt->fetchObject())
            $tags[$object->id] = $object->tag;

        return $tags;
    }

    public function getColumns() {
        return array(
            'id', 'title', 'slug', 'breadcrumb', 'keywords', 'description',
            'parent_id', 'layout_id', 'behavior_id', 'status_id',
            'created_on', 'published_on', 'valid_until', 'updated_on',
            'created_by_id', 'updated_by_id', 'position', 'is_protected',
            'needs_login'
        );
    }

    public function level() {
        if ($this->level === false) {
            $path = $this->path();
            $this->level = empty($path) ? 0 : substr_count($path, '/') + 1;
        }

        return $this->level;
    }

    public function date($format='%a, %e %b %Y', $which_one='created') {
        if ($which_one == 'update' || $which_one == 'updated')
            return strftime($format, strtotime($this->updated_on));
        else if ($which_one == 'publish' || $which_one == 'published')
            return strftime($format, strtotime($this->published_on));
        else if ($which_one == 'valid' || $which_one == 'valid')
            return strftime($format, strtotime($this->valid_until));
        else
            return strftime($format, strtotime($this->created_on));
    }

    public function content($part='body', $inherit=false) {
        // if part exist we generate the content en execute it!
        if (isset($this->part->$part)) {
            ob_start();
            eval('?'.'>'.$this->part->$part->content_html);
            $out = ob_get_contents();
            ob_end_clean();
            return $out;
        }
        else if ($inherit && $this->parent() !== false) {
            return $this->parent()->content($part, true);
        }
    }

    public function hasContent($part, $inherit=false) {
        if (isset($this->part->$part)) {
            $trim = trim($this->part->$part->content_html);
            if (!empty($trim)) {
                return true;
            }

            return false;
        }
        else if ($inherit && $this->parent() !== false) {
            return $this->parent()->hasContent($part, true);
        }
        return false;
    }

    public function partExists($part, $inherit=false) {
        if (isset($this->part->$part)) {
            return true;
        }
        else if ($inherit && $this->parent() !== false) {
            return $this->parent()->partExists($part, true);
        }
        return false;
    }

    public function link($label=null, $options='') {
        if ($label == null)
            $label = $this->title();

        return sprintf('<a href="%s" %s>%s</a>', $this->url(true), $options, $label
        );
    }

    public static function linkById($id, $label=null, $options='') {
        if (!is_numeric($id) || !is_int($id) || $id <= 0) {
            return '[linkById: id NAN or id <= 0]';
        }

        $page = self::findById($id);

        if ($label == null) {
            $label = $page->title();
        }

        return sprintf('<a href="%s" %s>%s</a>', $page->url(), $options, $label
        );
    }

    public function children($args=null, $value=array(), $include_hidden=false) {

        $page_class = 'Page';

        // Collect attributes...
        $where = isset($args['where']) ? $args['where'] : '';
        $order = isset($args['order']) ? $args['order'] : 'page.position ASC, page.id DESC';
        $offset = isset($args['offset']) ? $args['offset'] : 0;
        $limit = isset($args['limit']) ? $args['limit'] : 0;

        // auto offset generated with the page param
        if ($offset == 0 && isset($_GET['page']))
            $offset = ((int) $_GET['page'] - 1) * $limit;

        // Prepare query parts
        $where_string = trim($where) == '' ? '' : "AND ".$where;
        $limit_string = $limit > 0 ? "LIMIT $limit" : '';
        $offset_string = $offset > 0 ? "OFFSET $offset" : '';


        // Prepare SQL
        $sql = 'SELECT page.*, author.username AS author, author.id AS author_id, updater.username AS updater, updater.id AS updater_id '
                .'FROM '.TABLE_PREFIX.'page AS page '
                .'LEFT JOIN '.TABLE_PREFIX.'user AS author ON author.id = page.created_by_id '
                .'LEFT JOIN '.TABLE_PREFIX.'user AS updater ON updater.id = page.updated_by_id '
                .'WHERE parent_id = '.$this->id.' AND (status_id='.Page::STATUS_PUBLISHED.($include_hidden ? ' OR status_id='.Page::STATUS_HIDDEN : '').') '
                ." AND (COALESCE(valid_until, '') = '' OR '".date('Y-m-d H:i:s')."' < valid_until)"
                ."$where_string ORDER BY $order $limit_string $offset_string";

        self::logQuery($sql);

        $pages = array();

        // hack to be able to redefine the page class with behavior
        if (!empty($this->behavior_id)) {
            // will return Page by default (if not found!)
            $page_class = Behavior::loadPageHack($this->behavior_id);
        }

        // Run!
        if ($stmt = Record::getConnection()->prepare($sql)) {
            $stmt->execute($value);

            while ($object = $stmt->fetchObject()) {
                $page = new $page_class($object, $this);

                // assignParts
                $page->part = self::get_parts($page->id);
                $pages[] = $page;
            }
        }

        if ($limit == 1)
            return isset($pages[0]) ? $pages[0] : false;

        return $pages;
    }

    public function getLoginNeeded() {
        if ($this->needs_login == Page::LOGIN_INHERIT && $this->parent() !== false)
            return $this->parent()->getLoginNeeded();
        else
            return $this->needs_login;
    }


    public function _executeLayout() {

        $sql = 'SELECT content_type, content FROM '.TABLE_PREFIX.'layout WHERE id = :layout_id';

        $stmt = Record::getConnection()->prepare($sql);

        $stmt->execute(array(':layout_id' => $this->_getLayoutId()));

        if ($layout = $stmt->fetchObject()) {
            // if content-type not set, we set html as default
            if ($layout->content_type == '')
                $layout->content_type = 'text/html';

            // set content-type and charset of the page
            header('Content-Type: '.$layout->content_type.'; charset=UTF-8');

            Observer::notify('page_before_execute_layout', $layout);

            // execute the layout code
            eval('?'.'>'.$layout->content);
            // echo $layout->content;
        }
    }

    private function _getLayoutId() {
        if ($this->layout_id)
            return $this->layout_id;
        else if ($this->parent() !== false)
            return $this->parent()->_getLayoutId();
        else
            exit('You need to set a layout!');
    }


    public function beforeInsert() {
        $this->created_on = date('Y-m-d H:i:s');
        $this->created_by_id = AuthUser::getId();

        $this->updated_on = $this->created_on;
        $this->updated_by_id = $this->created_by_id;

        if ($this->status_id == Page::STATUS_PUBLISHED)
            $this->published_on = date('Y-m-d H:i:s');

        // Make sure we get a default position of 0;
        $this->position = 0;

        // Prevent certain stuff from entering the INSERT statement
        // @todo Replace by more appropriate use of Record::getColumns()
        unset($this->parent);
        unset($this->path);
        unset($this->level);
        unset($this->tags);

        return true;
    }


    public function beforeUpdate() {
        $this->created_on = $this->created_on.' '.$this->created_on_time;
        unset($this->created_on_time);

        if (!empty($this->published_on)) {
            $this->published_on = $this->published_on.' '.$this->published_on_time;
            unset($this->published_on_time);
        }
        else if ($this->status_id == Page::STATUS_PUBLISHED) {
            $this->published_on = date('Y-m-d H:i:s');
        }

        if (!empty($this->valid_until)) {
            $this->valid_until = $this->valid_until.' '.$this->valid_until_time;
            unset($this->valid_until_time);
            if ($this->valid_until < date('Y-m-d H:i:s')) {
                $this->status_id = Page::STATUS_ARCHIVED;
            }
        }
        unset($this->valid_until_time);

        $this->updated_by_id = AuthUser::getId();
        $this->updated_on = date('Y-m-d H:i:s');

        unset($this->path);
        unset($this->level);
        unset($this->tags);
        unset($this->parent);

        return true;
    }


    public function beforeDelete() {
        $ret = false;

        $ret = self::deleteChildrenOf($this->id);
        $ret = PagePart::deleteByPageId($this->id);
        $ret = PageTag::deleteByPageId($this->id);

        return $ret;
    }


    public function setTags($tags) {
        if (is_string($tags))
            $tags = explode(',', $tags);

        $tags = array_map('trim', $tags);

        $current_tags = $this->getTags();

        // no tag before! no tag now! ... nothing to do!
        if (count($tags) == 0 && count($current_tags) == 0)
            return;

        // delete all tags
        if (count($tags) == 0) {
            $tablename = self::tableNameFromClassName('Tag');

            // update count (-1) of those tags
            foreach ($current_tags as $tag)
                Record::update('Tag', array('count' => 'count - 1'), 'name = :tag_name', array(':tag_name' => $tag));

            return Record::deleteWhere('PageTag', 'page_id = :page_id', array(':page_id' => $this->id));
        }
        else {
            $old_tags = array_diff($current_tags, $tags);
            $new_tags = array_diff($tags, $current_tags);

            // insert all tags in the tag table and then populate the page_tag table
            foreach ($new_tags as $index => $tag_name) {
                if (!empty($tag_name)) {
                    // try to get it from tag list, if not we add it to the list
                    if (!$tag = Tag::findByName($tag_name))
                        $tag = new Tag(array('name' => trim($tag_name)));

                    $tag->count++;
                    $tag->save();

                    // create the relation between the page and the tag
                    $tag = new PageTag(array('page_id' => $this->id, 'tag_id' => $tag->id));
                    $tag->save();
                }
            }

            // remove all old tag
            foreach ($old_tags as $index => $tag_name) {
                // get the id of the tag
                $tag = Tag::findByName($tag_name);
                // delete the pivot record
                Record::deleteWhere('PageTag', 'page_id = :page_id AND tag_id = :tag_id', array(':page_id' => $this->id, ':tag_id' => $tag->id));
                $tag->count--;
                $tag->save();
            }
        }
    }

    public function saveTags($tags) {
        return $this->setTags($tags);
    }

    public static function find_page_by_uri($uri) {
        return Page::findByPath($uri);
    }

    public static function findByUri($uri, $all = false) {
        return self::findByPath($uri, $all);
    }

    public static function findByPath($path, $all = false) {
        $path = trim($path, '/');
        $has_behavior = false;

        // adding the home root
        $slugs = array_merge(array(''), explode_path($path));
        $url = '';

        $page = new stdClass;
        $page->id = 0;

        $parent = false;

        foreach ($slugs as $slug) {
            $url = ltrim($url . '/' . $slug, '/');

            $page = self::findBySlug($slug, $parent, $all);
            if ($page instanceof Page) {
                // check for behavior
                if ($page->behavior_id != '') {
                    // add an instance of the behavior with the name of the behavior
                    $params = explode_path(substr($path, strlen($url)));
                    $page->{$page->behavior_id} = Behavior::load($page->behavior_id, $page, $params);

                    return $page;
                }
            } else {
                break;
            }

            $parent = $page;
        } // foreach

        return $page;
    }

    public static function findBySlug($slug, &$parent, $all = false) {

        if (empty($slug)) {
            return self::findOne(array(
                'where' => 'parent_id=0'
            ));
        }

        $parent_id = $parent ? $parent->id : 0;

        if ($all) {
            return self::findOne(array(
                'where' => 'slug = :slug AND parent_id = :parent_id AND (status_id = :status_preview OR status_id = :status_published OR status_id = :status_hidden)',
                'values' => array(':slug' => $slug,
                    ':parent_id' => (int) $parent_id,
                    ':status_preview' => self::STATUS_PREVIEW,
                    ':status_published' => self::STATUS_PUBLISHED,
                    ':status_hidden' => self::STATUS_HIDDEN
                )
            ));
        }
        else {
            return self::findOne(array(
                'where' => 'slug = :slug AND parent_id = :parent_id AND (status_id = :status_published OR status_id = :status_hidden)',
                'values' => array(':slug' => $slug,
                    ':parent_id' => (int) $parent_id,
                    ':status_published' => self::STATUS_PUBLISHED,
                    ':status_hidden' => self::STATUS_HIDDEN
                )
            ));
        }
    }

    public static function find($options = null) {
        if (!is_array($options)) {
            // Assumes find was called with a uri
            return Page::findByPath($options);
        }

        $options = (is_null($options)) ? array() : $options;
        
        $class_name = get_called_class();
        $table_name = self::tableNameFromClassName($class_name);
        
        // Collect attributes
        $ses    = isset($options['select']) ? trim($options['select'])   : '';
        $frs    = isset($options['from'])   ? trim($options['from'])     : '';
        $jos    = isset($options['joins'])  ? trim($options['joins'])    : '';
        $whs    = isset($options['where'])  ? trim($options['where'])    : '';
        $gbs    = isset($options['group'])  ? trim($options['group'])    : '';
        $has    = isset($options['having']) ? trim($options['having'])   : '';
        $obs    = isset($options['order'])  ? trim($options['order'])    : '';
        $lis    = isset($options['limit'])  ? (int) $options['limit']    : 0;
        $ofs    = isset($options['offset']) ? (int) $options['offset']   : 0;
        $values = isset($options['values']) ? (array) $options['values'] : array();
        
        $single = ($lis === 1) ? true : false;
        $tablename_user = self::tableNameFromClassName('User');

        $jos .= " LEFT JOIN $tablename_user AS creator ON page.created_by_id = creator.id";
        $jos .= " LEFT JOIN $tablename_user AS updater ON page.updated_by_id = updater.id";
        
        // Prepare query parts
        // @todo Remove all "author" mentions and function and replace by more appropriate "creator" name.
        $select     = empty($ses) ? 'SELECT page.*, creator.username AS author, creator.id AS author_id, updater.username AS updater, updater.id AS updater_id, creator.username AS created_by_name, updater.username AS updated_by_name' : "SELECT $ses";
        $from       = empty($frs) ? "FROM $table_name AS page" : "FROM $frs";
        $joins      = empty($jos) ? '' : $jos;
        $where      = empty($whs) ? '' : "WHERE $whs";
        $group_by   = empty($gbs) ? '' : "GROUP BY $gbs";
        $having     = empty($has) ? '' : "HAVING $has";
        $order_by   = empty($obs) ? '' : "ORDER BY $obs";
        $limit      = $lis > 0 ? "LIMIT $lis" : '';
        $offset     = $ofs > 0 ? "OFFSET $ofs" : '';
        
        // Compose the query
        $sql = "$select $from $joins $where $group_by $having $order_by $limit $offset";
        
        $objects = self::findBySql($sql, $values);
        
        return ($single) ? (!empty($objects) ? $objects[0] : false) : $objects;
    }

    private static function findBySql($sql, $values = null) {
        $class_name = get_called_class();
        
        Record::logQuery($sql);
        
        // Prepare and execute
        $stmt = Record::getConnection()->prepare($sql);
        if (!$stmt->execute($values)) {
            return false;
        }

        $page_class = 'Page';
        
        $objects = array();
        while ($page = $stmt->fetchObject('Page')) {
            $parent = $page->parent();
            if (!empty($parent->behavior_id)) {
                // will return Page by default (if not found!)
                $page_class = Behavior::loadPageHack($parent->behavior_id);
            }

            // create the object page
            $page = new $page_class($page, $parent);
            $page->part = self::get_parts($page->id);
            $objects[] = $page;
        }
        
        return $objects;
    }

    public static function findAll($args = null) {
        return self::find($args);
    }


    public static function findById($id) {
        return self::findOne(array(
            'where' => 'page.id = :id',
            'values' => array(':id' => (int) $id)
        ));
    }

    public static function findByBehaviour($name, $parent_id=false) {
        if ($parent_id !== false && is_int($parent_id)) {
            return self::findOne(array(
                'where' => 'behavior_id = :behavior_id AND parent_id = :parent_id',
                'values' => array(':behavior_id' => $name, ':parent_id' => (int) $parent_id)
            ));
        } else {
            return self::findOne(array(
                'where' => 'behavior_id = :behavior_id',
                'values' => array(':behavior_id' => $name)
            ));
        }
    }

    public static function findOne($options = array()) {
        $options['limit'] = 1;
        return self::find($options);
    }

    public static function childrenOf($parent_id) {
        return self::find(array(
            'where' => 'parent_id = :parent_id',
            'values' => array(':parent_id' => $parent_id),
            'order' => 'page.position ASC, page.id DESC'
        ));
    }


    public static function hasChildren($id) {
        return (boolean) self::countFrom('Page', 'parent_id = :parent_id', array(':parent_id' => (int) $id));
    }


    public static function deleteChildrenOf($id) {
        $id = (int) $id;

        if (self::hasChildren($id)) {
            $children = self::childrenOf($id);
            if (is_array($children)) {
                foreach ($children as $child) {
                    if (!$child->delete()) {
                        return false;
                    }
                }
            }
            elseif ($children instanceof Page) { // because Page::childrenOf return directly an object when there is only 1 child...
                if (!$children->delete()) {
                    return false;
                }
            }
        }

        return true;
    }


    public static function cloneTree($page, $parent_id) {
        static $new_root_id = false;

        $clone = Page::findById($page->id);
        $clone->parent_id = (int) $parent_id;
        $clone->id = null;

        if (!$new_root_id) {
            $clone->title .= " (copy)";
            $clone->slug .= "-copy";
        }
        $clone->save();

        $page_part = PagePart::findByPageId($page->id);
        if (count($page_part)) {
            foreach ($page_part as $part) {
                $part->page_id = $clone->id;
                $part->id = null;
                $part->save();
            }
        }

        $page_tags = $page->getTags();
        if (count($page_tags)) {
            foreach ($page_tags as $tag_id => $tag_name) {
                $tag = new PageTag(array('page_id' => $clone->id, 'tag_id' => $tag_id));
                $tag->save();
            }
        }

        if (!$new_root_id) {
            $new_root_id = $clone->id;
        }

        if (Page::hasChildren($page->id)) {
            foreach (Page::childrenOf($page->id) as $child) {
                Page::cloneTree($child, $clone->id);
            }
        }

        return $new_root_id;
    }


    public static function get_parts($page_id) {

        $objPart = new stdClass;

        $sql = 'SELECT name, content_html FROM '.TABLE_PREFIX.'page_part WHERE page_id = :page_id';

        if ($stmt = Record::getConnection()->prepare($sql)) {
            $stmt->execute(array(':page_id' => $page_id));

            while ($part = $stmt->fetchObject())
                $objPart->{$part->name} = $part;
        }

        return $objPart;
    }

}


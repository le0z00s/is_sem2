<?php
/**
 * @package Models
 */

class Behavior {
    private static $loaded_files = array();
    private static $behaviors = array();

    public static function add($behavior_id, $file) {
        self::$behaviors[$behavior_id] = $file;
    }

    public static function remove($behavior_id) {
        if (isset(self::$behaviors[$behavior_id]))
            unset(self::$behaviors[$behavior_id]);
    }

    public static function load($behavior_id, &$page, $params) {
        if ( ! empty(self::$behaviors[$behavior_id])) {
            $file = CORE_ROOT.'/plugins/'.self::$behaviors[$behavior_id];
            $behavior_class = Inflector::camelize($behavior_id);

            if (isset(self::$loaded_files[$file]))
                return new $behavior_class($page, $params);

            if (file_exists($file)) {
                include $file;
                self::$loaded_files[$file] = true;
                return new $behavior_class($page, $params);
            }
            else {
                exit ("Behavior $behavior_id not found!");
            }
        }
    }

    public static function loadPageHack($behavior_id) {
        $behavior_page_class = Inflector::camelize('page_'.$behavior_id);

        if (class_exists($behavior_page_class, false))
            return $behavior_page_class;
        else
            return 'Page';
    }

    public static function findAll() {
        return array_keys(self::$behaviors);
    }

}

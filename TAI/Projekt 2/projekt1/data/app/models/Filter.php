<?php
/**
 * @package Models
 */
class Filter {
    static $filters = array();
    private static $filters_loaded = array();

    
    public static function add($filter_id, $file) {
        self::$filters[$filter_id] = $file;
    }

    public static function remove($filter_id) {
        if (isset(self::$filters[$filter_id]))
            unset(self::$filters[$filter_id]);
    }

    public static function findAll() {
        return array_keys(self::$filters);
    }

    public static function get($filter_id) {
        if ( ! isset(self::$filters_loaded[$filter_id])) {
            if (isset(self::$filters[$filter_id])) {
                $file = CORE_ROOT.'/plugins/'.self::$filters[$filter_id];
                if (file_exists($file)) {
                    include $file;

                    $filter_class = Inflector::camelize($filter_id);
                    self::$filters_loaded[$filter_id] = new $filter_class();
                    return self::$filters_loaded[$filter_id];
                }
            }
            else return false;
        }
        else return self::$filters_loaded[$filter_id];
    }

}


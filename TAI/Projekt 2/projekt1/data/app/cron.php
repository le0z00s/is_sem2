<?php
/**
 * @package CMS
 */

set_time_limit(86400);

ob_end_clean();
header("content-type: image/gif");
header("Connection: close");
ignore_user_abort(true);
ob_start();
echo base64_decode("R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");
$size = ob_get_length();
header("Content-Length: $size");
ob_end_flush();
flush();

define('IN_CMS', true);
define('DS', DIRECTORY_SEPARATOR);
define('CMS_ROOT', dirname(__FILE__).'/../..');
define('CORE_ROOT', CMS_ROOT.DS.'data');
define('PLUGINS_ROOT', CORE_ROOT.DS.'plugins');
define('APP_PATH',  CORE_ROOT.DS.'app');

require_once(CORE_ROOT.DS.'utils.php');
require_once(CMS_ROOT.DS.'config.php');

define('BASE_URL', URL_PUBLIC . (endsWith(URL_PUBLIC, '/') ? '': '/') . (USE_MOD_REWRITE ? '': '?'));

require CORE_ROOT.DS.'Main.php';

$__CMS_CONN__ = new PDO(DB_DSN, DB_USER, DB_PASS);
if ($__CMS_CONN__->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql')
    $__CMS_CONN__->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);

Record::connection($__CMS_CONN__);
Record::getConnection()->exec("set names 'utf8'");

use_helper('I18n');
Setting::init();
Plugin::init();

$cron = Cron::findByIdFrom('Cron', '1');
$cron->save();

Observer::notify('cron_run');

?>

<?php
/**
 * @package    org.cms.core
 * @subpackage main
 */

define('MAIN_STARTING_MICROTIME', get_microtime());

if (!defined('DEBUG'))              define('DEBUG', false);

if (!defined('CORE_ROOT'))          define('CORE_ROOT',   dirname(__FILE__));

if (defined('GLOBAL_XSS_FILTERING') && GLOBAL_XSS_FILTERING) {
    cleanXSS();
}

if (!defined('APP_PATH'))           define('APP_PATH',    CORE_ROOT.DIRECTORY_SEPARATOR.'app');
if (!defined('HELPER_PATH'))        define('HELPER_PATH', CORE_ROOT.DIRECTORY_SEPARATOR.'helpers');

if (!defined('BASE_URL'))           define('BASE_URL', 'http://'.dirname($_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']) .'/?/');

if (!defined('DEFAULT_CONTROLLER')) define('DEFAULT_CONTROLLER', 'index');
if (!defined('DEFAULT_ACTION'))     define('DEFAULT_ACTION', 'index');

error_reporting((DEBUG ? (E_ALL | E_STRICT) : 0));

if (PHP_VERSION < 5.3)
    set_magic_quotes_runtime(0);

if ( ! isset($_SESSION))
    session_start();

if ( ! isset($_SESSION['initiated'])) {
    session_regenerate_id(true);
    $_SESSION['initiated'] = true;
}

ini_set('date.timezone', DEFAULT_TIMEZONE);
if(function_exists('date_default_timezone_set'))
    date_default_timezone_set(DEFAULT_TIMEZONE);
else
    putenv('TZ='.DEFAULT_TIMEZONE);


final class Dispatcher {
    private static $routes = array();
    private static $params = array();
    private static $status = array();
    private static $requested_url = '';

    public static function addRoute($route, $destination=null) {
        if ($destination != null && !is_array($route)) {
            $route = array($route => $destination);
        }
        self::$routes = array_merge(self::$routes, $route);
    }

    public static function hasRoute($requested_url) {
        if (!self::$routes || count(self::$routes) == 0) {
            return false;
        }

        $requested_url = rtrim($requested_url, '/');

        foreach (self::$routes as $route => $action) {
            if (strpos($route, ':') !== false) {
                $route = str_replace(':any', '([^/]+)', str_replace(':num', '([0-9]+)', str_replace(':all', '(.+)', $route)));
            }

            if (preg_match('#^'.$route.'$#', $requested_url)) {
                if (strpos($action, '$') !== false && strpos($route, '(') !== false) {
                    $action = preg_replace('#^'.$route.'$#', $action, $requested_url);
                }
                self::$params = self::splitUrl($action);
                return true;
            }
        }

        return false;
    }

    public static function splitUrl($url) {
        return preg_split('/\//', $url, -1, PREG_SPLIT_NO_EMPTY);
    }

    public static function dispatch($requested_url = null, $default = null) {
        Flash::init();

        if ($requested_url === null) {
            $pos = strpos($_SERVER['QUERY_STRING'], '&');
            if ($pos !== false) {
                $requested_url = substr($_SERVER['QUERY_STRING'], 0, $pos);
            } else {
                $requested_url = $_SERVER['QUERY_STRING'];
            }
        }

        if ($requested_url == null && $default != null) {
            $requested_url = $default;
        }

        if (strpos($requested_url, '/') !== 0) {
            $requested_url = '/' . $requested_url;
        }
        
        $requested_url = rtrim($requested_url, '/');

        self::$requested_url = $requested_url;

        self::$status['requested_url'] = $requested_url;

        self::$params = self::splitUrl($requested_url);

        if (count(self::$routes) === 0) {
            return self::executeAction(self::getController(), self::getAction(), self::getParams());
        }

        if (isset(self::$routes[$requested_url])) {
            self::$params = self::splitUrl(self::$routes[$requested_url]);
            return self::executeAction(self::getController(), self::getAction(), self::getParams());
        }

        foreach (self::$routes as $route => $action) {

            if (strpos($route, ':') !== false) {
                $route = str_replace(':any', '([^/]+)', str_replace(':num', '([0-9]+)', str_replace(':all', '(.+)', $route)));
            }

            if (preg_match('#^'.$route.'$#', $requested_url)) {

                if (strpos($action, '$') !== false && strpos($route, '(') !== false) {
                    $action = preg_replace('#^'.$route.'$#', $action, $requested_url);
                }
                self::$params = self::splitUrl($action);

                break;
            }
        }

        return self::executeAction(self::getController(), self::getAction(), self::getParams());
    } 

    public static function getCurrentUrl() {
        return self::$requested_url;
    }

    public static function getController() {

        if (isset(self::$params[0]) && self::$params[0] == 'plugin' ) {
            $loaded_plugins = Plugin::$plugins;
            if (count(self::$params) < 2) {
                unset(self::$params[0]);
            } elseif (isset(self::$params[1]) && !isset($loaded_plugins[self::$params[1]])) {
                unset(self::$params[0]);
                unset(self::$params[1]);
            }
        }

        return isset(self::$params[0]) ? self::$params[0]: DEFAULT_CONTROLLER;
    }

    public static function getAction() {
        return isset(self::$params[1]) ? self::$params[1]: DEFAULT_ACTION;
    }

    public static function getParams() {
        return array_slice(self::$params, 2);
    }

    public static function getStatus($key=null) {
        return ($key === null) ? self::$status: (isset(self::$status[$key]) ? self::$status[$key]: null);
    }

    public static function executeAction($controller, $action, $params) {
        self::$status['controller'] = $controller;
        self::$status['action'] = $action;
        self::$status['params'] = implode(', ', $params);

        $controller_class = Inflector::camelize($controller);
        $controller_class_name = $controller_class . 'Controller';

        if (class_exists($controller_class_name)) {
            $controller = new $controller_class_name();
        } else {
        }
        if ( ! $controller instanceof Controller) {
            throw new Exception("Class '{$controller_class_name}' does not extends Controller class!");
        }

        $controller->execute($action, $params);
    }

} 

class Record {
    const PARAM_BOOL = 5;
    const PARAM_NULL = 0;
    const PARAM_INT = 1;
    const PARAM_STR = 2;
    const PARAM_LOB = 3;
    const PARAM_STMT = 4;
    const PARAM_INPUT_OUTPUT = -2147483648;
    const PARAM_EVT_ALLOC = 0;
    const PARAM_EVT_FREE = 1;
    const PARAM_EVT_EXEC_PRE = 2;
    const PARAM_EVT_EXEC_POST = 3;
    const PARAM_EVT_FETCH_PRE = 4;
    const PARAM_EVT_FETCH_POST = 5;
    const PARAM_EVT_NORMALIZE = 6;

    const FETCH_LAZY = 1;
    const FETCH_ASSOC = 2;
    const FETCH_NUM = 3;
    const FETCH_BOTH = 4;
    const FETCH_OBJ = 5;
    const FETCH_BOUND = 6;
    const FETCH_COLUMN = 7;
    const FETCH_CLASS = 8;
    const FETCH_INTO = 9;
    const FETCH_FUNC = 10;
    const FETCH_GROUP = 65536;
    const FETCH_UNIQUE = 196608;
    const FETCH_CLASSTYPE = 262144;
    const FETCH_SERIALIZE = 524288;
    const FETCH_PROPS_LATE = 1048576;
    const FETCH_NAMED = 11;

    const ATTR_AUTOCOMMIT = 0;
    const ATTR_PREFETCH = 1;
    const ATTR_TIMEOUT = 2;
    const ATTR_ERRMODE = 3;
    const ATTR_SERVER_VERSION = 4;
    const ATTR_CLIENT_VERSION = 5;
    const ATTR_SERVER_INFO = 6;
    const ATTR_CONNECTION_STATUS = 7;
    const ATTR_CASE = 8;
    const ATTR_CURSOR_NAME = 9;
    const ATTR_CURSOR = 10;
    const ATTR_ORACLE_NULLS = 11;
    const ATTR_PERSISTENT = 12;
    const ATTR_STATEMENT_CLASS = 13;
    const ATTR_FETCH_TABLE_NAMES = 14;
    const ATTR_FETCH_CATALOG_NAMES = 15;
    const ATTR_DRIVER_NAME = 16;
    const ATTR_STRINGIFY_FETCHES = 17;
    const ATTR_MAX_COLUMN_LEN = 18;
    const ATTR_EMULATE_PREPARES = 20;
    const ATTR_DEFAULT_FETCH_MODE = 19;

    const ERRMODE_SILENT = 0;
    const ERRMODE_WARNING = 1;
    const ERRMODE_EXCEPTION = 2;
    const CASE_NATURAL = 0;
    const CASE_LOWER = 2;
    const CASE_UPPER = 1;
    const NULL_NATURAL = 0;
    const NULL_EMPTY_STRING = 1;
    const NULL_TO_STRING = 2;
    const ERR_NONE = '00000';
    const FETCH_ORI_NEXT = 0;
    const FETCH_ORI_PRIOR = 1;
    const FETCH_ORI_FIRST = 2;
    const FETCH_ORI_LAST = 3;
    const FETCH_ORI_ABS = 4;
    const FETCH_ORI_REL = 5;
    const CURSOR_FWDONLY = 0;
    const CURSOR_SCROLL = 1;
    const MYSQL_ATTR_USE_BUFFERED_QUERY = 1000;
    const MYSQL_ATTR_LOCAL_INFILE = 1001;
    const MYSQL_ATTR_INIT_COMMAND = 1002;
    const MYSQL_ATTR_READ_DEFAULT_FILE = 1003;
    const MYSQL_ATTR_READ_DEFAULT_GROUP = 1004;
    const MYSQL_ATTR_MAX_BUFFER_SIZE = 1005;
    const MYSQL_ATTR_DIRECT_QUERY = 1006;

    public static $__CONN__ = false;
    public static $__QUERIES__ = array();

    final public static function connection($connection) {
        self::$__CONN__ = $connection;
    }

    final public static function getConnection() {
        return self::$__CONN__;
    }

    final public static function logQuery($sql) {
        self::$__QUERIES__[] = $sql;
    }

    final public static function getQueryLog() {
        return self::$__QUERIES__;
    }

    final public static function getQueryCount() {
        return count(self::$__QUERIES__);
    }

    final public static function query($sql, $values=false) {
        self::logQuery($sql);

        if (is_array($values)) {
            $stmt = self::$__CONN__->prepare($sql);
            $stmt->execute($values);
            return $stmt->fetchAll(self::FETCH_OBJ);
        } else {
            return self::$__CONN__->query($sql);
        }
    }

    final public static function tableNameFromClassName($class_name) {
        try {
            if (class_exists($class_name) && defined($class_name.'::TABLE_NAME'))
                return TABLE_PREFIX.constant($class_name.'::TABLE_NAME');
        }
        catch (Exception $e) {
            return TABLE_PREFIX.Inflector::underscore($class_name);
        }
    }

    final public static function escape($value) {
        return self::$__CONN__->quote($value);
    }

    final public static function lastInsertId() {
        return self::$__CONN__->lastInsertId();
    }

    public function __construct($data=false) {
        if (is_array($data)) {
            $this->setFromData($data);
        }
    }

    public function setFromData($data) {
        foreach($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function save() {
        if ( ! $this->beforeSave()) return false;

        $value_of = array();

        if (empty($this->id)) {

            if ( ! $this->beforeInsert()) return false;

            $columns = $this->getColumns();

            foreach ($columns as $column) {
                if ($column === 'id') {
                    continue;
                }
                
                if (!empty($this->$column) || is_numeric($this->$column)) {
                    $value_of[$column] = self::$__CONN__->quote($this->$column);
                }
                elseif (isset($this->$column)) { 
                    if (self::$__CONN__->getAttribute(PDO::ATTR_DRIVER_NAME) != 'sqlite') {
                        $value_of[$column] = 'DEFAULT';
                    }
                }
            }

            $sql = 'INSERT INTO '.self::tableNameFromClassName(get_class($this)).' ('
                . implode(', ', array_keys($value_of)).') VALUES ('.implode(', ', array_values($value_of)).')';
            $return = self::$__CONN__->exec($sql) !== false;
            $this->id = self::lastInsertId();

            if ( ! $this->afterInsert()) return false;

        } else {

            if ( ! $this->beforeUpdate()) return false;

            $columns = $this->getColumns();

            foreach ($columns as $column) {
                if (!empty($this->$column) || is_numeric($this->$column)) {
                    $value_of[$column] = $column.'='.self::$__CONN__->quote($this->$column);
                }
                elseif (isset($this->$column)) {
                    if (self::$__CONN__->getAttribute(PDO::ATTR_DRIVER_NAME) != 'sqlite') {
                        $value_of[$column] = $column.'=DEFAULT';
                    }
					else{
						$value_of[$column] = $column."=''";
					}
                }
            }

            unset($value_of['id']);

            $sql = 'UPDATE '.self::tableNameFromClassName(get_class($this)).' SET '
                . implode(', ', $value_of).' WHERE id = '.$this->id;
            $return = self::$__CONN__->exec($sql) !== false;

            if ( ! $this->afterUpdate()) return false;
        }

        self::logQuery($sql);

        if ( ! $this->afterSave()) return false;

        return $return;
    }

    public function delete() {
        if ( ! $this->beforeDelete()) return false;
        $sql = 'DELETE FROM '.self::tableNameFromClassName(get_class($this))
            . ' WHERE id='.self::$__CONN__->quote($this->id);

        $return = self::$__CONN__->exec($sql) !== false;
        if ( ! $this->afterDelete()) {
            $this->save();
            return false;
        }

        self::logQuery($sql);

        return $return;
    }

    public function beforeSave() { return true; }

    public function beforeInsert() { return true; }

    public function beforeUpdate() { return true; }

    public function beforeDelete() { return true; }

    public function afterSave() { return true; }

    public function afterInsert() { return true; }

    public function afterUpdate() { return true; }

    public function afterDelete() { return true; }

    public function getColumns() {
        return array_keys(get_object_vars($this));
    }

    public static function insert($class_name, $data) {
        $keys = array();
        $values = array();

        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = self::$__CONN__->quote($value);
        }

        $sql = 'INSERT INTO '.self::tableNameFromClassName($class_name).' ('.join(', ', $keys).') VALUES ('.join(', ', $values).')';

        self::logQuery($sql);

        return self::$__CONN__->exec($sql) !== false;
    }

    public static function update($class_name, $data, $where, $values=array()) {
        $setters = array();

        foreach ($data as $key => $value) {
            $setters[] = $key.'='.self::$__CONN__->quote($value);
        }

        $sql = 'UPDATE '.self::tableNameFromClassName($class_name).' SET '.join(', ', $setters).' WHERE '.$where;

        self::logQuery($sql);

        $stmt = self::$__CONN__->prepare($sql);
        return $stmt->execute($values);
    }

    public static function deleteWhere($class_name, $where, $values=array()) {
        $sql = 'DELETE FROM '.self::tableNameFromClassName($class_name).' WHERE '.$where;

        self::logQuery($sql);

        $stmt = self::$__CONN__->prepare($sql);
        return $stmt->execute($values);
    }

	public static function existsIn($class_name, $where=false, $values=array()) {
		$sql = 'SELECT EXISTS(SELECT 1 FROM '.self::tableNameFromClassName($class_name).($where ? ' WHERE '.$where:'').' LIMIT 1)';

        $stmt = self::$__CONN__->prepare($sql);
		$stmt->execute($values);

        self::logQuery($sql);

        return (bool) $stmt->fetchColumn();
	}

    public static function find($options = array()) {
        // @todo Replace by InvalidArgumentException if not array based on logger decision.
        $options = (is_null($options)) ? array() : $options;
        
        $class_name = get_called_class();
        $table_name = self::tableNameFromClassName($class_name);
        
        $ses    = isset($options['select']) ? trim($options['select'])   : '';
        $frs    = isset($options['from'])   ? trim($options['from'])     : '';
        $jos    = isset($options['joins'])  ? trim($options['joins'])    : '';       
        $whs    = isset($options['where'])  ? trim($options['where'])    : '';
        $gbs    = isset($options['group'])  ? trim($options['group'])    : '';
        $has    = isset($options['having']) ? trim($options['having'])   : '';
        $obs    = isset($options['order'])  ? trim($options['order'])    : '';
        $lis    = isset($options['limit'])  ? (int) $options['limit']    : 0;
        $ofs    = isset($options['offset']) ? (int) $options['offset']   : 0;
        $values = isset($options['values']) ? (array) $options['values'] : array();

        $single = ($lis === 1) ? true : false;
        
        $select      = empty($ses) ? 'SELECT *'         : "SELECT $ses";
        $from        = empty($frs) ? "FROM $table_name" : "FROM $frs";
        $joins       = empty($jos) ? ''                 : $jos;
        $where       = empty($whs) ? ''                 : "WHERE $whs";
        $group_by    = empty($gbs) ? ''                 : "GROUP BY $gbs";
        $having      = empty($has) ? ''                 : "HAVING $has";
        $order_by    = empty($obs) ? ''                 : "ORDER BY $obs";
        $limit       = $lis > 0    ? "LIMIT $lis"       : '';
        $offset      = $ofs > 0    ? "OFFSET $ofs"      : '';
        
        $sql = "$select $from $joins $where $group_by $having $order_by $limit $offset";

        $objects = self::findBySql($sql, $values);
        
        return ($single) ? (!empty($objects) ? $objects[0] : false) : $objects;
    }
    
    private static function findBySql($sql, $values = null) {
        $class_name = get_called_class();
        
        self::logQuery($sql);
        
        $stmt = self::getConnection()->prepare($sql);
        if (!$stmt->execute($values)) {
            return false;
        }
        
        $objects = array();
        while ($object = $stmt->fetchObject($class_name)) {
            $objects[] = $object;
        }
        
        return $objects;
    }
    
    public static function findById($id) {
        return self::findOne(array(
            'where'  => 'id = :id',
            'values' => array(':id' => (int) $id)
        ));
    }
        
    public static function findByIdFrom($class_name, $id) {
        return $class_name::findById($id);
    }

    public static function findOne($options = array()) {
        $options['limit'] = 1;
        return self::find($options);
    }

    public static function findOneFrom($class_name, $where, $values=array()) {
        return $class_name::findOne(array(
            'where'  => $where,
            'values' => $values
        ));
    }

    public static function findAllFrom($class_name, $where=false, $values=array()) {
        if ($where) {
            return $class_name::find(array(
                'where'  => $where,
                'values' => $values
            ));
        } else {
            return $class_name::find();
        }
    }

    public static function countFrom($class_name, $where=false, $values=array()) {
        $sql = 'SELECT COUNT(*) AS nb_rows FROM '.self::tableNameFromClassName($class_name).($where ? ' WHERE '.$where:'');

        $stmt = self::$__CONN__->prepare($sql);
        $stmt->execute($values);

        self::logQuery($sql);

        return (int) $stmt->fetchColumn();
    }

}

abstract class Finder extends Record {
    
    private static $reserved = array(
        'all',
        'one',
        'by',
        'and',
        'ordered'
    );
    
    private static function find_all(&$commands, &$options = array()) {
        $options['select'][] = '*';

        if (is_array($commands) && !empty($commands)) {
            if (in_array($commands[0], self::$reserved)) {
                $cmd = 'find_'.$commands[0];
                self::$cmd(array_splice($commands, 1), $options);
            }
            else {
                throw new BadMethodCallException("Unknown find method including ${$commands[0]}.");
            }
        }
    }

    private static function find_one(&$commands, &$options = array()) {
        $options['select'][] = '*';
        $options['limit'][] = 1;
        
        if (is_array($commands) && !empty($commands)) {
            $cmd = array_shift($commands);
            
            if (in_array($cmd, self::$reserved)) {
                $cmd = 'find_'.$cmd;
                self::$cmd($commands, $options);
            }
            else {
                throw new BadMethodCallException("Unknown find method including $cmd.");
            }
        }
    }

    private static function find_ordered(&$commands, &$options = array()) {
        if (is_array($commands) && !empty($commands) && array_shift($commands) == 'by') {
            $cmd = array_shift($commands);
            
            if (in_array($cmd, self::$reserved)) {
                $cmd = 'find_'.$cmd;
                self::$cmd($commands, $options);
            }
            else {
                if (count($commands) > 0 && ($commands[0] == 'asc' || $commands[0] == 'desc')) {
                    $cmd .= ' '.array_shift($commands);
                }
                $options['order'][] = $cmd;
            }
        }
        else {
            throw new BadMethodCallException();
        }
    }

    private static function find_and(&$commands, &$options = array()) {
        if (is_array($commands) && !empty($commands)) {
            if (in_array($commands[0], self::$reserved)) {
                $cmd = 'find_'.$commands[0];
                self::$cmd(array_splice($commands, 1), $options);
            }
            else {
                $options['where'][] = "${commands[0]}=?";
            }
        }
    }
    
    private static function find_by(&$commands, &$options = array()) {
        for ($i=0; $i<count($commands); $i++) {
            if (!in_array($commands[$i], self::$reserved)) {
                $options['where'][] = "${commands[$i]}=?";
            }
            else {
                $cmd = 'find_'.$commands[$i];
                self::$cmd(array_splice($commands, $i+1), $options);
            }
        }
    }

    public static function __callStatic($name, $arguments) {
        $options = array(
            'select' => array(),
            'where'  => array(),
            'order'  => array(),
            'limit'  => array(),
            'offset' => array()
        );
        
        preg_match("/^find[A-Z][a-z]+.*/", $name, $matches);
        if ( empty($matches) ) {

            parent::__callStatic($name, $arguments);
        }

        preg_match_all("/([A-Z][a-z]+)/", $name, $matches);
        $matches = array_map('strtolower', $matches[1]);
        
        for($i = 0; $i < count($matches); $i++) {
            $entry = array_shift($matches);
            
            if (!in_array($entry, self::$reserved)) {
                $options['select'][] = $entry;
            }
            else {
                $cmd = 'find_'.$entry;
                self::$cmd($matches, $options);
            }
        }
        
        $options['select'] = implode(','     , $options['select']);
        $options['where']  = implode(' AND ' , $options['where']);
        $options['order']  = implode(','     , $options['order']);
        $options['limit']  = (int) implode('', $options['limit']);
        $options['offset'] = (int) implode('', $options['offset']);
        $options['values'] = $arguments;

        return self::find($options);
    }
}

class View {
    private $file;
    private $vars = array();

    public function __construct($file, $vars=false) {
        if (strpos($file, '/') === 0 || strpos($file, ':') === 1) {
            $this->file = $file.'.php';
        }
        else {
            $this->file = APP_PATH.'/views/'.ltrim($file, '/').'.php';
        }

        if ( ! file_exists($this->file)) {
            throw new Exception("View '{$this->file}' not found!");
        }

        if ($vars !== false) {
            $this->vars = $vars;
        }
    }

    public function assign($name, $value=null) {
        if (is_array($name)) {
            $this->vars = array_merge($this->vars, $name);
        } else {
            $this->vars[$name] = $value;
        }
    }

    public function render() {
        ob_start();

        extract($this->vars, EXTR_SKIP);
        include $this->file;

        $content = ob_get_clean();
        return $content;
    }

    public function display() { echo $this->render(); }

    public function __toString() { return $this->render(); }

}

class Controller {
    protected $layout = false;
    protected $layout_vars = array();

    public function execute($action, $params) {
        if (substr($action, 0, 1) == '_' || ! method_exists($this, $action)) {
            throw new Exception("Action '{$action}' is not valid!");
        }
        call_user_func_array(array($this, $action), $params);
    }

    public function setLayout($layout) {
        $this->layout = $layout;
    }

    public function assignToLayout($var, $value = null) {
        if (is_array($var)) {
            $this->layout_vars = array_merge($this->layout_vars, $var);
        } else {
            $this->layout_vars[$var] = $value;
        }
    }

    public function render($view, $vars=array()) {
        if ($this->layout) {
            $this->layout_vars['content_for_layout'] = new View($view, $vars);
            return new View('../layouts/'.$this->layout, $this->layout_vars);
        } else {
            return new View($view, $vars);
        }
    }

    public function display($view, $vars=array(), $exit=true) {
        echo $this->render($view, $vars);

        if ($exit) exit;
    }

    public function renderJSON($data_to_encode) {
        if (class_exists('JSON')) {
            return JSON::encode($data_to_encode);
        } else if (function_exists('json_encode')) {
                return json_encode($data_to_encode);
            } else {
                throw new Exception('No function or class found to render JSON.');
            }
    }

}

final class Observer {
    static protected $events = array();

    public static function observe($event_name, $callback) {
        if ( ! isset(self::$events[$event_name]))
            self::$events[$event_name] = array();

        self::$events[$event_name][$callback] = $callback;
    }

    public static function stopObserving($event_name, $callback) {
        if (isset(self::$events[$event_name][$callback]))
            unset(self::$events[$event_name][$callback]);
    }

    public static function clearObservers($event_name) {
        self::$events[$event_name] = array();
    }

    public static function getObserverList($event_name) {
        return (isset(self::$events[$event_name])) ? self::$events[$event_name] : array();
    }

    public static function notify($event_name) {
        $args = array_slice(func_get_args(), 1);

        foreach(self::getObserverList($event_name) as $callback) {
            $Args = array();
            foreach($args as $k => &$arg){
                $Args[$k] = &$arg;
            }
            call_user_func_array($callback, $args);
        }
    }
}

class AutoLoader {
    protected static $files = array();
    protected static $folders = array();

    public static function register()
    {
        spl_autoload_register(array('AutoLoader', 'load'), true, true);
    }

    public static function addFile($class_name, $file=null) {
        if ($file == null && is_array($class_name)) {
            self::$files = array_merge(self::$files, $class_name);
        } else {
            self::$files[$class_name] = $file;
        }
    }

    public static function addFolder($folder) {
        if ( ! is_array($folder)) {
            $folder = array($folder);
        }
        self::$folders = array_merge(self::$folders, $folder);
    }

    public static function load($class_name) {
        if (isset(self::$files[$class_name])) {
            if (file_exists(self::$files[$class_name])) {
                require self::$files[$class_name];
                return;
            }
        } else {
            foreach (self::$folders as $folder) {
                $folder = rtrim($folder, DIRECTORY_SEPARATOR);
                $file = $folder.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, $class_name).'.php';
                if (file_exists($file)) {
                    require $file;
                    return;
                }
            }
        }
        throw new Exception("AutoLoader could not find file for '{$class_name}'.");
    }

}

final class Flash {
    const SESSION_KEY = 'main_flash';

    private static $_flashstore = array(); // Data that prevous page left in the Flash

    public static function get($var) {
        return isset(self::$_flashstore[$var]) ? self::$_flashstore[$var] : null;
    }

    public static function set($var, $value) {
        $_SESSION[self::SESSION_KEY][$var] = $value;
    }

    public static function setNow($var, $value) {
        self::$_flashstore[$var] = $value;
    }

    public static function clear() {
        $_SESSION[self::SESSION_KEY] = array();
    }

    public static function init() {
        if ( ! empty($_SESSION[self::SESSION_KEY]) && is_array($_SESSION[self::SESSION_KEY])) {
            self::$_flashstore = $_SESSION[self::SESSION_KEY];
        }
        $_SESSION[self::SESSION_KEY] = array();
    }

}

final class Inflector {

    public static function camelize($string) {
        return str_replace(' ','',ucwords(str_replace('_',' ', $string)));
    }

    public static function underscore($string) {
        return strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $string));
    }

    public static function humanize($string) {
        return ucfirst(strtolower(str_replace('_', ' ', $string)));
    }
}

function use_helper() {
    static $_helpers = array();

    $helpers = func_get_args();

    foreach ($helpers as $helper) {
        if (in_array($helper, $_helpers)) continue;

        $helper_file = HELPER_PATH.DIRECTORY_SEPARATOR.$helper.'.php';

        if ( ! file_exists($helper_file)) {
            throw new Exception("Helper file '{$helper}' not found!");
        }

        include $helper_file;
        $_helpers[] = $helper;
    }
}


function use_model() {
    static $_models = array();

    $models = func_get_args();

    foreach ($models as $model) {
        if (in_array($model, $_models)) continue;

        $model_file = APP_PATH.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR.$model.'.php';

        if ( ! file_exists($model_file)) {
            throw new Exception("Model file '{$model}' not found!");
        }

        include $model_file;
        $_models[] = $model;
    }
}


function get_url() {
    $params = func_get_args();
    if (count($params) === 1) return BASE_URL . $params[0];

    $url = '';
    foreach ($params as $param) {
        if (strlen($param)) {
            $url .= $param{0} == '#' ? $param: '/'. $param;
        }
    }
    return BASE_URL . preg_replace('/^\/(.*)$/', '$1', $url);
}

function get_request_method() {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
        return 'AJAX';
    else
        return $_SERVER['REQUEST_METHOD'];
}

function redirect($url) {
    Flash::set('HTTP_REFERER', html_encode($_SERVER['REQUEST_URI']));
    header('Location: '.$url); exit;
}

function redirect_to($url) {
    redirect($url);
}

function html_encode($string) {
    return htmlentities($string, ENT_QUOTES, 'UTF-8') ;
}

function html_decode($string) {
    return html_entity_decode($string, ENT_QUOTES, 'UTF-8') ;
}

function remove_xss($string) {
    $string = preg_replace('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '', $string);

    $search = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()~`";:?+/={}[]-_|\'\\';
    $search_count = count($search);
    for ($i = 0; $i < $search_count; $i++) {
        $string = preg_replace('/(&#[xX]0{0,8}'.dechex(ord($search[$i])).';?)/i', $search[$i], $string);
        $string = preg_replace('/(&#0{0,8}'.ord($search[$i]).';?)/', $search[$i], $string);
    }

    $ra = array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'style',
        'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound',
        'title', 'link',
        'base',
        'onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy',
        'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint',
        'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick',
        'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged',
        'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter',
        'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate',
        'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown',
        'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown',
        'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup',
        'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange',
        'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter',
        'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange',
        'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
    $ra_count = count($ra);

    $found = true;
    while ($found == true) {
        $string_before = $string;
        for ($i = 0; $i < $ra_count; $i++) {
            $pattern = '/';
            for ($j = 0; $j < strlen($ra[$i]); $j++) {
                if ($j > 0) {
                    $pattern .= '((&#[xX]0{0,8}([9ab]);)||(&#0{0,8}([9|10|13]);))*';
                }
                $pattern .= $ra[$i][$j];
            }
            $pattern .= '/i';
            $replacement = '';
            $string = preg_replace($pattern, $replacement, $string);
            if ($string_before == $string) {
                $found = false;
            }
        }
    }
    return $string;
}

function cleanArrayXSS($ar) {
    $ret = array();

    foreach ($ar as $k => $v) {
        if (is_array($k)) $k = cleanArrayXSS($k);
        else $k = remove_xss($k);

        if (is_array($v)) $v = cleanArrayXSS($v);
        else $v = remove_xss($v);

        $ret[$k] = $v;
    }

    return $ret;
}

function cleanXSS() {
    $in = array(&$_GET, &$_COOKIE, &$_SERVER); //, &$_POST);

    while (list($k,$v) = each($in)) {
        foreach ($v as $key => $val) {
            $oldkey = $key;

            if (!is_array($val)) {
                $val = remove_xss($val);
            }
            else {
                $val = cleanArrayXSS($val);
            }

            if (!is_array($key)) {
                $key = remove_xss($key);
            }
            else {
                $key = cleanArrayXSS($key);
            }

            unset($in[$k][$oldkey]);
            $in[$k][$key] = $val; continue;
            $in[] =& $in[$k][$key];
        }
    }
    unset($in);
    return;
}

function jsEscape($value) {
    return strtr((string) $value, array(
        "'"     => '\\\'',
        '"'     => '\"',
        '\\'    => '\\\\',
        "\n"    => '\n',
        "\r"    => '\r',
        "\t"    => '\t',
        chr(12) => '\f',
        chr(11) => '\v',
        chr(8)  => '\b',
        '</'    => '\u003c\u002F',
    ));
}

function pageNotFound($url=null) {
    Observer::notify('page_not_found', $url);

    header("HTTP/1.0 404 Not Found");
    echo new View('404');
    exit;
}

function page_not_found($url=null) {
    pageNotFound($url);
}

function convert_size($num) {
    if ($num >= 1073741824) $num = round($num / 1073741824 * 100) / 100 .' gb';
    else if ($num >= 1048576) $num = round($num / 1048576 * 100) / 100 .' mb';
        else if ($num >= 1024) $num = round($num / 1024 * 100) / 100 .' kb';
            else $num .= ' b';
    return $num;
}

function memory_usage() {
    return convert_size(memory_get_usage());
}

function execution_time() {
    return sprintf("%01.4f", get_microtime() - MAIN_STARTING_MICROTIME);
}

function get_microtime() {
    $time = explode(' ', microtime());
    return doubleval($time[0]) + $time[1];
}

function odd_even() {
    static $odd = true;
    return ($odd = !$odd) ? 'even': 'odd';
}

function even_odd() {
    return odd_even();
}

function getContentFromUrl($url, $flags=0, $context=false) {

    if (!defined('CHECK_TIMEOUT')) define('CHECK_TIMEOUT', 5);

    if (ini_get('allow_url_fopen') && function_exists('file_get_contents')) {    
        if ($context === false) $context = stream_context_create(array('http' => array('timeout' => CHECK_TIMEOUT)));

        return file_get_contents($url, $flags, $context);
    }
    else if (function_exists('curl_version')) {
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_HEADER, false);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, CHECK_TIMEOUT);
        curl_setopt ($ch, CURLOPT_TIMEOUT, CHECK_TIMEOUT);
        ob_start();
        curl_exec ($ch);
        curl_close ($ch);
        return ob_get_clean();
    }

    return false;
}

function main_exception_handler($e) {
    if (!DEBUG) pageNotFound();

    echo '<style>h1,h2,h3,p,td {font-family:Verdana; font-weight:lighter;}</style>';
    echo '<h1>CMS - Uncaught '.get_class($e).'</h1>';
    echo '<h2>Description</h2>';
    echo '<p>'.$e->getMessage().'</p>';
    echo '<h2>Location</h2>';
    echo '<p>Exception thrown on line <code>'
    . $e->getLine() . '</code> in <code>'
    . $e->getFile() . '</code></p>';

    echo '<h2>Stack trace</h2>';
    $traces = $e->getTrace();
    if (count($traces) > 1) {
        echo '<pre style="font-family:Verdana; line-height: 20px">';

        $level = 0;
        foreach (array_reverse($traces) as $trace) {
            ++$level;

            if (isset($trace['class'])) echo $trace['class'].'&rarr;';

            $args = array();
            if ( ! empty($trace['args'])) {
                foreach ($trace['args'] as $arg) {
                    if (is_null($arg)) $args[] = 'null';
                    else if (is_array($arg)) $args[] = 'array['.sizeof($arg).']';
                        else if (is_object($arg)) $args[] = get_class($arg).' Object';
                            else if (is_bool($arg)) $args[] = $arg ? 'true' : 'false';
                                else if (is_int($arg)) $args[] = $arg;
                                    else {
                                        $arg = htmlspecialchars(substr($arg, 0, 64));
                                        if (strlen($arg) >= 64) $arg .= '...';
                                        $args[] = "'". $arg ."'";
                                    }
                }
            }
            echo '<strong>'.$trace['function'].'</strong>('.implode(', ',$args).')  ';
            echo 'on line <code>'.(isset($trace['line']) ? $trace['line'] : 'unknown').'</code> ';
            echo 'in <code>'.(isset($trace['file']) ? $trace['file'] : 'unknown')."</code>\n";
            echo str_repeat("   ", $level);
        }
        echo '</pre><hr/>';
    }

    $dispatcher_status = Dispatcher::getStatus();
    $dispatcher_status['request method'] = get_request_method();
    debug_table($dispatcher_status, 'Dispatcher status');
    if ( ! empty($_GET)) debug_table($_GET, 'GET');
    if ( ! empty($_POST)) debug_table($_POST, 'POST');
    if ( ! empty($_COOKIE)) debug_table($_COOKIE, 'COOKIE');
    debug_table($_SERVER, 'SERVER');
}

function debug_table($array, $label, $key_label='Variable', $value_label='Value') {
    echo '<table cellpadding="3" cellspacing="0" style="margin: 1em auto; border: 1px solid #000; width: 90%;">';
    echo '<thead><tr><th colspan="2" style="font-family: Verdana, Arial, sans-serif; background-color: #2a2520; color: #fff;">'.$label.'</th></tr>';
    echo '<tr><td style="border-right: 1px solid #000; border-bottom: 1px solid #000;">'.$key_label.'</td>'.
        '<td style="border-bottom: 1px solid #000;">'.$value_label.'</td></tr></thead>';

    foreach ($array as $key => $value) {
        if (is_null($value)) $value = 'null';
        else if (is_array($value)) $value = 'array['.sizeof($value).']';
            else if (is_object($value)) $value = get_class($value).' Object';
                else if (is_bool($value)) $value = $value ? 'true' : 'false';
                    else if (is_int($value)) $value = $value;
                        else {
                            $value = htmlspecialchars(substr($value, 0, 64));
                            if (strlen($value) >= 64) $value .= ' &hellip;';
                        }
        echo '<tr><td><code>'.$key.'</code></td><td><code>'.$value.'</code></td></tr>';
    }
    echo '</table>';
}

set_exception_handler('main_exception_handler');

function fix_input_quotes() {
    $in = array(&$_GET, &$_POST, &$_COOKIE);
    while (list($k,$v) = each($in)) {
        foreach ($v as $key => $val) {
            if (!is_array($val)) {
                $in[$k][$key] = stripslashes($val); continue;
            }
            $in[] =& $in[$k][$key];
        }
    }
    unset($in);
}

if (PHP_VERSION < 6 && get_magic_quotes_gpc()) {
    fix_input_quotes();
}

<?php
/**
 * Simple upload library
 *
 * @package Helpers
 */

class Upload {

    public $max_size       = 0;
    public $max_width      = 0;
    public $max_height     = 0;
    public $allowed_types  = "";
    public $file_temp      = "";
    public $file_name      = "";
    public $orig_name      = "";
    public $file_type      = "";
    public $file_size      = "";
    public $file_ext       = "";
    public $upload_path    = "";
    public $overwrite      = false;
    public $encrypt_name   = false;
    public $is_image       = false;
    public $image_width    = '';
    public $image_height   = '';
    public $image_type     = '';
    public $image_size_str = '';
    public $error_msg      = array();
    public $remove_spaces  = true;
    public $xss_clean = false;
    public $temp_prefix = "temp_file_";
    public $mimes = array();

    function __construct($props = array()) {
        if (count($props) > 0) {
            $this->initialize($props);
        }

    }

    function initialize($config = array()) {
        $defaults = array(
            'max_size'          => 0,
            'max_width'         => 0,
            'max_height'        => 0,
            'allowed_types'     => "",
            'file_temp'         => "",
            'file_name'         => "",
            'orig_name'         => "",
            'file_type'         => "",
            'file_size'         => "",
            'file_ext'          => "",
            'upload_path'       => "",
            'overwrite'         => false,
            'encrypt_name'      => false,
            'is_image'          => false,
            'image_width'       => '',
            'image_height'      => '',
            'image_type'        => '',
            'image_size_str'    => '',
            'error_msg'         => array(),
            'mimes'             => array(),
            'remove_spaces'     => true,
            'xss_clean'         => false,
            'temp_prefix'       => "temp_file_"
        );


        foreach ($defaults as $key => $val) {
            if (isset($config[$key])) {
                $method = 'set_'.$key;
                if (method_exists($this, $method)) {
                    $this->$method($config[$key]);
                } else {
                    $this->$key = $config[$key];
                }
            } else {
                $this->$key = $val;
            }
        }
    }

    function doUpload($field = 'userfile') {
        if ( ! isset($_FILES[$field])) {
            return false;
        }

        if ( ! $this->validateUploadPath()) {
            return false;
        }

        if ( ! is_uploaded_file($_FILES[$field]['tmp_name'])) {
            $error = ( ! isset($_FILES[$field]['error'])) ? 4 : $_FILES[$field]['error'];

            switch($error) {
                case 1:
                    $this->setError('upload_file_exceeds_limit');
                    break;
                case 3:
                    $this->setError('upload_file_partial');
                    break;
                case 4:
                    $this->setError('upload_no_file_selected');
                    break;
                default:
                    $this->setError('upload_no_file_selected');
                    break;
            }

            return false;
        }

        $this->file_temp = $_FILES[$field]['tmp_name'];
        $this->file_name = $_FILES[$field]['name'];
        $this->file_size = $_FILES[$field]['size'];
        $this->file_type = preg_replace("/^(.+?);.*$/", "\\1", $_FILES[$field]['type']);
        $this->file_type = strtolower($this->file_type);
        $this->file_ext  = $this->getExtension($_FILES[$field]['name']);

        if ($this->file_size > 0) {
            $this->file_size = round($this->file_size/1024, 2);
        }

        if ( ! $this->isAllowedFiletype()) {
            $this->setError('upload_invalid_filetype');
            return false;
        }

        if ( ! $this->isAllowedFilesize()) {
            $this->setError('upload_invalid_filesize');
            return false;
        }

        if ( ! $this->isAllowedDimensions()) {
            $this->setError('upload_invalid_dimensions');
            return false;
        }

        $this->file_name = $this->cleanFileName($this->file_name);

        if ($this->remove_spaces == true) {
            $this->file_name = preg_replace("/\s+/", "_", $this->file_name);
        }

        $this->orig_name = $this->file_name;

        if ($this->overwrite == false) {
            $this->file_name = $this->setFilename($this->upload_path, $this->file_name);

            if ($this->file_name === false) {
                return false;
            }
        }

        if ( ! @copy($this->file_temp, $this->upload_path . $this->file_name)) {
            if ( ! @move_uploaded_file($this->file_temp, $this->upload_path . $this->file_name)) {
                $this->setError('upload_destination_error');
                return false;
            }
        }

        if ($this->xss_clean == true) {
            $this->doXssClean();
        }

        $this->setImageProperties($this->upload_path . $this->file_name);

        return true;
    }

    function data() {
        return array (
        'file_name'         => $this->file_name,
        'file_type'         => $this->file_type,
        'file_path'         => $this->upload_path,
        'full_path'         => $this->upload_path.$this->file_name,
        'raw_name'          => str_replace($this->file_ext, '', $this->file_name),
        'orig_name'         => $this->orig_name,
        'file_ext'          => $this->file_ext,
        'file_size'         => $this->file_size,
        'is_image'          => $this->isImage(),
        'image_width'       => $this->image_width,
        'image_height'      => $this->image_height,
        'image_type'        => $this->image_type,
        'image_size_str'    => $this->image_size_str,
        );
    }

    function setUploadPath($path) {
        $this->upload_path = $path;
    }

    function setFilename($path, $filename) {
        if ($this->encrypt_name == true) {
            mt_srand();
            $filename = md5(uniqid(mt_rand())).$this->file_ext;
        }

        if ( ! file_exists($path.$filename)) {
            return $filename;
        }

        $filename = str_replace($this->file_ext, '', $filename);

        $new_filename = '';
        for ($i = 1; $i < 100; $i++) {
            if ( ! file_exists($path.$filename.$i.$this->file_ext)) {
                $new_filename = $filename.$i.$this->file_ext;
                break;
            }
        }

        if ($new_filename == '') {
            $this->setError('upload_bad_filename');
            return false;
        } else {
            return $new_filename;
        }
    }

    function setMaxFilesize($n) {
        $this->max_size = ( ! preg_match('#^\d+$#i', $n)) ? 0 : (int) $n;
    }

    function setMaxWidth($n) {
        $this->max_width = ( ! preg_match('#^\d+$#i', $n)) ? 0 : (int) $n;
    }

    function setMaxHeight($n) {
        $this->max_height = ( ! preg_match('#^\d+$#i', $n)) ? 0 : (int) $n;
    }

    function setAllowedTypes($types) {
        $this->allowed_types = explode('|', $types);
    }

    function setImageProperties($path = '') {
        if ( ! $this->is_image) {
            return;
        }

        if (function_exists('getimagesize')) {
            if (false !== ($D = @getimagesize($path))) {
                $types = array(1 => 'gif', 2 => 'jpeg', 3 => 'png');

                $this->image_width      = $D['0'];
                $this->image_height     = $D['1'];
                $this->image_type       = ( ! isset($types[$D['2']])) ? 'unknown' : $types[$D['2']];
                $this->image_size_str   = $D['3'];  // string containing height and width
            }
        }
    }

    function setXssClean($flag = false) {
        $this->xss_clean = ($flag == true) ? true : false;
    }

    function isImage() {
        $img_mimes = array(
            'image/gif',
            'image/jpg',
            'image/jpe',
            'image/jpeg',
            'image/pjpeg',
            'image/png',
            'image/x-png'
        );


        return (in_array($this->file_type, $img_mimes, true)) ? true : false;
    }

    function isAllowedFiletype() {
        if (count($this->allowed_types) == 0) {
            $this->setError('upload_no_file_types');
            return false;
        }

        foreach ($this->allowed_types as $val) {
            $mime = $this->mimesTypes(strtolower($val));

            if (is_array($mime)) {
                if (in_array($this->file_type, $mime, true)) {
                    return true;
                }
            } else {
                if ($mime == $this->file_type) {
                    return true;
                }
            }
        }

        return false;
    }

    function isAllowedFilesize() {
        if ($this->max_size != 0  AND  $this->file_size > $this->max_size) {
            return false;
        } else {
            return true;
        }
    }

    function isAllowedDimensions() {
        if ( ! $this->isImage()) {
            return true;
        }

        if (function_exists('getimagesize')) {
            $D = @getimagesize($this->file_temp);

            if ($this->max_width > 0 AND $D['0'] > $this->max_width) {
                return false;
            }

            if ($this->max_height > 0 AND $D['1'] > $this->max_height) {
                return false;
            }

            return true;
        }

        return true;
    }

    function validateUploadPath() {
        if ($this->upload_path == '') {
            $this->setError('upload path is empty');
            return false;
        }

        if (function_exists('realpath') AND @realpath($this->upload_path) !== false) {
            $this->upload_path = str_replace("\\", "/", realpath($this->upload_path));
        }

        if ( ! @is_dir($this->upload_path)) {
            $this->setError('upload path is not a directory');
            return false;
        }

        if ( ! is_writable($this->upload_path)) {
            $this->setError('upload_not_writable');
            return false;
        }

        $this->upload_path = preg_replace("/(.+?)\/*$/", "\\1/",  $this->upload_path);
        return true;
    }

    function getExtension($filename) {
        $x = explode('.', $filename);
        return '.'.end($x);
    }

    function cleanFileName($filename) {
        $bad = array(
            "<!--",
            "-->",
            "'",
            "<",
            ">",
            '"',
            '&',
            '$',
            '=',
            ';',
            '?',
            '/',
            "%20",
            "%22",
            "%3c",      // <
            "%253c",    // <
            "%3e",      // >
            "%0e",      // >
            "%28",      // (
            "%29",      // )
            "%2528",    // (
            "%26",      // &
            "%24",      // $
            "%3f",      // ?
            "%3b",      // ;
            "%3d"       // =
        );

        foreach ($bad as $val) {
            $filename = str_replace($val, '', $filename);
        }

        return $filename;
    }

    function doXssClean() {
        $file = $this->upload_path.$this->file_name;

        if (filesize($file) == 0) {
            return false;
        }

        if ( ! $fp = @fopen($file, 'rb')) {
            return false;
        }

        flock($fp, LOCK_EX);

        $data = fread($fp, filesize($file));

        fwrite($fp, $data);
        flock($fp, LOCK_UN);
        fclose($fp);
    }

    function setError($msg) {
        if (is_array($msg)) {
            foreach ($msg as $val) {
                $this->error_msg[] = $val;
            }
        } else {
            $this->error_msg[] = $msg;
        }
    }

    function displayErrors($open = '<p>', $close = '</p>') {
        $str = '';
        foreach ($this->error_msg as $val) {
            $str .= $open.$val.$close;
        }

        return $str;
    }

    function mimesTypes($mime) {
        if (count($this->mimes) == 0) {
            $this->mimes = array('hqx' => 'application/mac-binhex40',
                'cpt' => 'application/mac-compactpro',
                'csv' => array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel'),
                'bin' => 'application/macbinary',
                'dms' => 'application/octet-stream',
                'lha' => 'application/octet-stream',
                'lzh' => 'application/octet-stream',
                'exe' => array('application/octet-stream', 'application/x-msdownload'),
                'class' => 'application/octet-stream',
                'psd' => 'application/x-photoshop',
                'so' => 'application/octet-stream',
                'sea' => 'application/octet-stream',
                'dll' => 'application/octet-stream',
                'oda' => 'application/oda',
                'pdf' => array('application/pdf', 'application/x-download'),
                'ai' => 'application/postscript',
                'eps' => 'application/postscript',
                'ps' => 'application/postscript',
                'smi' => 'application/smil',
                'smil' => 'application/smil',
                'mif' => 'application/vnd.mif',
                'xls' => array('application/excel', 'application/vnd.ms-excel', 'application/msexcel'),
                'ppt' => array('application/powerpoint', 'application/vnd.ms-powerpoint'),
                'wbxml' => 'application/wbxml',
                'wmlc' => 'application/wmlc',
                'dcr' => 'application/x-director',
                'dir' => 'application/x-director',
                'dxr' => 'application/x-director',
                'dvi' => 'application/x-dvi',
                'gtar' => 'application/x-gtar',
                'gz' => 'application/x-gzip',
                'php' => 'application/x-httpd-php',
                'php4' => 'application/x-httpd-php',
                'php3' => 'application/x-httpd-php',
                'phtml' => 'application/x-httpd-php',
                'phps' => 'application/x-httpd-php-source',
                'js' => 'application/x-javascript',
                'swf' => 'application/x-shockwave-flash',
                'sit' => 'application/x-stuffit',
                'tar' => 'application/x-tar',
                'tgz' => array('application/x-tar', 'application/x-gzip-compressed'),
                'xhtml' => 'application/xhtml+xml',
                'xht' => 'application/xhtml+xml',
                'zip' => array('application/x-zip', 'application/zip', 'application/x-zip-compressed'),
                'mid' => 'audio/midi',
                'midi' => 'audio/midi',
                'mpga' => 'audio/mpeg',
                'mp2' => 'audio/mpeg',
                'mp3' => array('audio/mpeg', 'audio/mpg', 'audio/mpeg3', 'audio/mp3'),
                'aif' => 'audio/x-aiff',
                'aiff' => 'audio/x-aiff',
                'aifc' => 'audio/x-aiff',
                'ram' => 'audio/x-pn-realaudio',
                'rm' => 'audio/x-pn-realaudio',
                'rpm' => 'audio/x-pn-realaudio-plugin',
                'ra' => 'audio/x-realaudio',
                'rv' => 'video/vnd.rn-realvideo',
                'wav' => 'audio/x-wav',
                'bmp' => 'image/bmp',
                'gif' => 'image/gif',
                'jpeg' => array('image/jpeg', 'image/pjpeg'),
                'jpg' => array('image/jpeg', 'image/pjpeg'),
                'jpe' => array('image/jpeg', 'image/pjpeg'),
                'png' => array('image/png', 'image/x-png'),
                'tiff' => 'image/tiff',
                'tif' => 'image/tiff',
                'css' => 'text/css',
                'html' => 'text/html',
                'htm' => 'text/html',
                'shtml' => 'text/html',
                'txt' => 'text/plain',
                'text' => 'text/plain',
                'log' => array('text/plain', 'text/x-log'),
                'rtx' => 'text/richtext',
                'rtf' => 'text/rtf',
                'xml' => 'text/xml',
                'xsl' => 'text/xml',
                'mpeg' => 'video/mpeg',
                'mpg' => 'video/mpeg',
                'mpe' => 'video/mpeg',
                'qt' => 'video/quicktime',
                'mov' => 'video/quicktime',
                'avi' => 'video/x-msvideo',
                'movie' => 'video/x-sgi-movie',
                'doc' => 'application/msword',
                'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'word' => array('application/msword', 'application/octet-stream'),
                'xl' => 'application/excel',
                'eml' => 'message/rfc822',
                'json' => array('application/json', 'text/json')
            );
        }

        return (!isset($this->mimes[$mime])) ? false : $this->mimes[$mime];
    }

}

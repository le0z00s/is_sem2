<?php
/**
 * @package Helpers
 */
class Pagination {

    var $base_url           = ''; // The page we are linking to
    var $total_rows         = ''; // Total number of items (database results)
    var $per_page           = 10; // Max number of items you want shown per page
    var $num_links          =  3; // Number of "digit" links to show before/after the currently viewed page
    var $cur_page           =  1; // The current page being viewed
    var $uri_param          =  0;
    var $first_link         = '&lsaquo; First';
    var $next_link          = '&gt;';
    var $prev_link          = '&lt;';
    var $last_link          = 'Last &rsaquo;';
    var $full_tag_open      = '';
    var $full_tag_close     = '';
    var $first_tag_open     = '';
    var $first_tag_close    = '&nbsp;';
    var $last_tag_open      = '&nbsp;';
    var $last_tag_close     = '';
    var $cur_tag_open       = '&nbsp;<b>';
    var $cur_tag_close      = '</b>';
    var $next_tag_open      = '&nbsp;';
    var $next_tag_close     = '&nbsp;';
    var $prev_tag_open      = '&nbsp;';
    var $prev_tag_close     = '';
    var $num_tag_open       = '&nbsp;';
    var $num_tag_close      = '';

    function __construct($params = array()) {
        if (count($params) > 0) {
            $this->initialize($params);
        }
    }

    function initialize($params = array()) {
        if (count($params) > 0) {
            foreach ($params as $key => $val) {
                if (isset($this->$key)) {
                    $this->$key = $val;
                }
            }
        }
    } 

    function createLinks() {

        if ($this->total_rows == 0 || $this->per_page == 0) {
            return '';
        }

        $num_pages = ceil($this->total_rows / $this->per_page);

        if ($num_pages == 1) {
            return '';
        }

        $start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
        $end = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

        $output = '';

        if ($this->cur_page > $this->num_links) {
            $output .= $this->first_tag_open.'<a href="'.$this->base_url.'">'.$this->first_link.'</a>'.$this->first_tag_close;
        }

        for ($page = $start -1; $page <= $end; $page++) {
            if ($page > 0) {
                if ($this->cur_page == $page) {
                    $output .= $this->cur_tag_open.$page.$this->cur_tag_close; 
                } else {
                    $output .= $this->num_tag_open.'<a href="'.$this->base_url.$page.'">'.$page.'</a>'.$this->num_tag_close;
                } 
            } 
        } 

        if (($this->cur_page + $this->num_links) < $num_pages) {
            $output .= $this->last_tag_open.'<a href="'.$this->base_url.$num_pages.'">'.$this->last_link.'</a>'.$this->last_tag_close;
        }

        $output = preg_replace("#([^:])//+#", "\\1/", $output);

        $output = $this->full_tag_open.$output.$this->full_tag_close;

        return $output;
    } 

  } 

<?php
/**
 * @package Helpers
 */
defined('I18N_PATH') or define('I18N_PATH', APP_PATH.DIRECTORY_SEPARATOR.'i18n');
defined('DEFAULT_LOCALE') or define('DEFAULT_LOCALE', 'en');

function __($string, $args=null) {
    
    $string = I18n::getText($string);
    if ($args === null) return $string;

    return strtr($string, $args);
}

class I18n {
    private static $locale = DEFAULT_LOCALE;
    private static $array = array();
    private static $default = array();

    public static function setLocale($locale) {
        self::$locale = $locale;
        self::loadArray();
    }

    public static function getLocale() {
        return self::$locale;
    }

    public static function getText($string) {
        if (isset(self::$array[$string])) {
            return self::$array[$string];
        }
        else {
            if (isset(self::$default[$string])) {
                return self::$default[$string];
            }
            else {
                return 'MISSING_DEFAULT_STRING';
            }
        }
    }

    public static function loadArray() {
        $catalog_file = I18N_PATH.DIRECTORY_SEPARATOR.self::$locale.'-message.php';
        $default_catalog_file = I18N_PATH.DIRECTORY_SEPARATOR.DEFAULT_LOCALE.'-message.php';

        if (file_exists($catalog_file)) {
            $array = include $catalog_file;
            self::add($array);
        }

        if (file_exists($default_catalog_file)) {
            $default = include $default_catalog_file;
            self::addDefault($default);
        }
        else {
            throw new Exception('Unable to find default language ('.DEFAULT_LOCALE.'-message.php) file for core system.');
        }
    }

    public static function add($array) {
        if (!empty($array))
            self::$array = array_merge(self::$array, $array);
    }
    
    public static function addDefault($default) {
        if (!empty($default))
            self::$default = array_merge(self::$default, $default);
    }

    public static function getPreferredLanguages() {
        $languages = array();

        if ( isset( $_SERVER["HTTP_ACCEPT_LANGUAGE"] ) ) {
            $list = strtolower( $_SERVER["HTTP_ACCEPT_LANGUAGE"] );
            $list = str_replace( ' ', '', $list );
            $list = explode( ",", $list );

            foreach ( $list as $language ) {
                $languages[] = substr( $language, 0, 2 );
            }
        }

        return $languages;
    }

}

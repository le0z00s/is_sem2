<?php

/**
 * @package Translations
 */

return array(
		'Ticket sales statistics' => 'Statystyki sprzedaży',
		'Provides interface for ticket sales statistics' => 'Zapewnia interfejs wyświetlający statystyki sprzedaży biletów'
);
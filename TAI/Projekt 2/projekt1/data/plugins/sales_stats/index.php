<?php
/**
 * @package Plugins
 * @subpackage sales_stats
 */

if (!defined('IN_CMS')) { exit(); }

Plugin::setInfos(array(
    'id'          => 'sales_stats',
    'title'       => __('Ticket sales statistics'),
    'description' => __('Provides interface for ticket sales statistics')
));

if(Permission::findByName('sales_stats_view')==NULL){
	$perm = new Permission();
	$perm->name = 'sales_stats_view';
	$perm->save();
}
if(Role::findByName('analyst')==NULL){
	$role = new Role();
	$role->name = 'analyst';
	$role->save();
}

$admin_perms = RolePermission::findPermissionsFor(Role::findByName('administrator')->id);
$admin_perms[] = Permission::findByName('sales_stats_view');
$admin_perms[] = Permission::findByName('comment_view');
RolePermission::savePermissionsFor(Role::findByName('administrator')->id, $admin_perms);

$perms = array();
$perms[] = Permission::findByName('sales_stats_view');
$perms[] = Permission::findByName('comments_view');
$perms[] = Permission::findByName('admin_view');
RolePermission::savePermissionsFor(Role::findByName('analyst')->id, $perms);


Plugin::addController('sales_stats', __('Ticket sales statistics'), 'sales_stats_view', true);

AutoLoader::addFolder(PLUGINS_ROOT.DS.'sales_stats'.DS.'models');
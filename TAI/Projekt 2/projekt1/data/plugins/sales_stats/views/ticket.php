<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Tickets'); ?></h1>
<table id="ticket" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('Flight From').'-'.__('Flight To'); ?></th>
      <th><?php echo __('Ticket Type');?></th>
      <th><?php echo __('Baggage');?></th>
      <th><?php echo __('Amount'); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($tickets as $ticket): ?> 
	
    <tr class="node <?php echo odd_even(); ?>">
      <td>
      	<?php $flight = Flight::findById($ticket->flight_id);
      		echo Airport::findById($flight->flight_from)->name.' - '.Airport::findById($flight->flight_to)->name;
      	?>
      </td>
      <td>
      <?php $type = TicketType::findById($ticket->type);
      	echo $type->name;
      ?>
      </td>
      <td>
      	<?php 
			if($ticket->baggage != 0 && $ticket->baggage!=null){
    			echo __('Yes');
    		}else{
    			echo __('No');
    		}
      	?>
      </td>
      <td>
      <?php
      		$amount = $flight->price * $type->price_multiplier;
      		if($ticket->baggage!=0){
      			$amount +=10;
      		}
      		echo $amount;
      	?>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
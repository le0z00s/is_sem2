<?php
/**
 * @package Plugins
 * @subpackage sales_stats
 */

if (!defined('IN_CMS')) { exit(); }

class SalesStatsController extends PluginController {

    public function __construct() {
        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('../../plugins/sales_stats/views/sidebar'));
    }

    public function index() {
        $this->ticket();
    }
    function ticket(){
    	$this->display('sales_stats/views/ticket', array(
    			'tickets' => Ticket::findAll()
    	));
    }
}
<?php
/**
 * @package Plugins
 * @subpackage customs_control
 */

if (!defined('IN_CMS')) { exit(); }

$PDO = Record::getConnection();
$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));

//Setup table structure
if ($driver == 'mysql') {
	//suspects table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."suspects (
			id int(11) unsigned NOT NULL auto_increment,
			user_id int(11) unsigned NOT NULL,
			image LONGBLOB DEFAULT NULL,
			description varchar(255) DEFAULT NULL,
			PRIMARY KEY (id),
			KEY name (name)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");
}

else if ($driver == 'sqlite') {
	//suspects table
	$PDO->exec("CREATE TABLE IF NOT EXISTS suspects (
			id INTEGER NOT NULL PRIMARY KEY,
			user_id INTEGER NOT NULL,
			image LONGBLOB DEFAULT NULL,
			description varchar(255) DEFAULT NULL)");
}

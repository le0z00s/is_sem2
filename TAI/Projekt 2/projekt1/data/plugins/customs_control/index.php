<?php
/**
 * @package Plugins
 * @subpackage customs_control
 */

if (!defined('IN_CMS')) { exit(); }

Plugin::setInfos(array(
    'id'          => 'customs_control',
    'title'       => __('Customs control'),
    'description' => __('Provides methods for customs control like checking if passenger was previously prosecuted for smuggling')
));

if(Permission::findByName('customs_control_view')==NULL){
	$perm = new Permission();
	$perm->name = 'customs_control_view';
	$perm->save();
}
if(Role::findByName('customs_officer')==NULL){
	$role = new Role();
	$role->name = 'customs_officer';
	$role->save();
}

$admin_perms = RolePermission::findPermissionsFor(Role::findByName('administrator')->id);
$admin_perms[] = Permission::findByName('customs_control_view');
RolePermission::savePermissionsFor(Role::findByName('administrator')->id, $admin_perms);

$perms = array();
$perms[] = Permission::findByName('customs_control_view');
$perms[] = Permission::findByName('admin_view');
RolePermission::savePermissionsFor(Role::findByName('customs_officer')->id, $perms);


Plugin::addController('customs_control', __('Customs control'), 'customs_control_view', true);

AutoLoader::addFolder(PLUGINS_ROOT.DS.'customs_control'.DS.'models');

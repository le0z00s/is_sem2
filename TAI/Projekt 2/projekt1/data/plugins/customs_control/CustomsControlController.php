<?php
/**
 * @package Plugins
 * @subpackage customs_control
 */

if (!defined('IN_CMS')) { exit(); }

class CustomsControlController extends PluginController {

    public function __construct() {
        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('../../plugins/customs_control/views/sidebar'));
    }

    public function index() {
        $this->security();
    }

    public function security() {
        $this->display('customs_control/views/security');
    }
    function suspect(){
    	$this->display('customs_control/views/suspect', array(
    			'suspects' => Suspects::findAll()
    	));
    }
    
    //suspects add, edit, delete functions
    public function suspect_add() {
    	if (get_request_method() == 'POST') {
    		$this->_suspect_add();
    	}
    
    	$suspect = Flash::get('post_data');
    
    	if (empty($suspect)) {
    		$suspect = new Suspects();
    	}
    
    	$this->display('customs_control/views/suspect/edit', array(
    			'action'  => 'add',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/customs_control/suspect_add'),
    			'suspect' => $suspect
    	));
    }
    
    private function _suspect_add() {
    	$data = $_POST['suspect'];
    
    	if (!AuthUser::hasPermission('customs_control_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/customs_control/suspect'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/customs_control/suspect_add')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/customs_control/suspect'));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/customs_control/suspect'));
    	}
    
    	$suspect = new Suspects($data);
    
    	if ( ! $suspect->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Suspect has not been added. Something went wrong!'));
    		redirect(get_url('plugin/customs_control/suspect', 'add'));
    	}
    	else {
    		Flash::set('success', __('Suspect has been added!'));
    		Observer::notify('suspect_after_add', $suspect);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/customs_control/suspect'));
    	}
    	else {
    		redirect(get_url('plugin/customs_control/suspect_edit/'.$suspect->id));
    	}
    }
    
    public function suspect_edit($id) {
    	$suspect = Flash::get('post_data');
    
    	if (empty($suspect)) {
    		$suspect = Suspects::findById($id);
    		if ( !$suspect ) {
    			Flash::set('error', __('Suspect not found!'));
    			redirect(get_url('plugin/customs_control/suspect'));
    		}
    	}
    
    	if (get_request_method() == 'POST') {
    		$this->_suspect_edit($id);
    	}
    
    	$this->display('customs_control/views/suspect/edit', array(
    			'action'  => 'edit',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/customs_control/suspect_edit'),
    			'suspect' => $suspect
    	));
    }
    
    private function _suspect_edit($id) {
    	$data = $_POST['suspect'];
    	$data['id'] = $id;
    
    	if (!AuthUser::hasPermission('customs_control_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/customs_control/views/suspect'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/customs_control/suspect_edit')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/customs_control/suspect_edit/'.$id));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/customs_control/suspect_edit/'.$id));
    	}
    
    	$suspect = new Suspects($data);
    
    	if ( ! $suspect->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Suspect has not been edited. Something went wrong!'));
    		redirect(get_url('plugin/customs_control/suspect_edit/'.$id));
    	}
    	else {
    		Flash::set('success', __('Suspect has been saved!'));
    		Observer::notify('suspect_after_edit', $suspect);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/customs_control/suspect'));
    	}
    	else {
    		redirect(get_url('plugin/customs_control/suspect_edit/'.$id));
    	}
    }
    
    function suspect_delete($id) {
    	if (!AuthUser::hasPermission('customs_control_view')) {
    		Flash::set('error', __('You do not have permission to delete this!'));
    		redirect(get_url('plugin/customs_control'));
    	}
    
    	if ($suspect = Suspects::findById($id)) {
    		if ($suspect->delete()) {
    			Flash::set('success', __('Suspect has been deleted!'));
    			Observer::notify('suspect_after_delete', $suspect);
    		}
    		else
    			Flash::set('error', __('Suspect has not been deleted!'));
    	}
    	else
    		Flash::set('error', __('Suspect not found!'));
    
    	redirect(get_url('plugin/customs_control/suspect'));
    }
}
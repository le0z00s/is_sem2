<?php
/**
 * @package Plugins
 * @subpackage customs_control
 */

if (!defined('IN_CMS')) { exit(); }

?>
<p class="button"><a href="<?php echo get_url('plugin/customs_control/security'); ?>"><img src="<?php echo ICONS_PATH;?>documentation-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Security'); ?></a>
<p class="button"><a href="<?php echo get_url('plugin/customs_control/suspect'); ?>"><img src="<?php echo ICONS_PATH;?>documentation-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Suspects'); ?></a>

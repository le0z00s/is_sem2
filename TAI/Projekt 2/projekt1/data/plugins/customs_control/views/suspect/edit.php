<?php
/**
 * @package Views
 */
?>
<h1><?php echo __(ucfirst($action).' Flight'); ?></h1>

<form action="<?php echo $action=='edit' ? get_url('plugin/customs_control/suspect_edit/'.$suspect->id): get_url('plugin/customs_control/suspect_add'); ; ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
  <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
    
    <?php $suspect_users = User::findAll(); ?>
    
    <tr>
    	<td class="label"><label for="suspect_user_id"><?php echo __('First Name').' '.__('And').' '.__('Last Name'); ?></label></td>
    	<td class="field">
          <select class="select" id="suspect_user_id" name="suspect[user_id]">
<?php foreach ($suspect_users as $suspect_user): ?>
            <option value="<?php echo $suspect_user->id; ?>"<?php if ($suspect->user_id == $suspect_user->id) echo ' selected="selected"'; ?>><?php echo $suspect_user->name.' '.$suspect_user->surname;?></option>
<?php endforeach; ?>
          </select>
        </td>
    </tr>
    <!-- 
    <tr>
    	<td class="label"><label for="suspect_image"><?php echo __('Image'); ?></label></td>
    	<td class="field"><textarea class="textbox" id="suspect_image" name="suspect[image]" rows="10" cols="20"><?php echo $suspect->image; ?></textarea></td>
    </tr>
     -->
    <tr>
    	<td class="label"><label for="suspect_description"><?php echo __('Description'); ?></label></td>
    	<td class="field">
    		<textarea class="textbox" id="suspect_description" name="suspect[description]" rows="10" cols="20"><?php echo $suspect->description; ?></textarea>
    	</td>
    </tr>
  </table>

<?php Observer::notify('suspect_edit_view_after_details', $suspect); ?>

  <p class="buttons">
    <input name="suspect[image]" type="hidden" value="none">
    <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    <?php echo __('or'); ?> <a href="<?php echo (AuthUser::hasPermission('customs_control_view')) ? get_url('plugin/customs_control/suspect') : get_url(); ?>"><?php echo __('Cancel'); ?></a>
  </p>

</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
Field.activate('customs_control_suspect_from');
// ]]>
</script>

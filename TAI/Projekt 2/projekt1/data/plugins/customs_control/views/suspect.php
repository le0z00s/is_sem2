<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Suspects'); ?></h1>


<p class="button"><a href="<?php echo get_url('plugin/customs_control/suspect_add'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Add'); ?></a></p>

<table id="suspects" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('First Name').' '.__('And').' '.__('Last Name'); ?></th>
      <th><?php echo __('Address'); ?></th>
      <th><?php echo __('Country'); ?></th>
      <!-- <th><?php echo __('Image'); ?></th> -->
      <th><?php echo __('Description'); ?></th>
      <th><?php echo __('Modify')?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($suspects as $suspect): ?> 
	<?php $suspect_user = User::findById($suspect->user_id);?>
    <tr class="node <?php echo odd_even(); ?>">
      <td class="suspect">
        <a href="<?php echo get_url('plugin/customs_control/suspect_edit/'.$suspect->id); ?>"><?php echo $suspect_user->name.' '.$suspect_user->surname;?></a>
      </td>
      <td>
      	<?php echo $suspect_user->address;?>
      </td>
      <td>
      	<?php echo $suspect_user->country;?>
      </td>
      <!-- 
      <td>
      	
      </td>
       -->
      <td>
      	<?php echo $suspect->description;?>
      </td>
      <td>
        <a href="<?php echo get_url('plugin/customs_control/suspect_delete/'.$suspect->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$suspect_user->name.' '.$suspect_user->surname.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete suspect'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
<?php
/**
 * @package Plugins
 * @subpackage customs_control
 */

if (!defined('IN_CMS')) { exit(); }

?>
<h1><?php echo __('Security'); ?></h1>
<p>
Działalność portu lotniczego z punktu widzenia bezpieczeństwa można rozpatrywać w dwóch
obszarach:  
I obszar: obsługa ruchu statków powietrznych – zabezpieczenie startu, lądowania, parkowania
i obsługi naziemnej samolotów,
II obszar: obsługa ruchu pasażerskiego i towarowego – handling, odprawa biletowo‐bagażowa  
i paszportowo‐celna, kontrola bezpieczeństwa bagażu i pasażera, załadunek na pokład
samolotu itd.
W każdym z tych obszarów występują zagrożenia innego typu. W pierwszym obszarze działalności
zagrożenia związane są z ruchem lotniczym (wypadek lotniczy, katastrofa, awaryjne lądowanie). W
drugim, dotyczą  ochrony portu przed aktami bezprawnej ingerencji (terroryzm, uprowadzenia,
zamachy bombowe).
Port lotniczy musi być przygotowany do efektywnego przeciwdziałania wymienionym zagrożeniom,
nie dopuszczać  do ich powstania, a w sytuacjach kryzysowych – po zaistnieniu zdarzenia –
błyskawicznie przeprowadzić skuteczną akcję ratowniczą.  
Dla zapewnienia bezpieczeństwa, w porcie lotniczym przez całą  dobę  czuwają 
wyspecjalizowane służby pod kierownictwem Dyżurnego Operacyjnego Portu, których zadaniem jest
ochrona i obrona portu oraz sprawne uruchomienie akcji kryzysowej po zaistnieniu zdarzenia  
i przeprowadzenie akcji ratowniczej.
2
2. Ratownictwo lotniskowe
Ze statystyki wynika,  że ponad 80% wypadków i katastrof lotniczych występuje  
podczas startu i lądowania, na lotnisku lub w bezpośredniej jego bliskości.  
Po zaistnieniu wypadku lotniczego, katastrofy lub awaryjnego lądowania natychmiast do akcji
wchodzi wyspecjalizowana jednostka Lotniskowej Straży Pożarnej i karetka reanimacyjna z lekarzem
lub pielęgniarką dyżurną, rozpoczynając działania ratownicze.  
W tym samym czasie Dyżurny Operacyjny Portu wraz z podległymi służbami uruchamia akcję 
kryzysową  powiadamiając osoby funkcyjne i zewnętrzne jednostki Państwowej Straży Pożarnej,
Pogotowia Ratunkowego i Policji – zgodnie z „Instrukcją Alarmową” oraz „Schematem Alarmowania i
Powiadamiania”. Na Stanowisku Kierowania formuje się  Zespół  Koordynujący (Sztab Kryzysowy).
Dowódcą  akcji ratowniczej jest Komendant Lotniskowej Straży Pożarnej (Kierownik Zmiany), a
Koordynatorem Działań jest Kierownik Zespołu Koordynującego.  
Działania jednostek ratowniczych, służb operacyjnych, osób funkcyjnych a także wszystkich
jednostek działających na terenie portu lotniczego oraz jednostek zewnętrznych biorących udział w
akcji, ujęte są  w „Operacyjnym Planie Ratownictwa Lotniskowego”, który jest jednym z
podstawowych dokumentów dotyczących bezpieczeństwa, obowiązujących w każdym porcie
lotniczym.
3. Ochrona portu przed aktami bezprawnej ingerencji
Każdy port lotniczy jest obiektem, który w sposób szczególny narażony jest na wszelkiego
typu akty terrorystyczne, uprowadzenia, zamachy bombowe itp.  
Przyczyn takiego stanu rzeczy jest wiele, do najważniejszych z nich można zaliczyć:
1) otwartość  portu lotniczego na  Świat i możliwość  szybkiego przemieszczania się  (nielegalne
przerzuty, porachunki mafijne itp.),
2) samolot jako środek transportu jest najbardziej narażony na uprowadzenie – z pokładu samolotu
najłatwiej stawiać żądania i uzyskać ich spełnienie,
3) zamach bombowy w samolocie lub w porcie lotniczym pociąga za sobą wiele ofiar i olbrzymie
koszty.
W sytuacji zaistnienia aktu bezprawnej ingerencji, działania służb operacyjnych, osób
funkcyjnych, jednostek działających na terenie portu lotniczego oraz grupy specjalnej z Komendy
Wojewódzkiej Policji ujęte są w „Programie Bezpieczeństwa Portu Lotniczego”, który podobnie jak
„Operacyjny Plan Ratownictwa Lotniskowego” jest podstawowym dokumentem obowiązującym
w każdym porcie lotniczym.  
Dodatkową  instrukcją  stanowiącą  uzupełnienie „Programu Bezpieczeństwa ...” w zakresie
działania służb operacyjnych i osób funkcyjnych na wypadek zagrożenia bombowego są „Procedury
postępowania po otrzymaniu informacji o podłożeniu  ładunku bombowego”. Dokument Doc. 30
Europejskiej Konwencji Lotnictwa Cywilnego (ECAC) pt. „Ochrona Lotnictwa Cywilnego” wprowadza
w tym zakresie pewne nowe elementy, w tym m.in. tzw. Zespół Analizy i Oceny Zagrożenia składający
3
się z najbardziej kompetentnych osób w porcie lotniczym do analizy sytuacji i oceny zagrożenia. W
jego skład wchodzą: Wiceprezes ds. Operacyjnych Portu Lotniczego, Komendant Policji i Komendant
Straży Granicznej. Zespół sprawdza wiarygodność  otrzymanej informacji o zagrożeniu bombowym,
analizuje sytuację i kwalifikuje zagrożenie do jednej z trzech kategorii „czerwonej”, „pomarańczowej”
lub „zielonej”, a następnie podejmuje decyzje o przeciw‐działaniu (wstrzymanie ruchu lotniczego,
ewakuacja dworca lotniczego, wezwanie grupy specjalnej z KWP, rozpoznanie minersko‐
pirotechniczne) lub działaniach profilaktycznych mających na celu zminimalizowanie zagrożenia
(wzmocnienie ochrony, wzmożenie czujność przy kontroli bezpieczeństwa bagażu i pasażerów). Po
przybyciu na lotnisko grupy specjalnej z Komendy Wojewódzkiej Policji, dowódca tej grupy przejmuje
inicjatywę, a Sztab Kryzysowy zabezpiecza przeciwdziałanie i koordynuje działania.
Wstrzymanie ruchu lotniczego i ewakuacja dworca lotniczego pociąga za sobą zawsze bardzo
duże koszty, szczególnie na dużych lotniskach. Ze statystyki wynika,  że ponad 99% informacji o
zagrożeniu bombowym w lotnictwie cywilnym stanowią  informacje fałszywe mające na celu
spowodowanie zakłóceń w ruchu lotniczym i wywołanie przerw w normalnym funkcjonowaniu portu
lotniczego. Mimo to, wszystkie bezprawne akty ingerencji skierowane przeciw lotnictwu cywilnemu
muszą być zawsze ostrożnie brane pod uwagę i obiektywnie analizowane.
Analiza i ocena sytuacji oraz zakwalifikowanie zagrożenia do jednej z trzech kategorii
(czerwona, pomarańczowa lub zielona) polega na udzieleniu odpowiedzi na szereg pytań dotyczących
lokalizacji obiektu zamachu bombowego, charakterystyki  ładunku bombowego, osoby informatora
lub organizacji terrorystycznej, stanu ochrony portu, a także sytuacji politycznej i gospodarczej
państwa oraz sytuacji ekonomicznej i prawnej przewoźnika. Problem ten nadaje się  dobrze do
oprogramowania informatycznego. Program komputerowy analizy i oceny zagrożenia, posiadając
aktualne dane o uwarunkowaniach politycznych, sytuacji przewoźnika i stanu ochrony portu – po
wprowadzeniu danych dotyczących informatora oraz lokalizacji  ładunku bombowego i jego
charakterystyki – błyskawicznie zakwalifikuje zagrożenie do odpowiedniej kategorii, ułatwiając w ten
sposób pracę Zespołu Analizy i Oceny Zagrożenia działającego w warunkach deficytu czasowego.

<h2>4. Zintegrowany system ochrony portu lotniczego średniej wielkości</h2>
W celu zapewnienia bezpieczeństwa dla pasażerów, załóg i obsługi oraz zabezpieczenia
samolotów i obiektów portu konieczne jest stosowanie wysoko efektywnych systemów ochrony.
Ochrona fizyczna przez funkcjonariuszy Służby Ochrony Lotniska, Policji i Straży Granicznej musi być 
wspomagana przez środki techniczne w postaci systemów elektronicznych.  
Podstawowym warunkiem efektywności systemu ochrony jest jego integracja, tak aby
zapewnić kontrolę stanu bezpieczeństwa całego obiektu z jednego centrum i przekazywanie poleceń 
wykonawczych z tego centrum dla personelu ochrony portu w celu skutecznego przeciwdziałania
incydentom.
Elektroniczne podsystemy ochrony wykorzystują  lokalną  sieć  komputerową  systemu
bezpieczeństwa jako medium transmisji danych. Do tej sieci mogą  być  podłączone następujące
podsystemy:

1) Kontrola dostępu do stref zastrzeżonych.
2) System antywłamaniowy do pomieszczeń.
3) Telewizja przemysłowa.
4) Ochrona samolotów na stanowiskach postojowych.
5) Ochrona ogrodzenia zewnętrznego.
6) Ochrona bram wjazdowych.
7) Kontrola bezpieczeństwa bagażu (screening).
8) System rejestracji bagażu współpracujący z systemem kontroli odlotów w celu szybkiej
identyfikacji bagażu z pasażerem.
9) System monitoringu pojazdów na lotnisku.
10) System zarządzania parkingami.
Integracja wszystkich wymienionych systemów ochrony w jednym centrum operacyjnym jest
rozwiązaniem bezdyskusyjnym: Bazą  całego systemu ochrony jest sieć  komputerowa, która
synchronizuje współpracę  poszczególnych podsystemów i prowadzi bazę  danych. Poszczególne
urządzenia elektroniczne w systemie ochrony powinny mieć budowę modularną, tak aby można było
je niezależnie rozbudowywać.
</p>
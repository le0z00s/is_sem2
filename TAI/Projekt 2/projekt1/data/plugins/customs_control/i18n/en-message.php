<?php

/**
 * @package Translations
 */

return array(
		'Customs control' => 'Airport Security',
		'Customs_officer' => 'Airport security guard',
		'Image' => 'Image',
		'Provides methods for customs control like checking if passenger was previously prosecuted for smuggling' => 'Provides methods for customs control like checking if passenger was previously prosecuted for smuggling',
		'Security' => 'Security notes',
		'Suspects' => 'Suspects'
);
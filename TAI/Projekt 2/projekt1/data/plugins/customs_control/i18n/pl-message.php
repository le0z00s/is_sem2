<?php

/**
 * @package Translations
 */

return array(
		'Customs control' => 'Straż Lotniska',
		'Customs_officer' => 'Strażnik lotniska',
		'Image' => 'Zdjęcie',
		'Provides methods for customs control like checking if passenger was previously prosecuted for smuggling' => 'Zapewnia metody dla kontroli granicznej, np. sprawdzenie czy pasażer był już wcześniej zatrzymany za przemyt',
		'Security' => 'Bezpieczeństwo',
		'Suspects' => 'Zatrzymani'
);
<?php
/**
 * @package Plugins
 * @subpackage customs_control
 */

class Suspects extends Record{
	const TABLE_NAME = 'suspects';
		
	public $user_id = '';
	public $image = '';
	public $description = '';
	
	public static function findBy($column, $value) {
		return self::findOne(array(
				'where' => $column . ' = :value',
				'values' => array(':value' => $value)
		));
	}
	
	public function getColumns() {
		return array(
				'id', 'user_id', 'image', 'description'
		);
	}
	
	public static function findAll($args = null) {
		return self::find($args);
	}
}
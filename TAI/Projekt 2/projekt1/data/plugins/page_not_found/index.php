<?php
/**
 * Provides Page not found page types.
 *
 * @package Plugins
 * @subpackage page-not-found
 */

if (!defined('IN_CMS')) {
    exit();
}

Plugin::setInfos(array(
    'id' => 'page_not_found',
    'title' => __('Page not found'),
    'description' => __('Provides Page not found page types.')
));

Behavior::add('page_not_found', '');
Observer::observe('page_not_found', 'behavior_page_not_found');

function behavior_page_not_found($url) {
    $page = Page::findByBehaviour('page_not_found');

    if (is_a($page, 'Page')) {
        header("HTTP/1.0 404 Not Found");
        header("Status: 404 Not Found");

        $page->_executeLayout();
        exit();
    }
}

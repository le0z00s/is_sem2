<?php
/**
 * @package Translations
 */

return array(
    'Page not found' => 'Strona nie znaleziona',
    'Provides Page not found page types.' => 'Dostarcza typ strony \'Strona nie znaleziona\'.'
);

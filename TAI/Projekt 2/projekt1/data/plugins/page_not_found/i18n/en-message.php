<?php

/**
 * @package Translations
 */

return array(
    'Page not found' => 'Page not found',
    'Provides Page not found page types.' => 'Provides Page not found page types.'
);

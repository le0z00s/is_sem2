<?php
/**
 * @package Plugins
 * @subpackage flights
 */

if (!defined('IN_CMS')) { exit(); }

Plugin::setInfos(array(
    'id'          => 'flights',
    'title'       => __('Rides'),
    'description' => __('Provides methods for planes, airports and tickets')
));

if(Permission::findByName('flights_view')==NULL){
	$perm = new Permission();
	$perm->name = 'flights_view';
	$perm->save();
}
if(Role::findByName('flight_manager')==NULL){
	$role = new Role();
	$role->name = 'flight_manager';
	$role->save();
}

$admin_perms = RolePermission::findPermissionsFor(Role::findByName('administrator')->id);
$admin_perms[] = Permission::findByName('flights_view');
RolePermission::savePermissionsFor(Role::findByName('administrator')->id, $admin_perms);

$perms = array();
$perms[] = Permission::findByName('flights_view');
$perms[] = Permission::findByName('admin_view');
RolePermission::savePermissionsFor(Role::findByName('flight_manager')->id, $perms);


Plugin::addController('flights', __('Rides'), 'flights_view', true);

AutoLoader::addFolder(PLUGINS_ROOT.DS.'flights'.DS.'models');

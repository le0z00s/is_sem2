<?php
/**
 * @package Plugins
 * @subpackage flights
 */

if (!defined('IN_CMS')) { exit(); }

$PDO = Record::getConnection();
$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));

//Setup table structure
if ($driver == 'mysql') {
	//ticket types table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."ticket_type (
			id int(11) unsigned NOT NULL auto_increment,
			name varchar(50) default NULL,
			price_multiplier float(6,2) default NULL,
			PRIMARY KEY (id),
			KEY name (name)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");
	//ticket table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."ticket (
			id int(11) unsigned NOT NULL auto_increment,
			flight_id int(11) unsigned NOT NULL,
			user_id int(11) unsigned NOT NULL,
			type int(11) unsigned NOT NULL,
			baggage tinyint(1) unsigned NOT NULL default '0',
			date_ordered datetime default NULL,
			date_confirmed datetime default NULL,
			paid  tinyint(1) unsigned NOT NULL default '0',
			PRIMARY KEY (id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");
	//flight table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."flight (
			id int(11) unsigned NOT NULL auto_increment,
			flight_from int(11) unsigned NOT NULL,
			flight_to int(11) unsigned NOT NULL,
			plane_id int(11) unsigned NOT NULL,
			price float(6,2) unsigned NOT NULL,
			flight_day tinyint(1) unsigned NOT NULL,
			flight_hour time NOT NULL,
			PRIMARY KEY (id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");
	//airport table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."airport (
			id int(11) unsigned NOT NULL auto_increment,
			name varchar(50) default NULL,
			country_name varchar(50) default NULL,
			city_name varchar(50) default NULL,
			PRIMARY KEY (id),
			KEY name (name)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");
	//plane table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."plane (
			id int(11) unsigned NOT NULL auto_increment,
			name varchar(50) default NULL,
			manufacturer_name varchar(50) default NULL,
			model varchar(50) default NULL,
			seats int(11) unsigned default NULL,
			weight_tare float(11,2) unsigned default NULL,
			weight_take_off float(11,2) unsigned default NULL,
			range float(11,2) unsigned default NULL,
			fuel_max float(11,2) unsigned default NULL,
			PRIMARY KEY (id),
			KEY name (name)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");
}

else if ($driver == 'sqlite') {
	//ticket types table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ticket_type (
			id INTEGER NOT NULL PRIMARY KEY,
			name varchar(50) default NULL,
			price_multiplier REAL)");
	//ticket table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ticket (
			id INTEGER NOT NULL PRIMARY KEY,
			flight_id int(11) NOT NULL,
			user_id int(11) NOT NULL,
			type int(11) NOT NULL,
			baggage tinyint(1) NOT NULL default '0',
			date_ordered TEXT,
			date_confirmed TEXT,
			paid  tinyint(1) NOT NULL default '0')");
	//flight table
	$PDO->exec("CREATE TABLE IF NOT EXISTS flight (
			id INTEGER NOT NULL PRIMARY KEY,
			flight_from int(11) NOT NULL,
			flight_to int(11) NOT NULL,
			plane_id int(11) NOT NULL,
			flight_day tinyint(1) NOT NULL,
			flight_hour TEXT NOT NULL,
			price REAL)");
	//airport table
	$PDO->exec("CREATE TABLE IF NOT EXISTS airport (
			id INTEGER NOT NULL PRIMARY KEY,
			name varchar(50) default NULL,
			country_name varchar(50) default NULL,
			city_name varchar(50) default NULL)");
	//plane table
	$PDO->exec("CREATE TABLE IF NOT EXISTS plane (
			id INTEGER NOT NULL PRIMARY KEY,
			name varchar(50) default NULL,
			manufacturer_name varchar(50) default NULL,
			model varchar(50) default NULL,
			seats int(11) default NULL,
			weight_tare REAL,
			weight_take_off REAL,
			range REAL,
			fuel_max REAL)");
	
	//create indexes for tables
	$PDO->exec("CREATE INDEX ticket_type_name ON ticket_type (name)");
	$PDO->exec("CREATE INDEX airport_name ON airport (name)");
	$PDO->exec("CREATE INDEX plane_name ON plane (name)");
	
}

$settings = array('baggage_price' => '0'
);

Plugin::setAllSettings($settings, 'flights');
<?php
/**
 * @package Plugins
 * @subpackage flights
 */

class Airport extends Record{
	const TABLE_NAME = 'airport';
		
	public $name = '';
	public $country_name = '';
	public $city_name = '';
	
	public static function findBy($column, $value) {
		return self::findOne(array(
				'where' => $column . ' = :value',
				'values' => array(':value' => $value)
		));
	}
	
	public function getColumns() {
		return array(
				'id', 'name', 'country_name', 'city_name'
		);
	}
	
	public static function findAll($args = null) {
		return self::find($args);
	}
}
<?php
/**
 * @package Plugins
 * @subpackage flights
 */

class Flight extends Record{
	const TABLE_NAME = 'flight';
	
	public $flight_from = '';
	public $flight_to = '';
	public $plane_id = '';
	public $price = '';
	public $flight_day = '';
	public $flight_hour = '';
	
	public static function findBy($column, $value) {
		return self::findOne(array(
				'where' => $column . ' = :value',
				'values' => array(':value' => $value)
		));
	}
	
	public function getColumns() {
		return array(
				'id', 'flight_from', 'flight_to', 'plane_id', 'price', 'flight_day', 'flight_hour'
		);
	}
	
	public static function findAll($args = null) {
		return self::find($args);
	}
}
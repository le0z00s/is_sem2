<?php
/**
 * @package Plugins
 * @subpackage flights
 */

class TicketType extends Record{
	const TABLE_NAME = 'ticket_type';
	
	public $name = '';
	public $price_multiplier = '';
	
	public static function findBy($column, $value) {
		return self::findOne(array(
				'where' => $column . ' = :value',
				'values' => array(':value' => $value)
		));
	}
	
	public function getColumns() {
		return array(
				'id', 'name', 'price_multiplier'
		);
	}
	
	public static function findAll($args = null) {
        return self::find($args);
    }
    
}
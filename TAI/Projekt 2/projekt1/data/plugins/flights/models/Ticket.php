<?php
/**
 * @package Plugins
 * @subpackage flights
 */

class Ticket extends Record{
	const TABLE_NAME = 'ticket';
	
	public $flight_id = '';
	public $user_id = '';
	public $type = '';
	public $baggage = '';
	public $date_ordered = '';
	public $date_confirmed = '';
	public $paid = '';
	
	public static function findBy($column, $value) {
		return self::findOne(array(
				'where' => $column . ' = :value',
				'values' => array(':value' => $value)
		));
	}
	
	public function getColumns() {
		return array(
				'id', 'flight_id', 'user_id', 'type', 'baggage', 'date_ordered', 'date_confirmed', 'paid'
		);
	}
	
	public static function findAll($args = null) {
		return self::find($args);
	}
	
	public static function order_ticket($flight_id, $user_id, $type, $baggage, $date_ordered){
		$sql = "INSERT INTO ".self::TABLE_NAME." (flight_id, user_id, type, baggage, date_ordered) VALUES (?,?,?,?,?);";
		if(($con = self::getConnection()) !== false){
			$stmt = $con->prepare($sql);
			$stmt->bindValue( 1, $flight_id);
			$stmt->bindValue( 2, $user_id);
			$stmt->bindValue( 3, $type);
			$stmt->bindValue( 4, $baggage);
			$stmt->bindValue( 5, $date_ordered);
			if($stmt->execute()){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
}
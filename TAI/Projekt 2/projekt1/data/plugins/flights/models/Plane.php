<?php
/**
 * @package Plugins
 * @subpackage flights
 */

class Plane extends Record{
	const TABLE_NAME = 'plane';
	
	public $name = '';
	public $manufacturer_name = '';
	public $model = '';
	public $seats = '';
	public $weight_tare = '';
	public $weight_take_off = '';
	public $range = '';
	public $fuel_max = '';
	
	public static function findBy($column, $value) {
		return self::findOne(array(
				'where' => $column . ' = :value',
				'values' => array(':value' => $value)
		));
	}
	
	public function getColumns() {
		return array(
				'id', 'name', 'manufacturer_name', 'model', 'seats', 'weight_tare', 'weight_take_off', 'range', 'fuel_max'
		);
	}
	
	public static function findAll($args = null) {
		return self::find($args);
	}
}
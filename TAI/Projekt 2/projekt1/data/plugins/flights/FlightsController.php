<?php
/**
 * @package Plugins
 * @subpackage flights
 */

if (!defined('IN_CMS')) { exit(); }

class FlightsController extends PluginController {

    public function __construct() {
        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('../../plugins/flights/views/sidebar'));
    }

    public function index() {
        $this->documentation();
    }

    public function documentation() {
        $this->display('flights/views/documentation');
    }

    function airport(){
    	$this->display('flights/views/airport', array(
    			'airports' => Airport::findAll()
    	));
    }
    
    function flight(){
    	$this->display('flights/views/flight', array(
    			'flights' => Flight::findAll()
    	));
    }
    
    function plane(){
    	$this->display('flights/views/plane', array(
    			'planes' => Plane::findAll()
    	));
    }
    
    function ticket_type(){
    	$this->display('flights/views/ticket_type', array(
    			'ticket_types' => TicketType::findAll()
    	));
    }

    //airports add, edit, delete functions
    public function airport_add() {
    	if (get_request_method() == 'POST') {
    		$this->_airport_add();
    	}
    
    	$airport = Flash::get('post_data');
    
    	if (empty($airport)) {
    		$airport = new Airport();
    	}
    
    	$this->display('flights/views/airport/edit', array(
    			'action'  => 'add',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/airport_add'),
    			'airport' => $airport
    	));
    }
    
    private function _airport_add() {
    	$data = $_POST['airport'];
    
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/airport'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/airport_add')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/airport'));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/airport'));
    	}
    
    	$airport = new Airport($data);
    
    	if ( ! $airport->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Airport has not been added. Name must be unique!'));
    		redirect(get_url('plugin/flights/airport', 'add'));
    	}
    	else {
    		Flash::set('success', __('Airport has been added!'));
    		Observer::notify('airport_after_add', $airport);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/airport'));
    	}
    	else {
    		redirect(get_url('plugin/flights/airport_edit/'.$airport->id));
    	}
    }
    
    public function airport_edit($id) {
    	$airport = Flash::get('post_data');
    
    	if (empty($airport)) {
    		$airport = Airport::findById($id);
    		if ( !$airport ) {
    			Flash::set('error', __('Airport not found!'));
    			redirect(get_url('plugin/flights/airport'));
    		}
    	}
    
    	if (get_request_method() == 'POST') {
    		$this->_airport_edit($id);
    	}
    
    	$this->display('flights/views/airport/edit', array(
    			'action'  => 'edit',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/airport_edit'),
    			'airport' => $airport
    	));
    }
    
    private function _airport_edit($id) {
    	$data = $_POST['airport'];
    	$data['id'] = $id;
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/views/airport'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/airport_edit')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/airport_edit/'.$id));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/airport_edit/'.$id));
    	}
    
    	$airport = new Airport($data);
    
    	if ( ! $airport->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Airport :name has not been saved. Name must be unique!', array(':name'=>$airport->name)));
    		redirect(get_url('plugin/flights/airport_edit/'.$id));
    	}
    	else {
    		Flash::set('success', __('Airport :name has been saved!', array(':name'=>$airport->name)));
    		Observer::notify('airport_after_edit', $airport);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/airport'));
    	}
    	else {
    		redirect(get_url('plugin/flights/airport_edit/'.$id));
    	}
    }
    
    function airport_delete($id) {
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to delete this!'));
    		redirect(get_url('plugin/flights'));
    	}
    	 
    	if ($airport = Airport::findById($id)) {
    		if ($airport->delete()) {
    			Flash::set('success', __('Airport :name has been deleted!', array(':name'=>$airport->name)));
    			Observer::notify('airport_after_delete', $airport);
    		}
    		else
    			Flash::set('error', __('Airport :name has not been deleted!', array(':name'=>$airport->name)));
    	}
    	else
    		Flash::set('error', __('Airport not found!'));
    
    	redirect(get_url('plugin/flights/airport'));
    }
    
    //flights add, edit, delete functions
    public function flight_add() {
    	if (get_request_method() == 'POST') {
    		$this->_flight_add();
    	}
    
    	$flight = Flash::get('post_data');
    
    	if (empty($flight)) {
    		$flight = new Flight();
    	}
    
    	$this->display('flights/views/flight/edit', array(
    			'action'  => 'add',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/flight_add'),
    			'flight' => $flight
    	));
    }
    
    private function _flight_add() {
    	$data = $_POST['flight'];
    
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/flight'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/flight_add')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/flight'));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/flight'));
    	}
    
    	$flight = new Flight($data);
    
    	if ( ! $flight->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Flight has not been added. Name must be unique!'));
    		redirect(get_url('plugin/flights/flight', 'add'));
    	}
    	else {
    		Flash::set('success', __('Flight has been added!'));
    		Observer::notify('flight_after_add', $flight);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/flight'));
    	}
    	else {
    		redirect(get_url('plugin/flights/flight_edit/'.$flight->id));
    	}
    }
    
    public function flight_edit($id) {
    	$flight = Flash::get('post_data');
    
    	if (empty($flight)) {
    		$flight = Flight::findById($id);
    		if ( !$flight ) {
    			Flash::set('error', __('Flight not found!'));
    			redirect(get_url('plugin/flights/flight'));
    		}
    	}
    
    	if (get_request_method() == 'POST') {
    		$this->_flight_edit($id);
    	}
    
    	$this->display('flights/views/flight/edit', array(
    			'action'  => 'edit',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/flight_edit'),
    			'flight' => $flight
    	));
    }
    
    private function _flight_edit($id) {
    	$data = $_POST['flight'];
    	$data['id'] = $id;
    
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/views/flight'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/flight_edit')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/flight_edit/'.$id));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/flight_edit/'.$id));
    	}
    
    	$flight = new Flight($data);
    
    	if ( ! $flight->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Flight has not been saved!'));
    		redirect(get_url('plugin/flights/flight_edit/'.$id));
    	}
    	else {
    		Flash::set('success', __('Flight has been saved!'));
    		Observer::notify('flight_after_edit', $flight);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/flight'));
    	}
    	else {
    		redirect(get_url('plugin/flights/flight_edit/'.$id));
    	}
    }
    
    function flight_delete($id) {
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to delete this!'));
    		redirect(get_url('plugin/flights'));
    	}
    	 
    	if ($flight = Flight::findById($id)) {
    		if ($flight->delete()) {
    			Flash::set('success', __('Flight has been deleted!'));
    			Observer::notify('flight_after_delete', $flight);
    		}
    		else
    			Flash::set('error', __('Flight has not been deleted!'));
    	}
    	else
    		Flash::set('error', __('Flight not found!'));
    
    	redirect(get_url('plugin/flights/flight'));
    }
    
    //planes add, edit, delete functions
    public function plane_add() {
    	if (get_request_method() == 'POST') {
    		$this->_plane_add();
    	}
    
    	$plane = Flash::get('post_data');
    
    	if (empty($plane)) {
    		$plane = new Plane();
    	}
    
    	$this->display('flights/views/plane/edit', array(
    			'action'  => 'add',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/plane_add'),
    			'plane' => $plane
    	));
    }
    
    private function _plane_add() {
    	$data = $_POST['plane'];
    
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/plane'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/plane_add')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/plane'));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/plane'));
    	}
    
    	$plane = new Plane($data);
    
    	if ( ! $plane->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Plane has not been added. Name must be unique!'));
    		redirect(get_url('plugin/flights/plane', 'add'));
    	}
    	else {
    		Flash::set('success', __('Plane has been added!'));
    		Observer::notify('plane_after_add', $plane);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/plane'));
    	}
    	else {
    		redirect(get_url('plugin/flights/plane_edit/'.$plane->id));
    	}
    }
    
    public function plane_edit($id) {
    	$plane = Flash::get('post_data');
    
    	if (empty($plane)) {
    		$plane = Plane::findById($id);
    		if ( !$plane ) {
    			Flash::set('error', __('Plane not found!'));
    			redirect(get_url('plugin/flights/plane'));
    		}
    	}
    
    	if (get_request_method() == 'POST') {
    		$this->_plane_edit($id);
    	}
    
    	$this->display('flights/views/plane/edit', array(
    			'action'  => 'edit',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/plane_edit'),
    			'plane' => $plane
    	));
    }
    
    private function _plane_edit($id) {
    	$data = $_POST['plane'];
    	$data['id'] = $id;
    
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/views/plane'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/plane_edit')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/plane_edit/'.$id));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/plane_edit/'.$id));
    	}
    
    	$plane = new Plane($data);
    
    	if ( ! $plane->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Plane :name has not been saved. Name must be unique!', array(':name'=>$plane->name)));
    		redirect(get_url('plugin/flights/plane_edit/'.$id));
    	}
    	else {
    		Flash::set('success', __('Plane :name has been saved!', array(':name'=>$plane->name)));
    		Observer::notify('plane_after_edit', $plane);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/plane'));
    	}
    	else {
    		redirect(get_url('plugin/flights/plane_edit/'.$id));
    	}
    }
    
    function plane_delete($id) {
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to delete this!'));
    		redirect(get_url('plugin/flights'));
    	}
    	 
    	if ($plane = Plane::findById($id)) {
    		if ($plane->delete()) {
    			Flash::set('success', __('Plane :name has been deleted!', array(':name'=>$plane->name)));
    			Observer::notify('plane_after_delete', $plane);
    		}
    		else
    			Flash::set('error', __('Plane :name has not been deleted!', array(':name'=>$plane->name)));
    	}
    	else
    		Flash::set('error', __('Plane not found!'));
    
    	redirect(get_url('plugin/flights/plane'));
    }
    
    //ticket types add, edit, delete functions
    public function ticket_type_add() {
    	if (get_request_method() == 'POST') {
    		$this->_ticket_type_add();
    	}
    
    	$ticket_type = Flash::get('post_data');
    
    	if (empty($ticket_type)) {
    		$ticket_type = new TicketType();
    	}
    
    	$this->display('flights/views/ticket_type/edit', array(
    			'action'  => 'add',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/ticket_type_add'),
    			'ticket_type' => $ticket_type
    	));
    }
    
    private function _ticket_type_add() {
    	$data = $_POST['ticket_type'];
    
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/ticket_type'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/ticket_type_add')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/ticket_type'));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/ticket_type'));
    	}
    
    	$ticket_type = new TicketType($data);
    
    	if ( ! $ticket_type->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Ticket type has not been added. Name must be unique!'));
    		redirect(get_url('plugin/flights/ticket_type', 'add'));
    	}
    	else {
    		Flash::set('success', __('Ticket type has been added!'));
    		Observer::notify('ticket_type_after_add', $ticket_type);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/ticket_type'));
    	}
    	else {
    		redirect(get_url('plugin/flights/ticket_type_edit/'.$ticket_type->id));
    	}
    }
    
    public function ticket_type_edit($id) {
    	$ticket_type = Flash::get('post_data');
    
    	if (empty($ticket_type)) {
    		$ticket_type = TicketType::findById($id);
    		if ( !$ticket_type ) {
    			Flash::set('error', __('Ticket type not found!'));
    			redirect(get_url('plugin/flights/ticket_type'));
    		}
    	}
    
    	if (get_request_method() == 'POST') {
    		$this->_ticket_type_edit($id);
    	}
    
    	$this->display('flights/views/ticket_type/edit', array(
    			'action'  => 'edit',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/flights/ticket_type_edit'),
    			'ticket_type' => $ticket_type
    	));
    }
    
    private function _ticket_type_edit($id) {
    	$data = $_POST['ticket_type'];
    	$data['id'] = $id;
    
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/flights/views/ticket_type'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/flights/ticket_type_edit')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/flights/ticket_type_edit/'.$id));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/flights/ticket_type_edit/'.$id));
    	}
    
    	$ticket_type = new TicketType($data);
    
    	if ( ! $ticket_type->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Ticket type :name has not been saved. Name must be unique!', array(':name'=>$ticket_type->name)));
    		redirect(get_url('plugin/flights/ticket_type_edit/'.$id));
    	}
    	else {
    		Flash::set('success', __('Ticket type :name has been saved!', array(':name'=>$ticket_type->name)));
    		Observer::notify('ticket_type_after_edit', $ticket_type);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/flights/ticket_type'));
    	}
    	else {
    		redirect(get_url('plugin/flights/ticket_type_edit/'.$id));
    	}
    }
    
    function ticket_type_delete($id) {
    	if (!AuthUser::hasPermission('flights_view')) {
    		Flash::set('error', __('You do not have permission to delete this!'));
    		redirect(get_url('plugin/flights'));
    	}
    	
    	if ($ticket_type = TicketType::findById($id)) {
    		if ($ticket_type->delete()) {
    			Flash::set('success', __('Ticket type :name has been deleted!', array(':name'=>$ticket_type->name)));
    			Observer::notify('ticket_type_after_delete', $ticket_type);
    		}
    		else
    			Flash::set('error', __('Ticket type :name has not been deleted!', array(':name'=>$ticket_type->name)));
    	}
    	else
    		Flash::set('error', __('Ticket type not found!'));
    
    	redirect(get_url('plugin/flights/ticket_type'));
    }    
    
    /*function settings() {
        $this->display('flights/views/settings', Plugin::getAllSettings('flights'));
    }*/
}
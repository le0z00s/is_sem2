<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Airports'); ?></h1>


<p class="button"><a href="<?php echo get_url('plugin/flights/airport_add'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Add'); ?></a></p>

<table id="airports" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Country'); ?></th>
      <th><?php echo __('City'); ?></th>
      <th><?php echo __('Modify'); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($airports as $airport): ?> 
    <tr class="node <?php echo odd_even(); ?>">
      <td class="airport">
        <a href="<?php echo get_url('plugin/flights/airport_edit/'.$airport->id); ?>"><?php echo $airport->name; ?></a>
      </td>
      <td class="airport">
        <?php echo $airport->country_name; ?>
      </td>
      <td class="airport">
        <?php echo $airport->city_name; ?>
      </td>
      <td>
        <a href="<?php echo get_url('plugin/flights/airport_delete/'.$airport->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$airport->name.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete airport'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
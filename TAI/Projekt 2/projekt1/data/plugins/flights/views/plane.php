<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Planes'); ?></h1>


<p class="button"><a href="<?php echo get_url('plugin/flights/plane_add'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Add'); ?></a></p>

<table id="plane" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Manufacturer'); ?></th>
      <th><?php echo __('Model'); ?></th>
      <th><?php echo __('Seats'); ?></th>
      <th><?php echo __('Weight tare'); ?></th>
      <th><?php echo __('Weight take off'); ?></th>
      <th><?php echo __('Range'); ?></th>
      <th><?php echo __('Fuel max'); ?></th>
      <th><?php echo __('Modify'); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($planes as $plane): ?> 
    <tr class="node <?php echo odd_even(); ?>">
      <td class="plane">
        <a href="<?php echo get_url('plugin/flights/plane_edit/'.$plane->id); ?>"><?php echo $plane->name?></a>
      </td>
      <td><?php echo $plane->manufacturer_name; ?></td>
      <td><?php echo $plane->model; ?></td>
      <td><?php echo $plane->seats; ?></td>
      <td><?php echo $plane->weight_tare; ?></td>
      <td><?php echo $plane->weight_take_off; ?></td>
      <td><?php echo $plane->range; ?></td>
      <td><?php echo $plane->fuel_max; ?></td>
      <td>
        <a href="<?php echo get_url('plugin/flights/plane_delete/'.$plane->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$plane->name.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete plane'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
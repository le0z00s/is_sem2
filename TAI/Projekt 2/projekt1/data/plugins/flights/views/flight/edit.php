<?php
/**
 * @package Views
 */
?>
<h1><?php echo __(ucfirst($action).' Flight'); ?></h1>

<form action="<?php echo $action=='edit' ? get_url('plugin/flights/flight_edit/'.$flight->id): get_url('plugin/flights/flight_add'); ; ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
  <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
    
    <?php $airports = Airport::findAll(); ?>
    
    <tr>
        <td class="label"><label for="flight_flight_from"><?php echo __('Flight From'); ?></label></td>
        <td class="field">
          <select class="select" id="flight_flight_from" name="flight[flight_from]">
<?php foreach ($airports as $airport): ?>
            <option value="<?php echo $airport->id; ?>"<?php if ($flight->flight_from == $airport->id) echo ' selected="selected"'; ?>><?php echo $airport->name; ?></option>
<?php endforeach; ?>
          </select>
        </td>
        <td class="help"></td>
    </tr>
    
    <tr>
        <td class="label"><label for="flight_flight_to"><?php echo __('Flight To'); ?></label></td>
        <td class="field">
          <select class="select" id="flight_flight_to" name="flight[flight_to]">
<?php foreach ($airports as $airport): ?>
            <option value="<?php echo $airport->id; ?>"<?php if ($flight->flight_to == $airport->id) echo ' selected="selected"'; ?>><?php echo $airport->name; ?></option>
<?php endforeach; ?>
          </select>
        </td>
        <td class="help"></td>
    </tr>
    
    <?php $planes = Plane::findAll();?>
    <tr>
        <td class="label"><label for="flight_plane_id"><?php echo __('Plane'); ?></label></td>
        <td class="field">
          <select class="select" id="flight_plane_id" name="flight[plane_id]">
<?php foreach ($planes as $plane): ?>
            <option value="<?php echo $plane->id; ?>"<?php if ($flight->plane_id == $plane->id) echo ' selected="selected"'; ?>><?php echo $plane->name; ?></option>
<?php endforeach; ?>
          </select>
        </td>
        <td class="help"></td>
    </tr>

    <tr>
        <td class="label"><label for="flight_flight_day"><?php echo __('Flight Day'); ?></label></td>
        <td class="field">
          <select class="select" id="flight_flight_day" name="flight[flight_day]">
			<?php for($i = 1; $i<8; $i++): ?>
            <option value="<?php echo $i; ?>"<?php if ($flight->flight_day == $i) echo ' selected="selected"'; ?>>
            <?php 
		      	switch ($flight->flight_day) {
		      		case "1":
		      			echo __('Monday');
		      			break;
		  			case "2":
		   				echo __('Tuesday');
		   				break;
					case "3":
						echo __('Wednesday');
						break;
		      		case "4":
		      			echo __('Thursday');
		      			break;
		      		case "5":
		      			echo __('Friday');
		      			break;
		      		case "6":
		      			echo __('Saturday');
		      			break;
		      		case "7":
		      			echo __('Sunday');
		      			break;
		      }?>
            </option>
			<?php endfor; ?>
          </select>
        </td>
        <td class="help"></td>
    </tr>
    
    <tr>    
      <td class="label"><label for="flight_flight_hour"><?php echo __('Flight Hour'); ?></label></td>
      <td class="field"><input class="textbox" id="flight_flight_hour" maxlength="100" name="flight[flight_hour]" size="100" type="time" value="<?php echo $flight->flight_hour; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    <tr>

      <td class="label"><label for="flight_price"><?php echo __('Price'); ?></label></td>
      <td class="field"><input class="textbox" id="flight_price" maxlength="100" name="flight[price]" size="100" type="number" step="0.01" min="0" value="<?php echo $flight->price; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
  </table>

<?php Observer::notify('flight_edit_view_after_details', $flight); ?>

  <p class="buttons">
    <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    <?php echo __('or'); ?> <a href="<?php echo (AuthUser::hasPermission('flights_view')) ? get_url('plugin/flights/flight') : get_url(); ?>"><?php echo __('Cancel'); ?></a>
  </p>

</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
Field.activate('flight_flight_from');
// ]]>
</script>

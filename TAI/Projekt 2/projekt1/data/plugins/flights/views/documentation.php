<?php
/**
 * @package Plugins
 * @subpackage flights
 */

if (!defined('IN_CMS')) { exit(); }

?>
<h1><?php echo __('Documentation'); ?></h1>
<p>
<?php echo __('Provides methods for planes, airports and tickets'); ?>
</p>

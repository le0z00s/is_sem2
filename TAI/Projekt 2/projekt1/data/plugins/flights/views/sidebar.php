<?php
/**
 * @package Plugins
 * @subpackage flights
 */

if (!defined('IN_CMS')) { exit(); }

?>
<p class="button"><a href="<?php echo get_url('plugin/flights/airport'); ?>"><img src="<?php echo PLUGINS_PATH?>flights/icons/plane-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Airports'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/flights/flight'); ?>"><img src="<?php echo PLUGINS_PATH?>flights/icons/ticket-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Flights'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/flights/plane'); ?>"><img src="<?php echo PLUGINS_PATH?>flights/icons/plane-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Planes'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/flights/ticket_type'); ?>"><img src="<?php echo PLUGINS_PATH?>flights/icons/ticket-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Ticket Types'); ?></a></p>
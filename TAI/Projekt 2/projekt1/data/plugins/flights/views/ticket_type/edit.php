<?php
/**
 * @package Views
 */
?>
<h1><?php echo __(ucfirst($action).' Ticket Type'); ?></h1>

<form action="<?php echo $action=='edit' ? get_url('plugin/flights/ticket_type_edit/'.$ticket_type->id): get_url('plugin/flights/ticket_type_add'); ; ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
  <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
    <tr>

      <td class="label"><label for="ticket_type_name"><?php echo __('Name'); ?></label></td>
      <td class="field"><input class="textbox" id="ticket_type_name" maxlength="100" name="ticket_type[name]" size="100" type="text" value="<?php echo $ticket_type->name; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    <tr>

      <td class="label"><label for="ticket_type_price_multiplier"><?php echo __('Price Modifier'); ?></label></td>
      <td class="field"><input class="textbox" id="ticket_type_price_multiplier" maxlength="100" name="ticket_type[price_multiplier]" size="100" type="number" step="0.01" value="<?php echo $ticket_type->price_multiplier; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
  </table>

<?php Observer::notify('ticket_type_edit_view_after_details', $ticket_type); ?>

  <p class="buttons">
    <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    <?php echo __('or'); ?> <a href="<?php echo (AuthUser::hasPermission('flights_view')) ? get_url('plugin/flights/ticket_type') : get_url(); ?>"><?php echo __('Cancel'); ?></a>
  </p>

</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
Field.activate('ticket_type_name');
// ]]>
</script>

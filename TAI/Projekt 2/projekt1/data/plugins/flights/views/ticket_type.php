<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Ticket Types'); ?></h1>


<p class="button"><a href="<?php echo get_url('plugin/flights/ticket_type_add'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Add'); ?></a></p>

<table id="ticket_types" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Price Modifier'); ?></th>
      <th><?php echo __('Modify'); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($ticket_types as $ticket_type): ?> 
    <tr class="node <?php echo odd_even(); ?>">
      <td class="ticket_type">
        <a href="<?php echo get_url('plugin/flights/ticket_type_edit/'.$ticket_type->id); ?>"><?php echo $ticket_type->name?></a>
      </td>
      <td><?php echo $ticket_type->price_multiplier; ?></td>
      <td>
        <a href="<?php echo get_url('plugin/flights/ticket_type_delete/'.$ticket_type->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$ticket_type->name.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete ticket type'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
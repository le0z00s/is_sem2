<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Flights'); ?></h1>


<p class="button"><a href="<?php echo get_url('plugin/flights/flight_add'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Add'); ?></a></p>

<table id="flights" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('Flight From').' - '.__('Flight To'); ?></th>
      <th><?php echo __('Plane'); ?></th>
      <th><?php echo __('Flight Day'); ?></th>
      <th><?php echo __('Flight Hour'); ?></th>
      <th><?php echo __('Price'); ?></th>
      <th><?php echo __('Modify'); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($flights as $flight): ?> 
    <tr class="node <?php echo odd_even(); ?>">
      <td class="flight">
        <a href="<?php echo get_url('plugin/flights/flight_edit/'.$flight->id); ?>"><?php echo Airport::findById($flight->flight_from)->name.' - '.Airport::findById($flight->flight_to)->name; ?></a>
      </td>
      <td><?php echo Plane::findById($flight->plane_id)->name; ?></td>
      <td><?php 
      	switch ($flight->flight_day) {
      		case "1":
      			echo __('Monday');
      			break;
  			case "2":
   				echo __('Tuesday');
   				break;
			case "3":
				echo __('Wednesday');
				break;
      		case "4":
      			echo __('Thursday');
      			break;
      		case "5":
      			echo __('Friday');
      			break;
      		case "6":
      			echo __('Saturday');
      			break;
      		case "7":
      			echo __('Sunday');
      			break;
      }?></td>
      <td><?php echo $flight->flight_hour; ?></td>
      <td><?php echo $flight->price; ?></td>
      <td>
        <a href="<?php echo get_url('plugin/flights/flight_delete/'.$flight->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.Airport::findById($flight->flight_from)->name.' '.Airport::findById($flight->flight_to)->name.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete flight'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
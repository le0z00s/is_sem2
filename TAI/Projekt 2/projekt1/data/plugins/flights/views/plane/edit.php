<?php
/**
 * @package Views
 */
?>
<h1><?php echo __(ucfirst($action).' Plane'); ?></h1>

<form action="<?php echo $action=='edit' ? get_url('plugin/flights/plane_edit/'.$plane->id): get_url('plugin/flights/plane_add'); ; ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
  <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td class="label"><label for="plane_name"><?php echo __('Name'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_name" maxlength="100" name="plane[name]" size="100" type="text" value="<?php echo $plane->name; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    
    <tr>
      <td class="label"><label for="plane_manufacturer_name"><?php echo __('Manufacturer'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_manufacturer_name" maxlength="100" name="plane[manufacturer_name]" size="100" type="text" value="<?php echo $plane->manufacturer_name; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    
    <tr>
      <td class="label"><label for="plane_model"><?php echo __('Model'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_model" maxlength="100" name="plane[model]" size="100" type="text" value="<?php echo $plane->model; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    
    <tr>
      <td class="label"><label for="plane_seats"><?php echo __('Seats'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_seats" maxlength="100" name="plane[seats]" size="100" type="number" step="1" min="0" value="<?php echo $plane->seats; ?>" /></td>
      <td class="help"></td>
    </tr>
    
    <tr>
      <td class="label"><label for="plane_weight_tare"><?php echo __('Weight tare'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_weight_tare" maxlength="100" name="plane[weight_tare]" size="100" type="number" step="1" min="0" value="<?php echo $plane->weight_tare; ?>" /></td>
      <td class="help"></td>
    </tr>
    
    <tr>
      <td class="label"><label for="plane_weight_take_off"><?php echo __('Weight take off'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_weight_take_off" maxlength="100" name="plane[weight_take_off]" size="100" type="number" step="1" min="0" value="<?php echo $plane->weight_take_off; ?>" /></td>
      <td class="help"></td>
    </tr>
    
    <tr>
      <td class="label"><label for="plane_range"><?php echo __('Range'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_range" maxlength="100" name="plane[range]" size="100" type="number" step="1" min="0" value="<?php echo $plane->range; ?>" /></td>
      <td class="help"></td>
    </tr>
    
    <tr>
      <td class="label"><label for="plane_fuel_max"><?php echo __('Fuel max'); ?></label></td>
      <td class="field"><input class="textbox" id="plane_fuel_max" maxlength="100" name="plane[fuel_max]" size="100" type="number" step="0.01" value="<?php echo $plane->fuel_max; ?>" /></td>
      <td class="help"></td>
    </tr>
  </table>

<?php Observer::notify('plane_edit_view_after_details', $plane); ?>

  <p class="buttons">
    <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    <?php echo __('or'); ?> <a href="<?php echo (AuthUser::hasPermission('flights_view')) ? get_url('plugin/flights/plane') : get_url(); ?>"><?php echo __('Cancel'); ?></a>
  </p>

</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
Field.activate('plane_name');
// ]]>
</script>

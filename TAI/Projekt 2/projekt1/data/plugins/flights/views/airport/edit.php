<?php
/**
 * @package Views
 */
?>
<h1><?php echo __(ucfirst($action).' Airport'); ?></h1>

<form action="<?php echo $action=='edit' ? get_url('plugin/flights/airport_edit/'.$airport->id): get_url('plugin/flights/airport_add'); ; ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
  <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
    <tr>
      <td class="label"><label for="airport_name"><?php echo __('Name'); ?></label></td>
      <td class="field"><input class="textbox" id="airport_name" maxlength="100" name="airport[name]" size="100" type="text" value="<?php echo $airport->name; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    
    <tr>
      <td class="label"><label for="airport_country_name"><?php echo __('Country'); ?></label></td>
      <td class="field"><input class="textbox" id="airport_country_name" maxlength="100" name="airport[country_name]" size="100" type="text" value="<?php echo $airport->country_name; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    
    <tr>
      <td class="label"><label for="airport_city_name"><?php echo __('City'); ?></label></td>
      <td class="field"><input class="textbox" id="airport_city_name" maxlength="100" name="airport[city_name]" size="100" type="text" value="<?php echo $airport->city_name; ?>" /></td>
      <td class="help"><?php echo __('Required.'); ?></td>
    </tr>
    
    
  </table>

<?php Observer::notify('airport_edit_view_after_details', $airport); ?>

  <p class="buttons">
    <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    <?php echo __('or'); ?> <a href="<?php echo (AuthUser::hasPermission('flights_view')) ? get_url('plugin/flights/airport') : get_url(); ?>"><?php echo __('Cancel'); ?></a>
  </p>

</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
Field.activate('airport_name');
// ]]>
</script>

<?php
/**
 * @package Plugins
 * @subpackage flights
 */

if (!defined('IN_CMS')) { exit(); }

if (Plugin::deleteAllSettings('flights') === false) {
    Flash::set('error', __('Unable to delete plugin settings.'));
    redirect(get_url('setting'));
}

$PDO = Record::getConnection();

if ($PDO->exec('DROP TABLE IF EXISTS '.TABLE_PREFIX.'ticket_type') === false) {
	Flash::set('error', __('Unable to drop table :tablename', array(':tablename' => TABLE_PREFIX.'ticket_type')));
	redirect(get_url('setting'));
}
if ($PDO->exec('DROP TABLE IF EXISTS '.TABLE_PREFIX.'ticket') === false) {
	Flash::set('error', __('Unable to drop table :tablename', array(':tablename' => TABLE_PREFIX.'ticket')));
	redirect(get_url('setting'));
}
if ($PDO->exec('DROP TABLE IF EXISTS '.TABLE_PREFIX.'flight') === false) {
	Flash::set('error', __('Unable to drop table :tablename', array(':tablename' => TABLE_PREFIX.'flight')));
	redirect(get_url('setting'));
}
if ($PDO->exec('DROP TABLE IF EXISTS '.TABLE_PREFIX.'airport') === false) {
	Flash::set('error', __('Unable to drop table :tablename', array(':tablename' => TABLE_PREFIX.'airport')));
	redirect(get_url('setting'));
}
if ($PDO->exec('DROP TABLE IF EXISTS '.TABLE_PREFIX.'plane') === false) {
	Flash::set('error', __('Unable to drop table :tablename', array(':tablename' => TABLE_PREFIX.'plane')));
	redirect(get_url('setting'));
}

$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));
$ret = true;

if ($driver == 'sqlite') {
	$ret = $PDO->exec('DROP INDEX IF EXISTS '.TABLE_PREFIX.'ticket_type.ticket_type_name');
	if ($ret === false) break;
	$ret = $PDO->exec('DROP INDEX IF EXISTS '.TABLE_PREFIX.'airport.airport_name');
	if ($ret === false) break;
	$ret = $PDO->exec('DROP INDEX IF EXISTS '.TABLE_PREFIX.'plane.plane_name');
	if ($ret === false) break;	

	$ret = $PDO->exec('VACUUM');
}

if ($ret === false) {
	Flash::set('error', __('Unable to clean up table alterations.'));
	redirect(get_url('setting'));
}
else {
	Flash::set('success', __('Successfully uninstalled plugin.'));
	redirect(get_url('setting'));
}
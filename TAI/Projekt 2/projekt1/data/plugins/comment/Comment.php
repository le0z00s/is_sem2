<?php
/**
 * @package Plugins
 * @subpackage comment
 */
if (!defined('IN_CMS')) { exit(); }

class Comment extends Record
{
    const TABLE_NAME = 'comment';
    const NONE = 0;
    const OPEN = 1;
    const CLOSED = 2;

    public static function findAll($args = null) {
        return self::find(array(
            'limit' => 10
        ));
    }

    public static function findApproved() {
        return self::find(array(
            'where' => 'is_approved = 1'
        ));
    }

    public static function findApprovedByPageId($id) {
        return self::find(array(
            'where' => 'is_approved = 1 AND page_id = ?',
            'values' => array((int) $id)
        ));
    }


    function name($class='')
    {
        if ($this->author_link != '')
        {
            if ($class != '') {
                $fullclass = 'class="'.$class.'" ';
            } else {
                $fullclass = '';
            };

            return sprintf(
                '<a %s href="%s" title="%s">%s</a>',
                $fullclass,
                $this->author_link,
                $this->author_name,
                $this->author_name
            );
        }
            else return $this->author_name;
    }

    function email() { return $this->author_email; }
    function link() { return $this->author_link; }
    function body() { return $this->body; }

    function date($format='%a, %d %m %Y')
    {
        return strftime($format, strtotime($this->created_on));
    }


}

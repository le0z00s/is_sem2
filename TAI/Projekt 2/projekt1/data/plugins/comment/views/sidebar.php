<?php
/**
 * @package Plugins
 * @subpackage comment
 */

if (!defined('IN_CMS')) { exit(); }

?>
<p class="button"><a href="<?php echo get_url('plugin/comment/'); ?>"><img src="<?php echo ICONS_PATH;?>comment-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Comments'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/comment/moderation/'); ?>"><img src="<?php echo ICONS_PATH;?>action-approve-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Moderation'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/comment/documents/'); ?>"><img src="<?php echo ICONS_PATH;?>action-approve-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Documents'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/comment/staff/'); ?>"><img src="<?php echo ICONS_PATH;?>action-approve-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Staff'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/comment/customers/'); ?>"><img src="<?php echo ICONS_PATH;?>action-approve-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Customers'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/comment/orders/'); ?>"><img src="<?php echo ICONS_PATH;?>action-approve-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Orders'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/comment/analysis/'); ?>"><img src="<?php echo ICONS_PATH;?>action-approve-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Analysis'); ?></a></p>
<p class="button"><a href="<?php echo get_url('plugin/comment/settings'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Settings'); ?></a></p>

<?php
/**
 * @package Plugins
 * @subpackage comment
 */

if (!defined('IN_CMS')) { exit(); }

if (Plugin::deleteAllSettings('comment') === false) {
    Flash::set('error', __('Unable to delete plugin settings.'));
    redirect(get_url('setting'));
}

$PDO = Record::getConnection();

if ($PDO->exec('DROP TABLE IF EXISTS '.TABLE_PREFIX.'comment') === false) {
    Flash::set('error', __('Unable to drop table :tablename', array(':tablename' => TABLE_PREFIX.'comment')));
    redirect(get_url('setting'));
}

$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));
$ret = true;

if ($driver == 'mysql') {
    $ret = $PDO->exec('ALTER TABLE '.TABLE_PREFIX.'page DROP comment_status');
}
else if ($driver == 'sqlite') {
    $ret = $PDO->exec('DROP INDEX IF EXISTS '.TABLE_PREFIX.'comment.comment_page_id');
    if ($ret === false) break;
    $ret = $PDO->exec('DROP INDEX IF EXISTS '.TABLE_PREFIX.'comment.comment_created_on');
    if ($ret === false) break;

    $ret = $PDO->exec('VACUUM');
}

if ($ret === false) {
    Flash::set('error', __('Unable to clean up table alterations.'));
    redirect(get_url('setting'));
}
else {
    Flash::set('success', __('Successfully uninstalled plugin.'));
    redirect(get_url('setting'));
}

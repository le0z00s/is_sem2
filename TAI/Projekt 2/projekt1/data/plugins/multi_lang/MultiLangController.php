<?php
/**
 * @package Plugins
 * @subpackage multi-lang
 */

if (!defined('IN_CMS')) { exit(); }

class MultiLangController extends PluginController {
    static public $urilang = false;

    private static function _checkPermission() {
        AuthUser::load();
        if ( ! AuthUser::isLoggedIn()) {
            redirect(get_url('login'));
        }
    }

    function __construct() {
        self::_checkPermission();
        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('../../plugins/multi_lang/views/sidebar'));
    }

    public function index() {
        $this->documentation();
    }

    public function documentation() {
        $this->display('multi_lang/views/documentation');
    }

    function settings() {
        $this->display('multi_lang/views/settings', array('settings' => Plugin::getAllSettings('multi_lang')));
    }

    function save() {
		$settings = $_POST['settings'];

        $ret = Plugin::setAllSettings($settings, 'multi_lang');

        if ($ret)
            Flash::set('success', __('The settings have been updated.'));
        else
            Flash::set('error', 'An error has occurred while trying to save the settings.');

        redirect(get_url('plugin/multi_lang/settings'));
	}
}

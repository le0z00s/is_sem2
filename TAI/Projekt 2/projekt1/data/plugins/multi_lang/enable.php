<?php
/**
 * @package Plugins
 * @subpackage multi-lang
 */

if (!defined('IN_CMS')) { exit(); }

$PDO = Record::getConnection();
$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));

// Store settings
$settings = array('style' => 'tab',
                  'langsource' => 'header'
                 );

Plugin::setAllSettings($settings, 'multi_lang');

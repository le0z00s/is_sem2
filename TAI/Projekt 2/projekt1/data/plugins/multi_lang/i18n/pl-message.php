<?php

/**
 * @package Translations
 */

return array(
    'Do you want to create a translated version of a page as a tab of the same page or as a copy of the page in a language specific subtree? (i.e. Home->nl->About as a Dutch translation of Home->About)' => 'Czy chcesz utworzyć przetłumaczoną wersję strony jako zakładkę tej samej strony czy jako kopię wszystkich stron w wybranej gałęzi języka? (np. Home->pl->About to będzie polskie tłumaczenie strony Home->About)',
    'Documentation' => 'Dokumentacja',
    'General' => 'Ogólne',
    'Get the language preference from the HTTP header (default), the uri (/nl/about.html for the Dutch version of about.html) or from the stored preference of a logged in user.' => 'Wczytaj preferencje z nagłówka HTTP (domyślnie), adres (/pl/about.html dla polskiej wersji about.html) lub z zapamiętanych ustawień zalogowanego użytkownika.',
    'HTTP_ACCEPT_LANG header' => 'nagłówek HTTP_ACCEPT_LANG',
    'Language source' => 'Źródło języka',
    'I18n Documentation' => 'Dokumentacja I18n',
    'I18n Settings' => 'Ustawienia I18n',
    'I18n' => 'Internacjonalizacja',
    'Provides language specific content when available based on user preferences.' => 'Umożliwia przełączenie zawartości strony na inny język jeśli istnieje tłumaczenie.',
    'Save' => 'Zapisz',
    'Settings' => 'Ustawienia',
    'Style' => 'Styl',
    'The settings have been updated.' => 'Ustawienia zostały zaktualizowane',
    'Translations as page copy' => 'Tłumaczenie jako kopia strony',
    'Translations as tab' => 'Tłumaczenie jako zakładka',
    'URI' => 'URI',
    'User preferences' => 'Ustawienia użytkownika',
    'You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.' => 'Wprowadziłeś zmiany do strony.  Jeśli przejdziesz do innej bez zapisania, zmiany zostaną utracone.'
);

<?php
/**
 * @package Views
 */
?>
<h1><?php echo __('Edit Ticket'); ?></h1>

<form action="<?php echo get_url('plugin/bookkeeping/ticket_edit/'.$ticket->id); ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
  	<input id="ticket_user_id" name="ticket[user_id]" type="hidden" value="<?php echo $ticket->user_id; ?>">
  	<input id="ticket_flight_id" name="ticket[flight_id]" type="hidden" value="<?php echo $ticket->flight_id; ?>">
  	<input id="ticket_type" name="ticket[type]" type="hidden" value="<?php echo $ticket->type; ?>">
  <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
    
    
    <tr>
    	<td class="label"><?php echo __('First Name').' '.__('And').' '.__('Last Name'); ?></td>
    	<td class="field">
    			<?php $ticket_user = User::findById($ticket->user_id);?>
    			<?php echo $ticket_user->name.' '.$ticket_user->surname;?>
    	</td>
    </tr>
    <tr>
    <tr>
    	<td class="label"><?php echo __('Flight');?></td>
    	<td class="field"><?php $flight = Flight::findById($ticket->flight_id);
      		echo Airport::findById($flight->flight_from)->name.' - '.Airport::findById($flight->flight_to)->name;
      	?></td>
    </tr>
    <tr>
    	<?php $type = TicketType::findById($ticket->type);?>
    	<td class="label"><?php echo __('Ticket Type');?></td>
    	<td class="field"><?php echo $type->name;?></td>
    </tr>
    <tr>
    	<td class="label"><?php echo __('Amount');?></td>
    	<td class="field"><?php 
    		$amount = $flight->price * $type->price_multiplier;
      		if($ticket->baggage!=0){
      			$amount +=10;
      		}
      		echo $amount.' PLN';?>
    </tr>
    <tr>
    	<td class="label"><?php echo __('Baggage');?></td>
    	<td class="field"><?php 
    		if($ticket->baggage != 0 || $ticket->baggage!=null){
    			echo __('Yes');
    		}else{
    			echo __('No');
    		}
    	?></td>
    </tr>
    <tr>
    	<td class="label"><?php echo __('Date Ordered');?></td>
    	<td class="field"><?php echo $ticket->date_ordered;?></td>
    </tr>
    <?php if ($ticket->date_confirmed!=null):?>
    <tr>
    	<td class="label"><?php echo __('Date Confirmed');?></td>
    	<td class="field"><?php echo $ticket->date_confirmed;?></td>
    </tr>
    <?php endif;?>
  </table>

<?php Observer::notify('salary_edit_view_after_details', $salary); ?>

  <p class="buttons">
  	<input id="ticket_paid" name="ticket[paid]" type="hidden" value="1">
  	<input id="ticket_date_confirmed" name="ticket[date_confirmed]" type="hidden" value="<?php echo date("Y-m-d");?>">
  	<input id="ticket_date_ordered" name="ticket[date_ordered]" type="hidden" value="<?php echo $ticket->date_ordered;?>">
  	<input id="ticket_baggage" name="ticket[baggage]" type="hidden" value="<?php $ticket->baggage;?>">
    <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Confirm'); ?>" />
    <?php echo __('or'); ?> <a href="<?php echo (AuthUser::hasPermission('salary_view')) ? get_url('plugin/bookkeeping/ticket') : get_url(); ?>"><?php echo __('Cancel'); ?></a>
  </p>

</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
Field.activate('salary_user_id');
// ]]>
</script>

<?php
/**
 * @package Views
 */
?>
<h1><?php echo __(ucfirst($action).' Salary'); ?></h1>

<form action="<?php echo $action=='edit' ? get_url('plugin/bookkeeping/salary_edit/'.$salary->id): get_url('plugin/bookkeeping/salary_add'); ; ?>" method="post">
    <input id="csrf_token" name="csrf_token" type="hidden" value="<?php echo $csrf_token; ?>" />
  <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
    
    <?php $salary_users = User::findAll(); ?>
    
    <tr>
    	<td class="label"><label for="salary_user_id"><?php echo __('First Name').' '.__('And').' '.__('Last Name'); ?></label></td>
    	<td class="field">
          <select class="select" id="salary_user_id" name="salary[user_id]">
			<?php foreach ($salary_users as $salary_user): ?>
				<?php if(UserRole::findByUserId($salary_user->id)!=NULL):?>
            		<option value="<?php echo $salary_user->id; ?>"<?php if ($salary->user_id == $salary_user->id) echo ' selected="selected"'; ?>><?php echo $salary_user->name.' '.$salary_user->surname;?></option>
				<?php endif;?>
			<?php endforeach; ?>
          </select>
        </td>
        <td class="help"><?php echo __('Select employee.'); ?></td>
    </tr>
    
    <tr>
      <td class="label"><label for="salary_user_salary"><?php echo __('Salary'); ?></label></td>
      <td class="field"><input class="textbox" id="salary_user_salary" maxlength="100" name="salary[user_salary]" size="100" type="number" step="0.01" min="0" value="<?php echo $salary->user_salary; ?>" /></td>
      <td class="help"><?php echo __('Brutto'); ?></td>
    </tr>
  </table>

<?php Observer::notify('salary_edit_view_after_details', $salary); ?>

  <p class="buttons">
    <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    <?php echo __('or'); ?> <a href="<?php echo (AuthUser::hasPermission('salary_view')) ? get_url('plugin/bookkeeping/salary') : get_url(); ?>"><?php echo __('Cancel'); ?></a>
  </p>

</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
    
Field.activate('salary_user_id');
// ]]>
</script>

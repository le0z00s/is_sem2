<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Tickets'); ?></h1>
<table id="ticket" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('Name and Surname'); ?></th>
      <th><?php echo __('Flight From').'-'.__('Flight To'); ?></th>
      <th><?php echo __('Amount'); ?></th>
      <th><?php echo __('Modify')?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($tickets as $ticket): ?> 
	
    <tr class="node <?php echo odd_even(); ?>">
      <td class="ticket">
		<?php $ticket_user = User::findById($ticket->user_id);?>
        <a href="<?php echo get_url('plugin/bookkeeping/ticket_edit/'.$ticket->id); ?>"><?php echo $ticket_user->name.' '.$ticket_user->surname;?></a>
      </td>
      <td>
      	<?php $flight = Flight::findById($ticket->flight_id);
      		echo Airport::findById($flight->flight_from)->name.' - '.Airport::findById($flight->flight_to)->name;
      	?>
      </td>
      	
      <td>
      	<?php $type = TicketType::findById($ticket->type);
      		$amount = $flight->price * $type->price_multiplier;
      		if($ticket->type!=0){
      			$amount +=10;
      		}
      		echo $amount;
      	?>
      </td>
      <td>
        <a href="<?php echo get_url('plugin/bookkeeping/ticket_delete/'.$ticket->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$ticket->id.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete ticket'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
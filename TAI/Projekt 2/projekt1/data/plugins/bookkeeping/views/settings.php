<?php
/**
 * @package Plugins
 * @subpackage bookkeeping
 */

if (!defined('IN_CMS')) { exit(); }

?>
<h1><?php echo __('Bookkeeping').' - '.__('Settings'); ?></h1>

<form action="<?php echo get_url('plugin/bookkeeping/save'); ?>" method="post">
    <fieldset style="padding: 0.5em;">
        <table class="fieldset" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td class="label"><label for="settings[bank_account]"><?php echo __('Bank Account'); ?>: </label></td>
                <td class="field">
					<input type="text" class="textinput" value="<?php echo $settings['bank_account']; ?>" name="settings[bank_account]" />
				</td>
        	</tr>
        	<tr>
                <td class="label"><label for="settings[name]"><?php echo __('Bank Details'); ?>: </label></td>
                <td class="field">
					<input type="text" class="textinput" value="<?php echo $settings['name']; ?>" name="settings[name]" />
				</td>
        	</tr>
        	<tr>
                <td class="label"><label for="settings[address]"><?php echo __('Address'); ?>: </label></td>
                <td class="field">
					<input type="text" class="textinput" value="<?php echo $settings['address']; ?>" name="settings[address]" />
				</td>
        	</tr>
        </table>
    </fieldset>
    <br/>
    <p class="buttons">
        <input class="button" name="commit" type="submit" accesskey="s" value="<?php echo __('Save'); ?>" />
    </p>
</form>

<script type="text/javascript">
// <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Prevent accidentally navigating away
        $(':input').bind('change', function() { setConfirmUnload(true); });
        $('form').submit(function() { setConfirmUnload(false); return true; });
    });
// ]]>
</script>

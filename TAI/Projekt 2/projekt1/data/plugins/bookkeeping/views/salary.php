<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Salaries'); ?></h1>


<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/salary_add'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Add'); ?></a></p>

<table id="salaries" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('First Name').' '.__('And').' '.__('Last Name'); ?></th>
      <th><?php echo __('Salary'); ?></th>
      <th><?php echo __('Modify')?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($salaries as $salary): ?> 
	<?php $salary_user = User::findById($salary->user_id);?>
    <tr class="node <?php echo odd_even(); ?>">
      <td class="salary">
        <a href="<?php echo get_url('plugin/bookkeeping/salary_edit/'.$salary->id); ?>"><?php echo $salary_user->name.' '.$salary_user->surname;?></a>
      </td>
      <td>
      	<?php echo $salary->user_salary;?>
      </td>
      <td>
        <a href="<?php echo get_url('plugin/bookkeeping/salary_delete/'.$salary->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$salary_user->name.' '.$salary_user->surname.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete suspect'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Documents'); ?></h1>
<table id="ticket" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('Name and Surname'); ?></th>
      <th><?php echo __('Flight From').'-'.__('Flight To'); ?></th>
      <th><?php echo __('Amount'); ?></th>
      <th><?php echo __('Modify')?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($documents as $documents): ?> 
	
    <tr class="node <?php echo odd_even(); ?>">
      <td class="document">
		<?php $ticket_user = User::findById($document->user_id);?>
        <a href="<?php echo get_url('plugin/bookkeeping/document_edit/'.$document->id); ?>"><?php echo $document_user->name.' '.$document_user->surname;?></a>
      </td>
      <td>
      	<?php $flight = Flight::findById($document->flight_id);
      		echo Airport::findById($flight->flight_from)->name.' - '.Airport::findById($flight->flight_to)->name;
      	?>
      </td>
      	
      <td>
      	<?php $type = DocumentType::findById($document->type);
      		$amount = $flight->price * $type->price_multiplier;
      		if($document->type!=0){
      			$amount +=10;
      		}
      		echo $amount;
      	?>
      </td>
      <td>
        <a href="<?php echo get_url('plugin/bookkeeping/ticket_delete/'.$document->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$document->id.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete document'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
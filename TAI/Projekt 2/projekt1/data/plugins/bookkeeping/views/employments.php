<?php
/**
 * @package Views
 */

?>
<h1><?php echo __('Employments'); ?></h1>


<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/employment_add'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Add'); ?></a></p>

<table id="employments" class="index" cellpadding="0" cellspacing="0" border="0">
  <thead>
    <tr>
      <th><?php echo __('First Name').' '.__('And').' '.__('Last Name'); ?></th>
	  <th><?php echo __('Employment'); ?></th>
	  <th><?php echo __('Seat'); ?></th>
      <th><?php echo __('Salary'); ?></th>
      <th><?php echo __('Modify')?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach($employments as $employment): ?> 
	<?php $salary_user = User::findById($salary->user_id);?>
    <tr class="node <?php echo odd_even(); ?>">
      <td class="salary">
        <a href="<?php echo get_url('plugin/bookkeeping/employment_edit/'.$employment->id); ?>"><?php echo $employment_user->name.' '.$employment_user->surname;?></a>
      </td>
      <td>
      	<?php echo $salary->user_employment;?>
      </td>
      <td>
        <a href="<?php echo get_url('plugin/bookkeeping/employment_delete/'.$employment->id)?>" onclick="return confirm('<?php echo __('Are you sure you wish to delete').' '.$employment_user->name.' '.$employment_user->surname.'?'; ?>');"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/icon-remove.gif" alt="<?php echo __('delete user icon'); ?>" title="<?php echo __('Delete suspect'); ?>" /></a>
      </td>
    </tr>
<?php endforeach; ?> 
  </tbody>
</table>
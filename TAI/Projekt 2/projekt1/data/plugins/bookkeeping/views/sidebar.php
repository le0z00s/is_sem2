<?php
/**
 * @package Plugins
 * @subpackage bookkeeping
 */

if (!defined('IN_CMS')) { exit(); }

?>
<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/salary'); ?>"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/page.png" align="middle" alt="page icon" /> <?php echo __('Salary'); ?></a>
<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/ticket'); ?>"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/page.png" align="middle" alt="page icon" /> <?php echo __('Tickets'); ?></a>
<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/employments'); ?>"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/page.png" align="middle" alt="page icon" /> <?php echo __('Employments'); ?></a>
<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/cv'); ?>"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/page.png" align="middle" alt="page icon" /> <?php echo __('CV'); ?></a>
<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/opinion'); ?>"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/page.png" align="middle" alt="page icon" /> <?php echo __('Opinion'); ?></a>
<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/document'); ?>"><img src="<?php echo PATH_PUBLIC;?>data/admin/images/page.png" align="middle" alt="page icon" /> <?php echo __('Document'); ?></a>
<p class="button"><a href="<?php echo get_url('plugin/bookkeeping/settings'); ?>"><img src="<?php echo ICONS_PATH;?>settings-32-ns.png" align="middle" alt="page icon" /> <?php echo __('Settings'); ?></a></p>

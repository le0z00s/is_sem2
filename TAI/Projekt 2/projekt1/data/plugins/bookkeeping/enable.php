<?php
/**
 * @package Plugins
 * @subpackage bookkeeping
 */

if (!defined('IN_CMS')) { exit(); }

$PDO = Record::getConnection();
$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));

//Setup table structure
if ($driver == 'mysql') {
	//ticket types table
	$PDO->exec("CREATE TABLE IF NOT EXISTS ".TABLE_PREFIX."salary (
			id int(11) unsigned NOT NULL auto_increment,
			user_id int(11) unsigned NOT NULL,
			user_salary float(6,2) default NULL,
			PRIMARY KEY (id)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8");
}

else if ($driver == 'sqlite') {
	//ticket types table
	$PDO->exec("CREATE TABLE IF NOT EXISTS salary (
			id INTEGER NOT NULL PRIMARY KEY,
			user_id INTEGER NOT NULL,
			user_salary REAL)");	
}

$settings = array('bank_account' => 'PL08 0000 1560 8326 4485 0010 1522',
		'name' => 'Lotnisko ltd.',
		'address' => 'Żwirka i Muchomorka 8, 30-010 Kraków Polska'
);

Plugin::setAllSettings($settings, 'bookkeeping');
?>
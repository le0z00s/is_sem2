<?php
/**
 * @package Plugins
 * @subpackage bookkeeping
 */

if (!defined('IN_CMS')) { exit(); }

Plugin::setInfos(array(
    'id'          => 'bookkeeping',
    'title'       => __('Bookkeeping'),
    'description' => __('Provides methods for managing financial data')
));



if(Permission::findByName('bookkeeping_view')==NULL){
	$perm = new Permission();
	$perm->name = 'bookkeeping_view';
	$perm->save();
}
if(Role::findByName('bookkeeper')==NULL){
	$role = new Role();
	$role->name = 'bookkeeper';
	$role->save();
}

$admin_perms = RolePermission::findPermissionsFor(Role::findByName('administrator')->id);
$admin_perms[] = Permission::findByName('bookkeeping_view');
RolePermission::savePermissionsFor(Role::findByName('administrator')->id, $admin_perms);

$perms = array();
$perms[] = Permission::findByName('bookkeeping_view');
$perms[] = Permission::findByName('admin_view');
RolePermission::savePermissionsFor(Role::findByName('bookkeeper')->id, $perms);


Plugin::addController('bookkeeping', __('Bookkeeping'), 'bookkeeping_view', true);

AutoLoader::addFolder(PLUGINS_ROOT.DS.'bookkeeping'.DS.'models');

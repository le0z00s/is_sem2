<?php
/**
 * @package Plugins
 * @subpackage customs_control
 */

if (!defined('IN_CMS')) { exit(); }

class BookkeepingController extends PluginController {

    public function __construct() {
        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('../../plugins/bookkeeping/views/sidebar'));
    }

    public function index() {
        $this->salary();
    }

    public function salary() {
        $this->display('bookkeeping/views/salary',array(
        		'salaries' => Salary::findAll()
        ));
    }
    
    function ticket(){
    	$this->display('bookkeeping/views/ticket', array(
    			'tickets' => Ticket::findAll()
    	));
    }
    
    //salaries add, edit, delete functions
    public function salary_add() {
    	if (get_request_method() == 'POST') {
    		$this->_salary_add();
    	}
    
    	$salary = Flash::get('post_data');
    
    	if (empty($salary)) {
    		$salary = new Salary();
    	}
    
    	$this->display('bookkeeping/views/salary/edit', array(
    			'action'  => 'add',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/bookkeeping/salary_add'),
    			'salary' => $salary
    	));
    }
    
    private function _salary_add() {
    	$data = $_POST['salary'];
    
    	if (!AuthUser::hasPermission('bookkeeping_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/bookkeeping/salary'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/bookkeeping/salary_add')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/bookkeeping/salary'));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/bookkeeping/salary'));
    	}
    
    	$salary = new Salary($data);
    
    	if ( ! $salary->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Salary info has not been added. Something went wrong!'));
    		redirect(get_url('plugin/bookkeeping/salary', 'add'));
    	}
    	else {
    		Flash::set('success', __('Salary has been added!'));
    		Observer::notify('salary_after_add', $salary);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/bookkeeping/salary'));
    	}
    	else {
    		redirect(get_url('plugin/bookkeeping/salary_edit/'.$salary->id));
    	}
    }
    
    public function salary_edit($id) {
    	$salary = Flash::get('post_data');
    
    	if (empty($salary)) {
    		$salary = Salary::findById($id);
    		if ( !$salary ) {
    			Flash::set('error', __('Salary not found!'));
    			redirect(get_url('plugin/bookkeeping/salary'));
    		}
    	}
    
    	if (get_request_method() == 'POST') {
    		$this->_salary_edit($id);
    	}
    
    	$this->display('bookkeeping/views/salary/edit', array(
    			'action'  => 'edit',
    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/bookkeeping/salary_edit'),
    			'salary' => $salary
    	));
    }
    
    private function _salary_edit($id) {
    	$data = $_POST['salary'];
    	$data['id'] = $id;
    
    	if (!AuthUser::hasPermission('bookkeeping_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/bookkeeping/views/salary'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/bookkeeping/salary_edit')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/bookkeeping/salary_edit/'.$id));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/bookkeeping/salary_edit/'.$id));
    	}
    
    	$salary = new Salary($data);
    
    	if ( ! $salary->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Salary has not been edited. Something went wrong!'));
    		redirect(get_url('plugin/bookkeeping/salary_edit/'.$id));
    	}
    	else {
    		Flash::set('success', __('Salary has been saved!'));
    		Observer::notify('salary_after_edit', $salary);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/bookkeeping/salary'));
    	}
    	else {
    		redirect(get_url('plugin/bookkeeping/salary_edit/'.$id));
    	}
    }
    
    function salary_delete($id) {
    	if (!AuthUser::hasPermission('bookkeeping_view')) {
    		Flash::set('error', __('You do not have permission to delete this!'));
    		redirect(get_url('plugin/bookkeeping'));
    	}
    
    	if ($salary = Salary::findById($id)) {
    		if ($salary->delete()) {
    			Flash::set('success', __('Salary has been deleted!'));
    			Observer::notify('salary_after_delete', $salary);
    		}
    		else
    			Flash::set('error', __('Salary has not been deleted!'));
    	}
    	else
    		Flash::set('error', __('Salary not found!'));
    
    	redirect(get_url('plugin/bookkeeping/salary'));
    }
    
	function settings() {
		
		$this->display('bookkeeping/views/settings', array(
			'settings' => Plugin::getAllSettings('bookkeeping')
		));
    }
    function save() {
    	$post_data = $_POST['settings'];
    	
    	$ret = Plugin::setAllSettings($post_data, 'bookkeeping');
    
    	if ($ret)
    		Flash::set('success', __('The settings have been updated.'));
    	else
    		Flash::set('error', 'An error has occured.');
    
    	redirect(get_url('plugin/bookkeeping/settings'));
    }
    
    public function ticket_edit($id) {
    	$ticket = Flash::get('post_data');
    
    	if (empty($ticket)) {
    		$ticket = Ticket::findById($id);
    		if ( !$ticket ) {
    			Flash::set('error', __('Ticket not found!'));
    			redirect(get_url('plugin/bookkeeping/ticket'));
    		}
    	}
    
    	if (get_request_method() == 'POST') {
    		$this->_ticket_edit($id);
    	}
    
    	$this->display('bookkeeping/views/ticket/edit', array(

    			'csrf_token' => SecureToken::generateToken(BASE_URL.'plugin/bookkeeping/ticket_edit'),
    			'ticket' => $ticket
    	));
    }
    
    private function _ticket_edit($id) {
    	$data = $_POST['ticket'];
    	$data['id'] = $id;
    
    	if (!AuthUser::hasPermission('bookkeeping_view')) {
    		Flash::set('error', __('You do not have permission to do this!'));
    		redirect(get_url('plugin/bookkeeping/views/ticket'));
    	}
    
    	if (isset($_POST['csrf_token'])) {
    		$csrf_token = $_POST['csrf_token'];
    		if (!SecureToken::validateToken($csrf_token, BASE_URL.'plugin/bookkeeping/ticket_edit')) {
    			Flash::set('post_data', (object) $data);
    			Flash::set('error', __('Invalid CSRF token found!'));
    			Observer::notify('csrf_token_invalid', AuthUser::getUserName());
    			redirect(get_url('plugin/bookkeeping/ticket_edit/'.$id));
    		}
    	}
    	else {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('No CSRF token found!'));
    		Observer::notify('csrf_token_not_found', AuthUser::getUserName());
    		redirect(get_url('plugin/bookkeeping/ticket_edit/'.$id));
    	}
    
    	$ticket = new Ticket($data);
    
    	if ( ! $ticket->save()) {
    		Flash::set('post_data', (object) $data);
    		Flash::set('error', __('Ticket has not been edited. Something went wrong!'));
    		redirect(get_url('plugin/bookkeeping/ticket_edit/'.$id));
    	}
    	else {
    		Flash::set('success', __('Ticket has been saved!'));
    		Observer::notify('ticket_after_edit', $salary);
    	}
    
    	if (isset($_POST['commit'])) {
    		redirect(get_url('plugin/bookkeeping/ticket'));
    	}
    	else {
    		redirect(get_url('plugin/bookkeeping/ticket_edit/'.$id));
    	}
    }
    
    function ticket_delete($id) {
    	if (!AuthUser::hasPermission('bookkeeping_view')) {
    		Flash::set('error', __('You do not have permission to delete this!'));
    		redirect(get_url('plugin/bookkeeping'));
    	}
    
    	if ($ticket = Ticket::findById($id)) {
    		if ($ticket->delete()) {
    			Flash::set('success', __('Ticket has been deleted!'));
    			Observer::notify('ticket_after_delete', $salary);
    		}
    		else
    			Flash::set('error', __('Ticket has not been deleted!'));
    	}
    	else
    		Flash::set('error', __('Ticket not found!'));
    
    	redirect(get_url('plugin/bookkeeping/ticket'));
    }
}
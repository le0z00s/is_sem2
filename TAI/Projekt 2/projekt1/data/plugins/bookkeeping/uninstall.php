<?php
/**
 * @package Plugins
 * @subpackage bookkeeping
 */

if (!defined('IN_CMS')) { exit(); }

if (Plugin::deleteAllSettings('bookkeeping') === false) {
    Flash::set('error', __('Unable to delete plugin settings.'));
    redirect(get_url('setting'));
}

$PDO = Record::getConnection();

if ($PDO->exec('DROP TABLE IF EXISTS '.TABLE_PREFIX.'salary') === false) {
	Flash::set('error', __('Unable to drop table :tablename', array(':tablename' => TABLE_PREFIX.'salary')));
	redirect(get_url('setting'));
}

$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));
$ret = true;

if ($driver == 'sqlite') {
	$ret = $PDO->exec('VACUUM');
}

if ($ret === false) {
	Flash::set('error', __('Unable to clean up table alterations.'));
	redirect(get_url('setting'));
}
else {
	Flash::set('success', __('Successfully uninstalled plugin.'));
	redirect(get_url('setting'));
}
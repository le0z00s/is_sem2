<?php

/**
 * @package Translations
 */

return array(
		'Add Salary' => 'Dodaj wynagrodzenie',
		'Address' => 'Adres',
		'Bank Account' => 'Konto bankowe',
		'Bank Details'=> 'Dane do przelewu',
		'Bookkeeper' => 'Księgowy',
		'Bookkeeping' => 'Księgowość',
		'Brutto' => 'Brutto',
		'Edit Salary' => 'Edytuj wynagrodzenie',
		'Provides methods for managing financial data' => 'Zapewnia metody do zarządzania danymi finansowymi',
		'Salaries' => 'Wynagrodzenia',
		'Salary' => 'Wynagrodzenie',
		'Salary info has not been added. Something went wrong!' => 'Wynagrodzenie nie zostało dodane. Wystąpił błąd w czasie dodawania',
		'Salary has not been edited. Something went wrong!' => 'Wynagrodzenie nie zostało zapisane. Wystąpił błąd w czasie dodawania',
		'Salary has been added!' => 'Dodano wynagrodzenie.',
		'Salary has been deleted!' => 'Wynagrodzenie zostało usunięte.',
		'Salary has been saved!' => 'Zapisano wynagrodzenie',
		'Salary has not been deleted!' => 'Wynagrodzenie nie zostało usunięte!',
		'Salary not found!' => 'Wynagrodzenie nie zostało znalezione!',
		'Select employee.' => 'Wybierz pracownika',
		'Transfer Title' => 'Tytuł przelewu',
		'Amount' => 'Kwota',
		'Document' => 'Dokumenty',
		'Add Document' => 'Dodaj dokument',
		'Employments' => 'Pracownicy',
		'Opinion' => 'Orzeczenia',
		'CV' => 'Cv'
);
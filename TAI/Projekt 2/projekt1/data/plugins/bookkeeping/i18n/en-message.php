<?php

/**
 * @package Translations
 */

return array(
		'Add Salary' => 'Add salary',
		'Address' => 'Address',
		'Bank Account' => 'Bank account',
		'Bank Details' => 'Bank transfer details',
		'Bookkeeper' => 'Bookkeeper',
		'Bookkeeping' => 'Bookkeeping',
		'Brutto' => 'Brutto',
		'Edit Salary' => 'Edit salary',
		'Provides methods for managing financial data' => 'Provides methods for managing financial data',
		'Salaries' => 'Salaries',
		'Salary' => 'Salary',
		'Salary info has not been added. Something went wrong!' => 'Salary info has not been added. Something went wrong!',
		'Salary has not been edited. Something went wrong!' => 'Salary has not been edited. Something went wrong!',
		'Salary has been added!' => 'Salary has been added!',
		'Salary has been deleted!' => 'Salary has been deleted!',
		'Salary has been saved!' => 'Salary has been saved!',
		'Salary has not been deleted!' => 'Salary has not been deleted!',
		'Salary not found!' => 'Salary not found!',
		'Select employee.' => 'Select employee.',
		'Transfer Title' => 'Transfer title',
		'Amount' => 'Amount',
		'Document' => 'Document',
		'Employments' => 'Employments',
		'Opinion' => 'Opinion',
		'CV' => 'CV'
);
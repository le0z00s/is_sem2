<?php
/**
 * @package Plugins
 * @subpackage bookkeeping
 */

class Salary extends Record{
	const TABLE_NAME = 'salary';
		
	public $user_id = '';
	public $user_salary = '';
	
	public static function findBy($column, $value) {
		return self::findOne(array(
				'where' => $column . ' = :value',
				'values' => array(':value' => $value)
		));
	}
	
	public function getColumns() {
		return array(
				'id', 'user_id', 'user_salary'
		);
	}
	
	public static function findAll($args = null) {
		return self::find($args);
	}
}
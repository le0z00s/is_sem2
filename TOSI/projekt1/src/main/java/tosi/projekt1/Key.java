package tosi.projekt1;

import java.math.BigInteger;
import java.util.AbstractMap;
import java.util.Map;

/**
 * Klasa reprezentująca klucz RSA (publiczny lub prywatny)
 * @author Rafał Zbojak
 * @version 0.1
 */
public class Key {
	private Map.Entry<BigInteger, BigInteger> key;
	
	public Key(Map.Entry<BigInteger, BigInteger> key) {
		this.key = key;
	}
	
	public Key(BigInteger n, BigInteger ab) {
		this.key = new AbstractMap.SimpleEntry<BigInteger, BigInteger>(n,ab);
	}
	
	public BigInteger getN() {
		return this.key.getKey();
	}
	
	public BigInteger getAb() {
		return this.key.getValue();
	}
}

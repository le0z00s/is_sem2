package tosi.projekt1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.List;

/**
 * Klasa główna programu
 * @author Rafał Zbojak
 * @version 0.1
 */
public class Main {

	public static void main(String[] args) {		
		BufferedReader buffer = new BufferedReader(
				new InputStreamReader(System.in));
		
		Keypair keypair = new Keypair();
		
		RSA rsa = new RSA(new Keypair(keypair.getEncryptionKey(), keypair.getDecryptionKey()));
		
		try {
		
			System.out.print("Enter message to encrypt: ");
			String message = buffer.readLine();
		
			System.out.println("Encrypting, please wait...");
			List<BigInteger> encryptedMessage = rsa.encrypt(message);
			System.out.println("Decrypting message...");
			
			System.out.format("Decrypted message: %s", rsa
					.decrypt(encryptedMessage));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

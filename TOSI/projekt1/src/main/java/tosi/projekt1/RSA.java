package tosi.projekt1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Klasa szyrująca i deszyrująca ciąg znaków algorytmem RSA
 * @author Rafał Zbojak
 * @version 0.1
 */
public class RSA {
	private static final char paddingSign = '~';
	private static final int blockLenght = 10;
	private Keypair keypair;
	
	public RSA(Keypair keypair) {
		this.keypair = keypair;
	}
	
	public static char getPaddingSign() {
		return paddingSign;
	}
	
	public List<BigInteger> encrypt(String message) throws Exception {
		List<BigInteger> output = new ArrayList<BigInteger>();
		
		/* Wyrównanie wiadomości do długości bloku */
		while(message.length() % blockLenght != 0) {
				message += paddingSign;
		}
		
		String messagePart;
		while (message.length() > 0) {
			messagePart = message.substring(0, blockLenght);
			message = message.substring(blockLenght, message.length());
			
			output.add(this.encryptBlock(messagePart));
		}
		
		return output;
	}
	
	private BigInteger encryptBlock(String block) throws Exception {
		BigInteger output = BigInteger.ZERO;
		
		int counter = 0;
		
		for(char c : block.toCharArray()) {
			output = output.add(BigInteger.valueOf(Util.charToInt(c))
					.multiply(Util.getAlphabetLenghtBigInt().pow(counter)));
			counter++;
		}
		
		return output.modPow(this.keypair.getEncryptionKey().getAb(), 
				this.keypair.getEncryptionKey().getN());
	}
	
	public String decrypt(List<BigInteger> message) throws NumberFormatException, Exception {
		String output = "";
		
		for(BigInteger block : message) {
			output += this.decryptBlock(block);
		}
		
		return output.replaceFirst(String.format("%c+$", paddingSign), "");
	}
	
	private String decryptBlock(BigInteger block) throws NumberFormatException, Exception {
		block = block.modPow(this.keypair.getDecryptionKey().getAb(), 
				this.keypair.getDecryptionKey().getN());
		
		StringBuilder output = new StringBuilder();
		
		BigInteger mod;
		for(int i = 0; i < blockLenght; ++i) {
			mod = block.mod(Util.getAlphabetLenghtBigInt());
			block = block.divide(Util.getAlphabetLenghtBigInt());
			
			output.append(Util.intToChar(Integer.parseInt(mod.toString())));
		}
		
		return output.toString();
	}
}

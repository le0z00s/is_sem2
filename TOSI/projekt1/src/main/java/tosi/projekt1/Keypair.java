package tosi.projekt1;

import java.math.BigInteger;
import java.util.Random;

/**
 * Klasa reprezentująca parę kluczy RSA (publiczny i prywatny)
 * @author Rafał Zbojak
 * @version 0.1
 */
public class Keypair {
	private Key encryptionKey;
	private Key decryptionKey;
	
	/**
	 * Bezargumentowy konstruktor
	 * Oblicza klucz prywatny oraz publiczny
	 */
	public Keypair() {
		BigInteger p = BigInteger.probablePrime(232, new Random());
		BigInteger q = BigInteger.probablePrime(232, new Random());
		BigInteger n = p.multiply(q);
		BigInteger m = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
		
		BigInteger a;
		do {
			a = BigInteger.probablePrime(new Random().nextInt(232), new Random());
		} while (!a.gcd(m).equals(BigInteger.ONE));
		
		BigInteger b = a.modInverse(m);
		
		this.encryptionKey = new Key(n, a);
		this.decryptionKey = new Key(n, b);
	}
	
	public Keypair(Key encryptionKey, Key decryptionKey) {
		this.encryptionKey = encryptionKey;
		this.decryptionKey = decryptionKey;
	}
	
	public Key getEncryptionKey() {
		return this.encryptionKey;
	}
	
	public Key getDecryptionKey() {
		return this.decryptionKey;
	}
}

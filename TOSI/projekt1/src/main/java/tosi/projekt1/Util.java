package tosi.projekt1;

import java.math.BigInteger;

/**
 * Klasa pomocnicza do obliczenia wartości dla znaków
 * @author Rafał Zbojak
 */
public class Util {
	private static final int alphabetLenght = 92;
	private static final int space = 32;
	
	public static int getAlphabetLenght() {
		return alphabetLenght;
	}
	
	public static BigInteger getAlphabetLenghtBigInt() {
		return BigInteger.valueOf(alphabetLenght);
	}
	
	public static int charToInt(char c) throws Exception {
		int value = (int) c;
		
		if(value == space) {
			return 0;
		}else if (value == (int) RSA.getPaddingSign()) {
			return 1;
		}else if (value > space && value <= (space + alphabetLenght + 3)) {
			value -= space - 1;
			return value;
		}else {
			throw new Exception(String.format("%c is an invalid character\n", c));
		}
	}
	
	public static char intToChar(int i) throws Exception {
		if(i == 0) {
			return ' ';
		}else if (i == 1) {
			return RSA.getPaddingSign();
		}else if (i > 0 && i < alphabetLenght) {
			return (char) (i + space - 1);
		}else {
			throw new Exception(String.format("%d is not a valid code for ASCII sign", i));
		}
	}
}

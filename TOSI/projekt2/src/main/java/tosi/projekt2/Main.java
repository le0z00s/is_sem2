package tosi.projekt2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;

/**
 * Główna klasa programu
 * @author Rafał Zbojak
 * @version 0.2
 */
public class Main {

	public static void main(String[] args) throws Exception {
//		BufferedReader buffer = new BufferedReader(
//				new InputStreamReader(System.in));

                File file = new File(args[0]);
                if(!file.canRead()){
                    throw new Exception("Cannot read file");
                }
                BufferedReader buffer = new BufferedReader(new FileReader(file));
                
		try {
//			System.out.print("Enter message to encrypt: ");
//			String message = buffer.readLine();
			
                        String message = "";
                        
                        while(buffer.ready()){
                            message += String.format("%s\n", buffer.readLine());
                        }
                        

			System.out.format("Message before encryption: %s\n", 
					Util.byteArrayToString(message.getBytes()));
			
			System.out.println("Creating key...");
			Key key = new BlumBlumShub().generateKey(message.getBytes().length);
			
			System.out.format("Generated key: %s\n", key.toString());
			
			System.out.println("Encrypting...");
			Vernam vernam = new Vernam(key);
			byte[] cryptogram = vernam.encrypt(message);
			
			System.out.format("Cryptogram: %s\n", 
					Util.byteArrayToString(cryptogram));
			
			System.out.println("Decrypting...");
			System.out.format("Message after decryption: %s\n", 
					vernam.decrypt(cryptogram));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package tosi.projekt2;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Klasa implementująca generator pseudolosowy dla klucza
 * algorytmem Blum Blum Shub
 * @author Rafał Zbojak
 * @version 0.2
 */
public class BlumBlumShub {
	
	private BigInteger x;
	private BigInteger n;
	
	public BlumBlumShub() {
		BigInteger p = BigInteger.probablePrime(768, new SecureRandom());
		BigInteger q = BigInteger.probablePrime(768, new SecureRandom());
		this.n = p.multiply(q);
		
		BigInteger result;
		do {
			result = new BigInteger(n.bitLength(), new Random());
		} while (!(result.compareTo(BigInteger.ZERO) >= 0 
				&& result.compareTo(n) <= 0));
		
		this.x = result;
	}
	
	private byte getByte() {
		byte b = 0;
		byte nextBit;
		
		for (int i = 0; i < Byte.SIZE; i++) {
			this.x = this.x.pow(2).mod(this.n);
			nextBit = (byte) (this.x.mod(BigInteger.valueOf(2))
					.equals(BigInteger.ZERO) ? 0 : 1);
			
			b = (byte) ((b << 1) | nextBit);
		}
		
		return b;
	}
	
	public Key generateKey(int keyLength) {
		byte[] key = new byte[keyLength];
		
		for(int i = 0; i < keyLength; i++) {
			key[i] = getByte();
		}
		
		return new Key(key);
	}
}

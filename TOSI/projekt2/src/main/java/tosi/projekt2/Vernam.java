package tosi.projekt2;

/**
 * Główna implementująca szyfr Vermana
 * @author Rafał Zbojak
 * @version 0.1
 */
public class Vernam {
	
	private Key key;
	
	public Vernam(Key key) {
		this.key = key;
	}
	
	public byte[] encrypt(String message) throws Exception {
		byte[] messageBytes = message.getBytes();
		byte[] keyBytes = this.key.getKey();
		
		if (messageBytes.length > keyBytes.length) {
			throw new Exception("Message length is greater than key length. "
					+ "That shouldn't happen. You fucked that up, Sonny.");
		}
		
		for(int i = 0; i < messageBytes.length; i++) {
			messageBytes[i] ^= keyBytes[i];
		}
		
		return messageBytes;
	}
	
	public String decrypt(byte[] cryptogram) throws Exception {
		byte[] keyBytes = this.key.getKey();
		
		if (cryptogram.length > keyBytes.length) {
			throw new Exception("Cryptogram length is greater than key length. "
					+ "That shouldn't happen. You fucked that up, Sonny.");
		}
		
		for(int i = 0; i < cryptogram.length; i++) {
			cryptogram[i] ^= keyBytes[i];
		}
		
		return new String(cryptogram);
	}
}

package tosi.projekt2;

/**
 * Klasa reprezentująca klucz
 * @author Rafał Zbojak
 * @version 0.1
 */
public class Key {
	private byte[] key;
	
	public Key(byte[] key) {
		this.key = key;
	}
	
	public byte[] getKey() {
		return this.key;
	}
	
	@Override
    public String toString() {
        return Util.byteArrayToString(this.key);
    }
}

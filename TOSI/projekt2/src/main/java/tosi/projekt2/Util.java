package tosi.projekt2;

/**
 * Klasa pomocnicza
 * @author Rafał Zbojak
 * @version 0.1
 */
public class Util {
	public static String byteArrayToString(byte[] array) {
		String output = "";
        for(byte b : array) {
        	output += String.format("0x%02X ", b);
        }
        return output;
	}
}

#include <iostream>
#include <stdio.h>
#include <string.h>
using namespace std;


/* Definicja stalych urzadzenia */
__constant__ unsigned int k[64], s[16];			// magiczne liczby MD5
__constant__ unsigned int searchHash[4];		// poszukiwany hash
__constant__ char alphabet[63];					// alfabet z dostepnymi znakami
__device__ bool isFound = false;					// flaga informujaca o tym czy znaleziono juz poszukiwany hash

/* Definicja stalych na procesorze */
// Alfabet z dostepnymi znakami
static const char alphabetCpu[63] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

//MAGICZNE LICZBY MD5
// Wartosci wyliczone ze wzorzu: kCpu[i] = floor(abs(sin(i + 1)) × (2 pow 32))
static const unsigned int kCpu[64] = {
	0xd76aa478,	0xe8c7b756,	0x242070db,	0xc1bdceee,
	0xf57c0faf,	0x4787c62a,	0xa8304613,	0xfd469501,
	0x698098d8,	0x8b44f7af,	0xffff5bb1,	0x895cd7be,
	0x6b901122,	0xfd987193,	0xa679438e,	0x49b40821,

	0xf61e2562,	0xc040b340,	0x265e5a51,	0xe9b6c7aa,
	0xd62f105d,	0x2441453,	0xd8a1e681,	0xe7d3fbc8,
	0x21e1cde6,	0xc33707d6,	0xf4d50d87,	0x455a14ed,
	0xa9e3e905,	0xfcefa3f8,	0x676f02d9, 0x8d2a4c8a,

	0xfffa3942,	0x8771f681, 0x6d9d6122, 0xfde5380c,
	0xa4beea44,	0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
	0x289b7ec6,	0xeaa127fa,	0xd4ef3085,	0x4881d05,
	0xd9d4d039,	0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,

	0xf4292244,	0x432aff97, 0xab9423a7, 0xfc93a039,
	0x655b59c3,	0x8f0ccc92, 0xffeff47d, 0x85845dd1,
	0x6fa87e4f,	0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
	0xf7537e82,	0xbd3af235, 0x2ad7d2bb, 0xeb86d391
};

// Wartosci definiuja o ile nastapi przesuniecie w danym kroku
static const unsigned int sCpu[16] = {
	7, 12, 17, 22, 			//7, 12, 17, 22, 	7, 12, 17, 22, 		7, 12, 17, 22,
	5, 9, 14, 20,			//5, 9, 14, 20, 	5, 9, 14, 20, 		5, 9, 14, 20,
	4, 11, 16, 23,			//4, 11, 16, 23, 	4, 11, 16, 23, 		4, 11, 16, 23,
	6, 10, 15, 21			//6, 10, 15, 21, 	6, 10, 15, 21, 		6, 10, 15, 21
};

/*
	Metoda odpowiedzialna za inicjalizacje stalych urzadzenia
	Argumenty:
		- searchHashCpu - wskaznik na poszukiwany hash
*/
void initConstants(unsigned int searchHashCpu[4]) {
	cudaMemcpyToSymbol(s, sCpu, sizeof(s));
	cudaMemcpyToSymbol(k, kCpu, sizeof(k));
	cudaMemcpyToSymbol(alphabet, alphabetCpu, sizeof(alphabet));
	cudaMemcpyToSymbol(searchHash, searchHashCpu, 4*sizeof(unsigned int));
}

/*
	Metoda urzadzenia odpowiedzialna za dokonanie rotacji w lewo o "n" wartosci.
	Argumenty:
		- x - element do dokonania rotacji,
		- n - 
*/
__device__ inline unsigned int leftRotate (unsigned int x, unsigned int n)
{
	return (x << n) | (x >> (32-n));
}

/*
	Metoda urzadzenia zwracajca wartosc przesuniecia dla danego indeksu
	Argumenty:
		- i - indeks elementu w bloku.
*/
__device__ inline unsigned int getS(const unsigned int i) {
	return s[(i / 16) * 4 + i % 4]; 
}

/*
	Metoda urzadzenia odpowiedzialna za wyliczenie wartosci stanow a, b, c, d w danym kroku.
	Argumenty:
		- i - indeks elementu w bloku,
		- f - referencja do wartosci funkcji stanu w danym kroku,
		- g - referencja do indeksu bloku w danym kroku,
		- a - referencja do aktualnej wartosci stanu a,
		- b - referencja do aktualnej wartosci stanu b,
		- c - referencja do aktualnej wartosci stanu c,
		- d - referencja do aktualnej wartosci stanu d,
		- w - wskaznik do aktualnie przetwarzanego bloku danych.
*/
__device__ inline void step(unsigned int i, unsigned int &f, unsigned int &g, unsigned int &a, unsigned int &b, unsigned int &c, unsigned int &d, unsigned int w[16])
{
	unsigned int temp = d;
	d = c;
	c = b;
	b = b + leftRotate((a + f + k[i] + w[g]), getS(i));
	a = temp;
}

/*
	Metoda urzadzenia odpowiedzialna za dekodowanie bloku przechowywanego w postaci tabeli elementow jednobajtowych na tabele elementow czterobajtowych. 
	Argumenty:
		- output - wskaznik na tabele wyjsciowa,
		- input - wskaznik na tabele wejsciowa,
		- len - liczba elementow w tabeli wejsciowej, ktore chcemy zdekodowac (musi byc wielokrotnoscia wartosci 4).
*/
__device__ void decode(unsigned int output[], const unsigned char input[], unsigned int len)
{
  for (unsigned int i = 0, j = 0; j < len; i++, j += 4)
    output[i] = ((unsigned int)input[j]) | (((unsigned int)input[j+1]) << 8) |
      (((unsigned int)input[j+2]) << 16) | (((unsigned int)input[j+3]) << 24);
}

// Domyslne funkcje zmiany stanu w MD5
/*
	Funkcja urzadzenia sluzaca do zmiany stanu w pierwszym cyklu (kroki 1-16). (jeśli x to y, w przeciwnym wypadku z)
	Argumenty:
		- x - referencja do wartosci pierwszego stanu w danym kroku,
		- y - referencja do wartosci drugiego stanu w danym kroku,
		- z - referencja do wartosci trzeciego stanu w danym kroku.
*/
__device__ inline unsigned int F(const unsigned int &x, const unsigned int &y, const unsigned int &z) 	{		return (x & y) | ((~x) & z); 	}
/*
	Funkcja urzadzenia sluzaca do zmiany stanu w drugim cyklu (kroki 17-32). (jeśli z to x, w przeciwnym wypadku y)
	Argumenty takie jak powyzej.
*/
__device__ inline unsigned int G(const unsigned int &x, const unsigned int &y, const unsigned int &z) {		return (z & x) | ((~z) & y); 	}
/*
	Funkcja urzadzenia sluzaca do zmiany stanu w trzecim cyklu (kroki 33-48). (suma argumentów modulo 2, lub innymi słowy: czy występuje nieparzysta liczba jedynek w argumentach)
	Argumenty takie jak powyzej.
*/
__device__ inline unsigned int H(const unsigned int &x, const unsigned int &y, const unsigned int &z) {		return x ^ y ^ z; 				}
/*
	Funkcja urzadzenia sluzaca do zmiany stanu w czwartym cyklu (kroki 49-64). (jeżeli (z=1 i x=0) wtedy y, w przeciwnym wypadku nie y)
	Argumenty takie jak powyzej.
*/
__device__ inline unsigned int I(const unsigned int &x, const unsigned int &y, const unsigned int &z) 	{		return y ^ (x | (~z)); 			}

/*
	Funkcja urzadzenia zwracajaca dlugosc lancucha podanego jako argument:
	Argumenty:
		- text - wskaznik na lancuch.
*/
__device__ int strlenOwn(const char *text) {
	int length = 0;

	while(text[length++] != '\0');

	return (length - 1);
}

/*
	Metoda odpowiedzialna za przygotowanie lancucha wejsciowego do obliczenia skrotu MD5. Na podstawie lancucha tworzony jest
	blok skladajacy sie z 64 elementow jednobajtowych. Na poczatku bloku kopiowana jest tresc wiadomosci, za wiadomoscia dodawana jest
	wartosc "1" (w little-endian wartosci 0x80). Nastepnie blok uzupelniany jest zerami. Na ostatnich 8 bajtach przechowujemy dlugosc
	wiadomosci oryginalnej w bitach (wartosc w little-endian).
	Argumenty:
		- text - wskaznik do wiadomosci, dla ktorej bedziemy liczyc skrot MD5
*/
__device__ unsigned char* prepText(const char *text) {
	int len = strlenOwn(text);

	unsigned char* msg = (unsigned char*) malloc (64);
	memcpy( msg, text, len);
    msg[len] = (unsigned char) 0x80; 

	memset((unsigned char*) &msg[len+1], 0, 64 - (len+1));
	((unsigned int*)msg)[14] = len << 3;

	return msg;
}

/*
	Metoda urzadzenia zwracajaca a-ty (argument funkcji) element permutacji elementow alfabetu.
	Argumenty:
		- a - indeks elementu permutacji.
*/
__device__ char* generateWord(unsigned long long a) {
	unsigned long long k = 1;
	unsigned short N = 62; 
	unsigned long long n = N;

	while (a >= n) {
		a = a - n;
		n = n*N;
		k = k + 1;
	}

	char* s = (char*) malloc(k+1 * sizeof(char));
	for (unsigned long long q = 0; q < k; q++) {
		s[q] = alphabet[(a%N)];
		a /= N;
	}

	s[k] = '\0';

	return s;
}

/*
	Metoda urzadzenia odpowiedzialna za wyliczenie skrotu wiadomosci MD5 dla pojedynczego bloku wiadomosci.
	Argumenty:
		- block - pojedynczy blok wiadomosci skladajacy sie z 64 elementow.
*/
__device__ void calculateMD5(unsigned char block[64])
{
	//wartosci poczatkowe dla stanow
	const unsigned int a0 = 0x67452301;
	const unsigned int b0 = 0xEFCDAB89;
	const unsigned int c0 = 0x98BADCFE;
	const unsigned int d0 = 0x10325476;

	//ustalamy wawrtosci poczatkowe dla stanow
	unsigned int a = a0;
	unsigned int b = b0;
	unsigned int c = c0;
	unsigned int d = d0;

	//dekodujemy blok elementow jednobajtowych na blok elementow czterobajtowych
	unsigned int w[16];
	decode(w, block, 64);	

	//glowna czesc wyliczania skrotu, iterujemy po wszystkich elementach bloku
	unsigned int f, g, i = 0;
	//I cykl
	for(; i != 16; i++)
	{
		f = F(b, c, d);
		g = i;
		step(i, f, g, a, b, c, d, w);
	}

	//II cykl
	for(; i != 32; i++)
	{
		f = G(b, c, d);
		g = (5*i + 1) % 16;
		step(i, f, g, a, b, c, d, w);
	}

	//III cykl
	for(; i != 48; i++)
	{
		f = H(b, c, d);
		g = (3*i + 5) % 16;
		step(i, f, g, a, b, c, d, w);
	}

	//IV cykl
	for(; i != 64; i++)
	{
		f = I(b, c, d);
		g = (7*i) % 16;
		step(i, f, g, a, b, c, d, w);
	}

	//na koniec dodajemy wartosci poczatkowe dla wyliczonych wartosci stanow
	a += a0;
	b += b0;
	c += c0;
	d += d0;
	
	//sprawdzamy czy obliczony hash jest zgodny z poszukiwanym jesli tak ustawiamy flage
	if (a == searchHash[0] && b == searchHash[1] && c == searchHash[2] && d == searchHash[3]) {
		isFound = true;
	}
}

/*
	Jadro urzadzenia odpowiedzialne za wyszukanie przez watki GPU slowa generujacego zadany hash
*/
__global__ void kernel() {
	unsigned long long tid = blockIdx.x * blockDim.x + threadIdx.x;
	
	char* word;
	unsigned char* blockWord;
	while (isFound == false && tid <= 221919451578089 ) {
		word = generateWord(tid);
		blockWord = prepText(word);
		calculateMD5(blockWord);
		
		//zwalniamy zajeta pamiec na wygenerowane slowo i blok wiadomosci
		free(word);
		free(blockWord);
		
		tid += blockDim.x * gridDim.x;
	}
}

/*
	Metoda odpowiedzialna za przygotowanie wejsciowego skrotu MD5. Podzial lancucha na czteroelementowana tablice wraz z zamiana
	na kodowanie little-endian.
	Argumenty:
		- target - wejsciowych skrot MD5.
*/
unsigned int* prepareSearchHash(char* target) {
	string str(target);

	unsigned int* hash = (unsigned int*) malloc (4 * sizeof(unsigned int));
	hash[0] = strtoul(str.substr(0, 8).c_str(), NULL, 16);
	hash[1] = strtoul(str.substr(8, 8).c_str(), NULL, 16);
	hash[2] = strtoul(str.substr(16, 8).c_str(), NULL, 16);
	hash[3] = strtoul(str.substr(24, 8).c_str(), NULL, 16);

	for(int i =0; i< 4; i++) {
		hash[i] = ((hash[i]>>24) & 0xff) | // move byte 3 to byte 0
                    ((hash[i]<<8) & 0xff0000) | // move byte 1 to byte 2
                    ((hash[i]>>8) & 0xff00) | // move byte 2 to byte 1
                    ((hash[i]<<24) & 0xff000000); // byte 0 to byte 3
	}

	return hash;
}

/* 
	Glowna metoda programu
*/
int main(int argc, char *argv[]) {
	//walidacja danych wejściowych
	if (argc != 2) {
		printf("Użycie: %s <skrot_md5>\n", argv[0]);
		exit(-1);
	}
	
	if (strlen(argv[1]) != 32) {
		printf("Niepoprawny skrot MD5. Powinien sklada sie z 32 znakow.\n");
		exit(-1);
	}

	unsigned int* hash = prepareSearchHash(argv[1]);
	initConstants(hash);
	
	//inicjujemy i uruchamiamy licznik
	cudaEvent_t start,stop;
	float elapsedTime;
	
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,0);
	
	//uruchamiamy jadro
	kernel<<<10, 640>>>();

	//zatrzymujemy licznik i zczytujemy czas obliczen
	cudaEventRecord(stop,0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsedTime,start,stop);
	
	//wypisujemy otrzymany czas
	printf("%.2f\n", elapsedTime);
	
	cudaDeviceSynchronize();
	
	free(hash);
}
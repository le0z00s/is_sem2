#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;
using namespace MPI;

double **dev_2d;

void copyRowsCols(const Mat& input, Mat& output) {
	//kopiujemy 4 wiersze (2 gora, 2 dol)
	input.row(0).copyTo(output.row(0));
	input.row(1).copyTo(output.row(1));
	input.row(input.rows - 1).copyTo(output.row(output.rows - 1));
	input.row(input.rows - 2).copyTo(output.row(output.rows - 2));

	//kopiujemy 4 kolumny (2 lewo, 2 prawo)
	input.col(0).copyTo(output.col(0));
	input.col(1).copyTo(output.col(1));
	input.col(input.cols - 1).copyTo(output.col(output.cols - 1));
	input.col(input.cols - 2).copyTo(output.col(output.cols - 2));
}

void produceGaussian(double **filter) {
	double sigma = 2;
	const int size = 5;
	double mu = size / 2;
	double sum = 0.0;

	for (int x = 0; x < size; ++x) {
		for (int y = 0; y < size; ++y) {
			filter[x][y] = exp(
					-0.5
							* (pow((x - mu) / sigma, 2.0)
									+ pow((y - mu) / sigma, 2.0)))
					/ (2 * M_PI * sigma * sigma);

			// Sumuj elementy
			sum += filter[x][y];
		}
	}

	// Normalizujemy macierz
	for (int x = 0; x < size; ++x)
		for (int y = 0; y < size; ++y)
			filter[x][y] /= sum;
}

void gaussianBlur(unsigned char* input, unsigned char* output, int width,
		int height, int colorWidthStep, int channels) {
//	int i = blockIdx.x * blockDim.x + threadIdx.x;
	double sum;

	//wykonujemy do momencu aż nie przekroczymy przedostatniego wiersza klatkia
	for (int i = 2; i < (height - 2) * colorWidthStep; i++) {
		//sprawdzamy czy przypadkiem
		if (i >= 2 * colorWidthStep) {
			int mod = i % colorWidthStep;

			//sprawdzamy czy piksel nie znajduje sie na poczatku lub koncu wiersza klatki, jesli nie wykonujemy operacji matematyczne rozmywajace piksel
			if ((mod >= (2 * channels))
					&& (mod <= (colorWidthStep - (2 * channels) - 1))) {
				sum = 0;
				sum += dev_2d[0][0]
						* input[i - (2 * colorWidthStep) - (2 * channels)]
						+ dev_2d[1][0]
								* input[i - (2 * colorWidthStep)
										- (1 * channels)]
						+ dev_2d[2][0] * input[i - (2 * colorWidthStep)]
						+ dev_2d[3][0]
								* input[i - (2 * colorWidthStep)
										+ (1 * channels)]
						+ dev_2d[4][0]
								* input[i - (2 * colorWidthStep)
										+ (2 * channels)];
				sum += dev_2d[0][1]
						* input[i - (1 * colorWidthStep) - (2 * channels)]
						+ dev_2d[1][1]
								* input[i - (1 * colorWidthStep)
										- (1 * channels)]
						+ dev_2d[2][1] * input[i - (1 * colorWidthStep)]
						+ dev_2d[3][1]
								* input[i - (1 * colorWidthStep)
										+ (1 * channels)]
						+ dev_2d[4][1]
								* input[i - (1 * colorWidthStep)
										+ (2 * channels)];
				sum += dev_2d[0][2] * input[i - (2 * channels)]
						+ dev_2d[1][2] * input[i - (1 * channels)]
						+ dev_2d[2][2] * input[i]
						+ dev_2d[3][2] * input[i + (1 * channels)]
						+ dev_2d[4][2] * input[i + (2 * channels)];
				sum += dev_2d[0][3]
						* input[i + (1 * colorWidthStep) - (2 * channels)]
						+ dev_2d[1][3]
								* input[i + (1 * colorWidthStep)
										- (1 * channels)]
						+ dev_2d[2][3] * input[i + (1 * colorWidthStep)]
						+ dev_2d[3][3]
								* input[i + (1 * colorWidthStep)
										+ (1 * channels)]
						+ dev_2d[4][3]
								* input[i + (1 * colorWidthStep)
										+ (2 * channels)];
				sum += dev_2d[0][4]
						* input[i + (2 * colorWidthStep) - (2 * channels)]
						+ dev_2d[1][4]
								* input[i + (2 * colorWidthStep)
										- (1 * channels)]
						+ dev_2d[2][4] * input[i + (2 * colorWidthStep)]
						+ dev_2d[3][4]
								* input[i + (2 * colorWidthStep)
										+ (1 * channels)]
						+ dev_2d[4][4]
								* input[i + (2 * colorWidthStep)
										+ (2 * channels)];

				//zapisujemy otrzymany wynik jako nowa wartosc piksela
				output[i] = sum;
			}
		}

		//przeskakujemy przez wszystkie pracujace watki

	}
}

int main(int argc, char** argv) {
	int taskid, ntasks;
	ostringstream s, d;
	Init(argc, argv);
	taskid = COMM_WORLD.Get_rank();
	ntasks = COMM_WORLD.Get_size();

//	if (taskid == 0) {
	//walidacja liczby argumentow wejściowych
	if (argc != 3) {
		printf("Usage: %s <path in> <path out>\n", argv[0]);
		Finalize();
		exit(EXIT_FAILURE);
	}

	if (ntasks < 2) {
		printf("Insuficient number of processes. At least two are needed\n");
		Finalize();
		exit(EXIT_FAILURE);

	}
//		}

	dev_2d = new double*[5];
	for (int i = 0; i < 5; i++) {
		dev_2d[i] = new double[5];
	}

	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			dev_2d[i][j] = 0;
		}
	}

	produceGaussian(dev_2d);
	/* Rozpoczecie pomiaru czasu */
	double initTime, totalTime;
	MPI_Barrier(MPI_COMM_WORLD);
	initTime = MPI_Wtime();
	/*****************************/

	if (taskid == 0) {

		Mat mat = imread(argv[1], CV_LOAD_IMAGE_COLOR);
		if (!mat.data)
			exit(1);

		int idx = 1;
		int t = mat.rows / (ntasks - 1);
		vector<Mat> fragment;
		for (int c = 0; c < mat.rows; c += t) {
			if (idx == ntasks - 1) {

				t = mat.rows - c;

			}
			fragment.push_back(mat(Rect(0, c, mat.cols, t)).clone());
			int *tmp = new int[3];
			tmp[0] = fragment[idx - 1].rows;
			tmp[1] = fragment[idx - 1].cols;
			tmp[2] = 3;
			MPI::COMM_WORLD.Send(tmp, 3, MPI_INT, idx, 1);

			idx++;

			free(tmp);

		}

		for (int i = 1; i < ntasks; i++) {

			COMM_WORLD.Send(fragment[i - 1].data,
					fragment[i - 1].rows * fragment[i - 1].cols * 3, MPI_BYTE,
					i, 0);
		}

		fragment.clear();
		mat.release();

	}

	if (taskid != 0) {
		int *img_size = new int[3];
		MPI::COMM_WORLD.Recv(img_size, 3, MPI_INT, 0, 1);
		Mat test = Mat(img_size[0], img_size[1], CV_8UC3);
		COMM_WORLD.Recv(test.data, img_size[0] * img_size[1] * 3, MPI_BYTE, 0,
				0);

		s << "p" << taskid << ".jpg";
		Mat temp = Mat(img_size[0], img_size[1], CV_8UC3);

		gaussianBlur(test.ptr(), temp.ptr(), test.cols, test.rows,
				test.step, test.channels());

		copyRowsCols(test, temp);

		COMM_WORLD.Send(img_size, 3, MPI_INT, 0, 3);

		COMM_WORLD.Send(temp.data, temp.cols * temp.rows * 3, MPI_BYTE, 0, 4);

		test.release();
		temp.release();
		free(img_size);

	}
	if (taskid == 0) {
		vector<Mat> parts;
		Mat output;
		int tmp2[ntasks - 1][3];
		for (int i = 1; i < ntasks; i++) {
			COMM_WORLD.Recv(tmp2[i - 1], 3, MPI_INT, i, 3);

		}

		for (int i = 1; i < ntasks; i++) {
			Mat strip = Mat(tmp2[i - 1][0], tmp2[i - 1][1], CV_8UC3);
			COMM_WORLD.Recv(strip.data, tmp2[i - 1][0] * tmp2[i - 1][1] * 3,
					MPI_BYTE, i, 4);
			output.push_back(strip);
			strip.release();
		}
		/* Zakonczenie pomiaru czasu */
		totalTime = MPI_Wtime() - initTime;
		/******************************/
		printf("%.0f\n", (totalTime * 1000));
		imwrite(argv[2], output);
		output.release();
		parts.clear();

	}

	free(dev_2d);
	Finalize();
}

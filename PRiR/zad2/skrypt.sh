#!/bin/bash
deploy() {
	make
	make pdf
	make clean
}

test() {
	TIME=0;
	for ((i=1; i<11; i++))
	do
		TMP=`./gauss_omp $1 $2 $3`
		TIME=$(echo "$TIME + $TMP" | bc -l)
	done
	echo $( echo "$TIME/10" | bc -l )
}

run() {
	/dev/null > wynik
	for ((i=1; i<=$1; i++))
	do
		echo "$(test $i $2 $3)" >> wynik.log
	done
}

deploy
run $1 $2 $3

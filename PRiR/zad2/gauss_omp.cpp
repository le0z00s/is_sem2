#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;


int main(int argc, char *argv[]) {

	if(argc !=4){
		printf("Usage: gauss_omp <n> <input_image> <output_image>\n");
		exit(-1);
	}
	int t = atoi(argv[1]);
	if(t<1){
        printf("Parameter <n> has to be gigger than 0\n");
		exit(-1);
	}

    cv::Mat input, output;
    int x, y;
    int r, g, b;

    int mask[5][5] = {{1, 4,  7,  4,  1},
                      {4, 16, 26, 16, 4},
                      {7, 26, 41, 26, 7},
                      {4, 26, 16, 26, 4},
                      {1, 4,  7,  4,  1}};

    input = cv::imread(argv[2], CV_LOAD_IMAGE_COLOR);
    output = input.clone();
    double czasRozpoczecia = omp_get_wtime();
    #pragma omp parallel for private(y, x, r, g, b) num_threads(atoi(argv[1])) schedule(dynamic)
    for (y = 2; y < input.rows - 2; y++) {
        for (x = 2; x < input.cols - 2; x++) {

            r = 0;
            g = 0;
            b = 0;

            for (int maskX = 0; maskX < 5; maskX++) {
                for (int maskY = 0; maskY < 5; maskY++) {
                    cv::Vec3b &in = input.at<cv::Vec3b>(maskY + y - 2, maskX + x - 2);
                    r += in[0] * mask[maskX][maskY];
                    g += in[1] * mask[maskX][maskY];
                    b += in[2] * mask[maskX][maskY];
                }
            }

            cv::Vec3b &out = output.at<cv::Vec3b>(y, x);
            out[0] = (uchar) (r / 283);
            out[1] = (uchar) (g / 283);
            out[2] = (uchar) (b / 283);
        }
    }
    //czas koncowy
    	double czasZakonczenia = omp_get_wtime();
    	double czasTrwaniaOmp = 1000 * (czasZakonczenia - czasRozpoczecia);
        printf("%.4f\n", czasTrwaniaOmp);

    imwrite(argv[3], output);

    return 0;
}
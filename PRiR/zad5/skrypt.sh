#!/bin/bash
deploy() {
	make
	make pdf
}

test() {
	TIME=0;
	TIMES=3;
	for ((i=1; i<$TIMES; i++))
	do
		TMP=`mpirun -n $1 ./macierz_mpi $2`
		TIME=$(echo "$TIME + $TMP" | bc -l)
	done
	echo $( echo "$TIME/$TIMES" | bc -l )
}

run() {
	cat /dev/null > czasy
	cat /dev/null > przyspieszenie
	for ((i=1; i<=$1; i*=2))
	do
		CZAS=$(test $i $2)
		if(i==1)
		then
			FIRST=$CZAS
		fi

		PRZYSPIESZENIE=$(echo "scale=4; $FIRST/$CZAS" | bc -l)

		echo "$i;$CZAS" >> czasy
		echo "$i;$PRZYSPIESZENIE" >> przyspieszenie
	done
}

deploy
run $1 $2 
make clean

#include "mpi.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

using namespace std;

#define MASTER 0

/* allocates array in memory */
double **allocate_array(int rows, int cols) {
	double **output;
	int i;

	output = (double **)malloc(rows*sizeof(double *));
	output[0] = (double *)malloc(rows*cols*sizeof(double));

	for(i = 1; i < rows; i++) {
		output[i] = output[i - 1] + cols;
	}
	return output;
}

void free_memory(double **array, int size_of_matrix) {
	int i;
	for(i=1; i < size_of_matrix; i++) {
		array[i] = NULL;
	}
	free(array[0]);
	free(array);
}

int getTime(long start, long stop) {
	return (stop - start) / CLOCKS_PER_SEC*1000;
}

int main(int argc, char *argv[])
{
	double **A, **B, **C; /* matrices */
	int size_of_matrix, /* size o matrix */		
		i, j, k;
	clock_t start, stop;
	double **A_part, **C_part; /* parts of arrays that will be calculated by task */
	int taskId, numTasks;

	if(argc != 2) {
		printf("Usage: %s <size_of_matrix>\n", argv[0]);
		exit(1);
	}

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &taskId);
	MPI_Comm_size(MPI_COMM_WORLD, &numTasks);

	/* read size of matrices on master*/
	if(taskId == MASTER) {
		size_of_matrix = atoi(argv[1]);
	}

	/* wait for master to end (just in case) */
	MPI_Barrier(MPI_COMM_WORLD);

	/* send size of matrices to every task */
	MPI_Bcast(&size_of_matrix, 1, MPI_INT, MASTER, MPI_COMM_WORLD);

	/* allocate memory or matrix B for every task */
	B = allocate_array(size_of_matrix, size_of_matrix);

	/* only master needs to have whole matrices A and C */
	if(taskId == MASTER) {
		A = allocate_array(size_of_matrix, size_of_matrix);
		C = allocate_array(size_of_matrix, size_of_matrix);
	}

	/* every task allocates their part of matrices A and C */
	A_part = allocate_array(size_of_matrix / numTasks, size_of_matrix);
	C_part = allocate_array(size_of_matrix / numTasks, size_of_matrix);

	srand( time( NULL ) );
	/* initialize arrays */
	if(taskId == MASTER) {
		for(i=0; i < size_of_matrix; i++) {
			for(j=0; j < size_of_matrix; j++) {
				A[i][j] = (int)(sin(i)*i*j) % 10;
				B[i][j] = (int)(cos(j)*(i + j)) % 10;
			}
		}
	}

	/* wait for master to end (just in case) */
	MPI_Barrier(MPI_COMM_WORLD);

	start = clock();

	MPI_Scatter(*A, size_of_matrix*size_of_matrix / numTasks, MPI_DOUBLE, 
		*A_part, size_of_matrix*size_of_matrix / numTasks, MPI_DOUBLE, 
		MASTER, MPI_COMM_WORLD);

	MPI_Bcast(*B, size_of_matrix*size_of_matrix, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

	/* each process works on its own part of matrices */
	for(i = 0; i < size_of_matrix / numTasks; i++) {
		for(j = 0; j < size_of_matrix; j++) {
			C_part[i][j] = 0.0;
		}
	}

	for(i = 0; i < size_of_matrix / numTasks; i++) {
		for(k=0; k < size_of_matrix; k++) {
			for(j=0; j < size_of_matrix; j++) {
				C_part[i][j] += A_part[i][k]*B[k][j];
			}
		}
	}

	/* collect all the data back on master */
	MPI_Gather(*C_part, size_of_matrix*size_of_matrix / numTasks, MPI_DOUBLE,
		*C, size_of_matrix*size_of_matrix / numTasks, MPI_DOUBLE,
		MASTER, MPI_COMM_WORLD);

	if(taskId == MASTER) {
		stop = clock();
		free_memory(A, size_of_matrix);
		free_memory(C, size_of_matrix);
	}

	free_memory(C_part, size_of_matrix / numTasks);
	free_memory(A_part, size_of_matrix / numTasks);
	free_memory(B, size_of_matrix);

	if(taskId == MASTER) {
		printf("%d\n", getTime(start, stop));
	}
        
	MPI_Finalize();
	return 0;
}
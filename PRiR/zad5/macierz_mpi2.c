#include "mpi.h"
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <time.h>

using namespace std;

#define MASTER 0
#define FROM_MASTER 1
#define FROM_SLAVE 2

int getTime(long start, long stop) {
	return (stop - start) / CLOCKS_PER_SEC * 1000;
}

void abort() {
	int err;
	MPI_Abort(MPI_COMM_WORLD, err);
	exit(1);
}

int main(int argc, char *argv[]) {
	int number_of_tasks, /* overall number of tasks */
		task_id,	/* id of a task*/
		i, j, k,	/*indexes for  loops*/
		rows,		/* number of rows to send to slave processes */
		from_row,
		to_row,
		offset,		/* offset o matrix for a single process */
		size_of_matrix;		/* size of a matrix */

	double **A,
		   **B,
		   **C;
	long start, stop;	/* variables for clock */

	MPI_Status status;
	MPI_Request request;

	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&task_id);
	MPI_Comm_size(MPI_COMM_WORLD,&number_of_tasks);

	if( task_id == MASTER ) {
		if(number_of_tasks < 2) {
			printf("Need at least two MPI processes. Exiting...\n");
			abort();
		}
		if( argc != 2 ) {
			printf("Usage: %s <size_of_matrix>\n", argv[0]);
			abort();
		}else {
			size_of_matrix = atoi(argv[1]);
			/* Alocate memory */
			srand(time(NULL));
			A = new double* [size_of_matrix];
			B = new double* [size_of_matrix];
			C = new double* [size_of_matrix];

			for(i=0; i < size_of_matrix; ++i) {
				A[i] = new double [size_of_matrix];
				B[i] = new double [size_of_matrix];
				C[i] = new double [size_of_matrix];
			}
			/* generate matrices */
			for(i=0; i < size_of_matrix; i++) {
				for(j=0; j < size_of_matrix; j++) {
					A[i][j] = (long)(sin(i) * i * j) % 10;
					B[i][j] = (long)(cos(i) * (i + j)) % 10;
					C[i][j] = 0;
				}
			}

			start = clock();
			/* send fragments of matrices to slaves */
			for(i=1; i < number_of_tasks; i++) {
				rows = (size_of_matrix / (number_of_tasks - 1));
				from_row = (i - 1) * rows;
				if(((i + 1) == number_of_tasks) && ((rows % (number_of_tasks - 1)) !=0)) {
					to_row = rows;
				}else {
					to_row = from_row + rows;
				}
				/* Send lower bound to slave without blocking */
				MPI_Isend(&from_row, 1, MPI_INT, i, FROM_MASTER, MPI_COMM_WORLD, &request);
				MPI_Isend(&to_row, 1, MPI_INT, i, FROM_MASTER + 1, MPI_COMM_WORLD, &request);
				MPI_Isend(&A[from_row][0], (to_row - from_row) * size_of_matrix, MPI_DOUBLE, i, FROM_MASTER + 2, MPI_COMM_WORLD, &request);
			}
		}
		/* Broadcast matrix B to all slaves */	
		MPI_Bcast(&B, size_of_matrix * size_of_matrix, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		MPI_Bcast(&size_of_matrix, 1, MPI_INT, 0, MPI_COMM_WORLD);
	}
	if(task_id > MASTER) {
		/* Slave receives its portion o data to process */
		MPI_Recv(&from_row, 1, MPI_INT, 0, FROM_MASTER, MPI_COMM_WORLD, &status);
		MPI_Recv(&to_row, 1, MPI_INT, 0, FROM_MASTER + 1, MPI_COMM_WORLD, &status);
		MPI_Recv(&A[from_row], (to_row - from_row) * size_of_matrix, MPI_DOUBLE, 0, FROM_MASTER + 2, MPI_COMM_WORLD, &status);
		for (i = from_row; i < to_row; i++) {
			for (j = 0; i < size_of_matrix; j++) {
				for (k=0; k<size_of_matrix; k++) {
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}

		MPI_Isend(&from_row, 1, MPI_INT, 0, FROM_SLAVE, MPI_COMM_WORLD, &request);
		MPI_Isend(&to_row, 1, MPI_INT, i, FROM_SLAVE + 1, MPI_COMM_WORLD, &request);
		MPI_Isend(&C[from_row][0], (to_row - from_row) * size_of_matrix, MPI_DOUBLE, 0, FROM_SLAVE + 2, MPI_COMM_WORLD,&request);
	}
	if(task_id == MASTER) {
		for (i = 1; i < number_of_tasks; i++) {
			MPI_Recv(&from_row, 1, MPI_INT, i, FROM_SLAVE, MPI_COMM_WORLD, &status);
			MPI_Recv(&to_row, 1, MPI_INT, i, FROM_SLAVE + 1, MPI_COMM_WORLD, &status);
			MPI_Recv(&C[from_row][0], (to_row - from_row) * size_of_matrix, MPI_DOUBLE, i, FROM_SLAVE + 2, MPI_COMM_WORLD, &status);
		}
		stop = clock();
		int time = getTime(start, stop);
		printf("%d\n", time);
	}
	/* clean memory */
	MPI_Finalize();
	return 0;
}
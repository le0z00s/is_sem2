#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <omp.h>
#include <cmath>
using namespace std;

/*
	PRiR Laborataroium numer 1
	Zadanie 1. Mnozenie macierzy - OpenMP
	Rafal Zbojak
*/
int main(int argc, char* argv[])
{
	long **A;
	long **B;
	long **C;
	long i,j,k;

	//walidacja danych wejsciowych programu, wymagane dwa argumenty: n - liczba watkow, size - rozmiar macierzy
	if (argc != 3) {
		printf("Usage: ./pi_omp <n> <size>\n");
		exit(-1);
	}
	
	//konwersja na liczby calkowite
	int nthreads = atoi(argv[1]);
	long size = atoi(argv[2]);

	//sprawdzamy poprawnosc liczb
	if (nthreads <= 0 || size <= 0) {
		printf("Invalid arguments. Should be bigger than 0.\n");
		exit(-1);
	}
	
	srand( time( NULL ) );

	//alokacja pamieci
	A = new long* [size];
	B = new long* [size];
	C = new long* [size];
	
	for (i = 0; i < size; ++i){
		A[i] = new long [size];
		B[i] = new long [size];
		C[i] = new long [size];
	}

	//wylosowanie danych i wyzerowanie macierzy docelowej
	for(i=0; i<size; i++) {
		for(j=0; j<size; j++) {
			A[i][j] = (int)(sin(i) * i * j) % 10;
			B[i][j] = (int)(cos(j) * (i + j)) % 10;
			C[i][j]=0;
		}
	}

	//czas startowy
	double czasRozpoczecia = omp_get_wtime();

	//mnozenie
	#pragma omp parallel for default(none) shared(A, B, C) private(k,j) firstprivate(size) num_threads(nthreads)
	for(i=0; i<size; i++) {
		for(j=0; j<size; j++) {			
			for(k=0; k<size; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	//czas koncowy mnozenia
	double czasZakonczenia = omp_get_wtime();

	double czasTrwaniaOmp = 1000 * (czasZakonczenia - czasRozpoczecia);
	printf("%.0f\n", czasTrwaniaOmp);

	//zwolnienie pamieci
	for (i = 0; i < size; ++i){
		delete[] A[i];
		delete[] B[i];
		delete[] C[i];
	}

	delete[] A;
	delete[] B;
	delete[] C;

	return 0;
}


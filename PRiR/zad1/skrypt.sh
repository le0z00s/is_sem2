#!/bin/bash
deploy() {
	make
	make pdf
	make clean
}

test() {
	TIME=0;
	for ((i=1; i<11; i++))
	do
		TIME=$TIME+`./macierz_omp $1 $2`
	done
	echo $(($TIME/10))
}

run() {
	/dev/null > wynik
	for ((i=1; i<=$1; i++))
	do
		echo "$(test $i $2)" >> wynik
	done
}

deploy
run $1 100

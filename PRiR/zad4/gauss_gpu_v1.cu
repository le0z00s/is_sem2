#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

#define MAX_THREADS 1024
#define MASK_SIZE 5

//hardcoded number of threads
#define THREADS_NUMBER 8192

int grid, block;

//mask constant stored in device's memory
__constant__ double device_kernel_2d[MASK_SIZE][MASK_SIZE];

__global__ void gaussianBlurKernel(unsigned char* input, unsigned char* output, int width, int height, int step, int channels) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	double newPixelValue;

	while(tid < (height - 2) * step) {
		if(tid >= 2 * step) {
			int mod = tid % step;

			//check if pixel is on the border of picture
			if((mod >= (2 * channels)) && (mod <= (step - (2 * channels) - 1))) {
				newPixelValue = 0.0;

				for(int x = 0 ; x < MASK_SIZE ; x++) {
					for(int y = 0 ; y < MASK_SIZE ; y++) {
						newPixelValue += device_kernel_2d[x][y] * input[tid + ((y - 2) * step) + ((x - 2) * channels)];
					}
				}
				output[tid] = (unsigned char) newPixelValue;
			}
		}

		tid += blockDim.x * gridDim.x;
	}
}

float blur(const Mat& input, Mat& output) {
	float elapsedTime = 0.0;

	const int matrixSize = input.step * input.rows;
	unsigned char *dev_input, *dev_output;

	//allocate memory on the device
	cudaMalloc<unsigned char>(&dev_input, matrixSize);
	cudaMalloc<unsigned char>(&dev_output, matrixSize);

	//copy input data into device
	cudaMemcpy(dev_input, input.ptr(), matrixSize, cudaMemcpyHostToDevice);

	//start timer
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);

	gaussianBlurKernel<<<grid, block>>>(dev_input, dev_output, input.cols, input.rows, input.step, input.channels());

	//stop timer and read execution time
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsedTime, start, stop);

	//synchronize threads to check if everything went well
	cudaDeviceSynchronize();

	//copy data from device into output stream
	cudaMemcpy(output.ptr(), dev_output, matrixSize, cudaMemcpyDeviceToHost);

	//free device memory
	cudaFree(dev_input);
	cudaFree(dev_output);

	return elapsedTime;
}

void copyMatrixData(const Mat& input, Mat& output) {
	//copy 4 rows from input to output
	input.row(0).copyTo(output.row(0));
	input.row(1).copyTo(output.row(1));
	input.row(input.rows - 1).copyTo(output.row(output.rows - 1));
	input.row(input.rows - 2).copyTo(output.row(output.rows - 2));

	//copy 4 columns from input to output
	input.col(0).copyTo(output.col(0));
	input.col(1).copyTo(output.col(1));
	input.col(input.cols - 1).copyTo(output.col(output.cols - 1));
	input.col(input.cols - 2).copyTo(output.col(output.cols - 2));
}

void estimateGridBlockSize(const int threads_number) {
	grid = ceil((double) threads_number / MAX_THREADS);
	block = threads_number / grid;
}

/*
	Function generates Gaussian blur filter as a XxY matrix.
	Returns normilized XxY matrix
*/
void generateGaussianMask(double mask[][MASK_SIZE]) {

	double sigma = 1.0;
	double mu = MASK_SIZE / 2.0;

	double sum = 0.0;

	for (int x = 0; x < MASK_SIZE; ++x) {
		for (int y = 0; y < MASK_SIZE; ++y) {
			mask[x][y] = exp( -0.5 * (pow((x - mu) / sigma, 2.0)
				+ pow((y - mu) / sigma, 2.0)))
				/ (2.0 * M_PI * pow(sigma, 2.0));

			//sum all elements
			sum+=mask[x][y];
		}
	}

	//normalize matrix
	for (int x = 0; x < MASK_SIZE; ++x) {
		for (int y = 0; y < MASK_SIZE; ++y) {
			mask[x][y] /= sum;
		}
	}
}

int main(int argc, char const *argv[]) {
	int threads_number;
	//validate input data
	#ifdef THREADS_NUMBER
		if(argc != 3) {
			printf("Usage: %s <image_input> <image_output>\n", argv[0]);
			exit(-1);
		}	
		threads_number = THREADS_NUMBER;
	#endif
	#ifndef THREADS_NUMBER
		/* Old code for testing purposes. 
	   Adds additional parameter which determine number of threads.
	   To use this code comment threads_number definition on line 19.*/
		if (argc != 4) {
			printf("Usage: %s <threads_number> <image_input> <image_output>\n", argv[0]);
			exit(-1);
		}

		threads_number = atoi(argv[1]);
	#endif
	//validate number o threads
	if(threads_number <= 0) {
		printf("Invalid number of threads. Should be higher than 0. Number of threads was set to: %d\n", threads_number);
		exit(-1);
	}

	//estimate grid and block size
	estimateGridBlockSize(threads_number);

	cv::Mat input, output;
	
	input = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);// change to argv[2] for testing
	
	output = input.clone();

	//generate Gaussian mask and copy it to the device's memory
	double kernel2d[MASK_SIZE][MASK_SIZE];
	generateGaussianMask(kernel2d);
	cudaMemcpyToSymbol(device_kernel_2d, kernel2d, sizeof(double) * MASK_SIZE * MASK_SIZE);

	float executionTime;

	executionTime = blur(input, output);
	copyMatrixData(input, output);
	
	cv::imwrite(argv[2], output);// change to argv[3] for testing.

	printf("%.0f\n", executionTime);
	return 0;
}
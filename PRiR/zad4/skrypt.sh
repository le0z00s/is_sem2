#!/bin/bash
deploy() {
	make
	make pdf
}

test() {
	TIME=0;
	TIMES=10;
	for ((i=1; i<$TIMES; i++))
	do
		TMP=`./gauss_gpu $1 $2 $3`
		TIME=$(echo "$TIME + $TMP" | bc -l)
	done
	echo $( echo "$TIME/$TIMES" | bc -l )
}

run() {
	cat /dev/null > czasy
	cat /dev/null > przyspieszenie
	for ((i=1; i<=$1; i*=2))
	do
		CZAS=$(test $i $2 $3)
		if(i==1)
		then
			FIRST=$CZAS
		fi

		PRZYSPIESZENIE=$(echo "scale=4; $FIRST/$CZAS" | bc -l)

		echo "$i;$CZAS" >> czasy
		echo "$i;$PRZYSPIESZENIE" >> przyspieszenie
	done
}

deploy
run $1 $2 "./output.jpg"
make clean

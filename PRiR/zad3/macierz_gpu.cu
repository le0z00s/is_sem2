#include <stdlib.h>
#include <stdio.h>
#include <math.h>
 
__global__ void initializeArrays(double* d_a, double* d_b, unsigned int size) {
	double i, j;

	for (int index = 0; index < size ; index++) {
		i = (double) index;
		j = (double)(index / size);

      int ix = blockIdx.x * size + threadIdx.x;

		d_a[index * ix * size] = (double)(sin(i) * i * j);
		d_b[index * ix * size] = (double)(cos(j) * (i + j));
	}
}

void randomInitA(double* data, unsigned int size) {
	for(int i=0; i<size; i++) {
		for(int j=0; j<size; j++) {
			data[i * size + j] = (int)(sin(i) * i * j) % 10;
		}
	}
}

void randomInitB(double* data, unsigned int size) {
	for(int i=0; i<size; i++) {
		for(int j=0; j<size; j++) {
			data[i * size + j] = (int)(cos(j) * (i + j)) % 10;
		}
	}
}

// CUDA Kernel
__global__ void matrixMul( double* C, double* A, double* B, unsigned int size)
{
 
   // 1. 2D Thread ID
   int tx = blockIdx.x * size + threadIdx.x;
   int ty = blockIdx.y * size + threadIdx.y;
 
   // value stores the element that is 
   // computed by the thread
   float value = 0.0;
   for (int i = 0; i < size; i++) {
      double elementA = A[ty * size + i];
      double elementB = B[i * size + tx];
      value += elementA * elementB;
   }
 
   // Write the matrix to device memory each 
   // thread writes one element
   C[ty * size + tx] = value;
}
 
float executionTime = 0.0;

int main(int argc, char *argv[]) {

	if (argc != 4) {
		printf("Usage: ./macierz_gpu <n> <m> <size>\n");
		exit(-1);
	}

	int gridSize = atoi(argv[1]);
	int blockSize = atoi(argv[2]);
	unsigned int matrixSize = pow(atoi(argv[3]), 2);
	unsigned int mem_matrixSize = sizeof(double) * matrixSize;

	if (gridSize <= 0 || blockSize <= 0 || matrixSize <= 0) {
		printf("Invalid arguments. All should be bigger than 0.\n");
		exit(-1);
	}
 
   //allocate host memory for matrices A and B
   double* h_A = (double*) malloc(mem_matrixSize);
   double* h_B = (double*) malloc(mem_matrixSize);
 
   //setup execution parameters
   dim3 threads(blockSize, blockSize);
   dim3 grid(gridSize, gridSize);

   //allocate device memory
   double* d_A;
   double* d_B;
   cudaMalloc((void**) &d_A, mem_matrixSize);
   cudaMalloc((void**) &d_B, mem_matrixSize);

   initializeArrays<<<grid,threads>>>(d_A, d_B, matrixSize);

   //allocate host memory for matrix C
   double* h_C = (double*) malloc(mem_matrixSize);

   //allocate device memory or matrix C
   double* d_C;
   cudaMalloc((void**) &d_C, mem_matrixSize);
   
   //initialize timers
   cudaEvent_t start,stop;
   float elapsedTime;

   cudaEventCreate(&start);
   cudaEventCreate(&stop);
   cudaEventRecord(start,0);

   matrixMul<<<grid,threads>>>(d_C, d_A, d_B, 8);

   //stop timer
   cudaEventRecord(stop,0);
   cudaEventSynchronize(stop);
   cudaEventElapsedTime(&elapsedTime,start,stop);
   executionTime += elapsedTime;

   //synchronize threads to see i everything went well
   //cudaDeviceSynchronize();

   //copy device memory to host
   cudaMemcpy(h_C, d_C, mem_matrixSize, cudaMemcpyDeviceToHost);

   printf("%.8f\n", executionTime);

   free(h_A);
   free(h_B);
   free(h_C);
   cudaFree(d_A);
   cudaFree(d_B);
   cudaFree(d_C);
}